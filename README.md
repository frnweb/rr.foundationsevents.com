# Foundations Recovery Network WordPress Upstream

This is a Pantheon Custom Upstream maintained by [Foundations Recovery Network](https://foundationsrecoverynetwork.com).

## Development

### Site Development

To begin development on this site, clone the repository from Pantheon, enter the newly created directory, and type the following in your terminal:

```
yarn
yarn run init
```

### Theme Development

Each Pantheon instance may have different themes with their own requirements for development, builds, testing, etc. Refer to the theme documentation in the `./web/wp-content/themes/{your theme here}` folder.

## Maintenance

## Testing

To run all tests, enter the following in your terminal:

```
yarn run test
```

### Visual Testing

This Custom Upstream equips all of its downstream sites with [BackstopJS](https://garris.github.io/BackstopJS/) Visual Regression testing tools. The tools are already part of the test workflow, and they have been automated with the Gulp build process.

This process downloads a list of all of the actively published pages on the Pantheon **Live** environment. It them takes screenshots of all those pages and compares them to screenshots taken from the Pantheon **Test** environment.
