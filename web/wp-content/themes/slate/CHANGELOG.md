#### Changelog 
** 1.8.27 **
- Added the Wrapper feature to the wildcard Card.  
- Added the function that returns the classes and module info in the admin options to the wildcard flexible content field.  

** 1.8.26 **
- Cleaned up and reorganized the shortcodes.php file. Also added new nested comments and collapsing for sublime.  
- Added the 'do_shortcode' filter to the output of some shortcodes where it was missing (Accordion_item, Tab panel).  

** 1.8.25 **
- Added a class to the block grid shortcode called 'column-block'.
- Added support for shortcodes in the Duo Wysiwyg's. 

** 1.8.24 **
- Adding an option to have a solo card in a deck. 

** 1.8.23 **
- Removed the 'HEYYYYYY Bitch' comment that was added as a demonstration for Slate 1.8.21.  

** 1.8.22 **
- Fixed Spotlight Header to not echo an empty H1 tag if there is no text.

** 1.8.21 **
- Changed Spotlight for no reason.

** 1.8.20 **
- Reworked the Footer-Location section/module to work in the same way as the Contact Module.
- You can now add multiple markers/locations to the Footer Location.
- Theme Footer Options now include an Address Section where you will populate the Facility Address to be used throughout the site.  

** 1.8.13 **
- Fixed new bug introduced in 1.8.12 with PHP args.  

** 1.8.12 **
- Fixed PHP warning for incompatible walker menu   

** 1.8.11 **
- Added shortcode support for the spotlight secondary text  

** 1.8.10 **
- Added an option for Medium Column Width on Decks. Older Versions will have to have this set in the wp-admin.  

** 1.8.02 **
- Added filters for the wysiwyg's in Modules to allow shortcodes to work.  
- 
** 1.8.01 **
- Fixed a small bug that was left in from testing the new modal module.  

** 1.8 **
- NEW MODULE: Modal, any button and/or link can now target a Modal that can be added via a module.
- The new Modal currently has an option for a Video or Content. 
- The Content Modal is currently a Wildcard Flexible Content field for flexibility. 

** 1.7.11 **
- Added the 'Section Header' to the Settings for Duo's. Idk why it wasn't there already. 

** 1.7.08 **
- Added dat nice lil 'min-height: 1px' that makes everything in a bxslider work all nice and good. Shit was skipping slides again.  
- Added the rule to dat style.beautified.css for dat basic gallery.  

** 1.7.07 **
- Added 2 new insurance choices (Health Partners, Preferred One) in the Insurance module

** 1.7.06 **
- Added ability to add custom style="" tag to the [callout] shortcode

** 1.7.05 **
- Made a few changes to the staff-module.php that fixes the equalizer not being called properly.
- Equalizer is now called from the slate-child-theme/assets/js/child.js like the rest of the bx-slider's equalizers, this is because the equalizer needed to be called on bx-viewport but only after the bx-slider was initialized. 
- Should equalize fine now, but a new wrapper called staff__holder is where the 'data-equalizer-watch' lives that is wrapped around each staff member and is either a link or a div based on the staff link option.

** 1.7.04 **
- Added an optional checkbox for the Staff Module and having the staff members link to their own pages or not.
- Additional filter for carousel deck repeater to keep those collapse icons.

** 1.7.03 **
- Added a filter to acf-layout.php that allows all Deck's to have the collapse feature.
- Was just relying on a fortunate bug from ACF that had to be repeated everytime the deck module was edited.

** 1.7.02 **
- Changed Admin Options to only append the layout__title to the module name instead of replacing the module name.
- This only affects the Wordpress Admin area on editing a page.
- Added some styles for the new layout__label.  

** 1.7.01 **
- added custom ID option for wysiwyg where it was missing.
- moved the call for footer-location to the footer-layout.php.
- fixed some styles for the insurance__grid not working correctly.

** 1.7.02 **  
- Changed Admin Options to only append the layout__title to the module name instead of replacing the module name.  
- This only affects the Wordpress Admin area on editing a page.  
- Added some styles for the new layout__label.  

** 1.7 **  
-Version Control and Automatic Updates using a plugin-update-checker.  
-Three new header options, Nav-Title-Bar, Nav-Default, Nav-Stacked.  
-Two options for mobile menus, Off-Canvas or Popover.  
-New options for sticky.  
-NEW MODULE: Wrapper  
-Reorganize the Nav Secondary to reorder nav pieces like Phone Number, Search, Usernav.  
-Reconfigured the header.php and broke into several parts.  
-Reconfigured footer.php and broke into several parts.  
-Resources Page now uses a width and column option.  
-Added a wyswiwyg option for Billboard.  
-Option to completely turn off the Header.

** 1.2.78 **  
-Added some insurance logos for Valley Hospital

** 1.2.77 **  
-Footer Addons.
-Adding top anchor to header.php.
-Moved search popup outside of the closing header tag.

** 1.2.76 **  
-Fixed some issues with wildcard and the equalizer.

** 1.2.75 **  
-Changes how Title-Bar is built.
-Easier overrides to pieces inside Title-Bar

** 1.2.74 **  
-Added option for wildcard to be wrapped in a link

** 1.2.72 **  
-Fix for foundations adding an 'opens-left' class to dropdown menus

** 1.2.71 **  
-Single Staff page layout options.

** 1.2.7 **  
-Added option for Title-Bar or Mega-Menu.

** 1.2.6 **  
-Fixes to Deck and how it's spitting out card openers.

** 1.2.5 **  
-Finished first Slate sprint.

** 1.0.0 **  
-Initializing Slate