<?php
/*
Module: Carousel
*/
?>

<?php
$optionheader = get_sub_field('optional_header');
$optionheadertext = get_sub_field('optional_header_text');

// module id
$addid = get_sub_field('add_moduleid');
$id = get_sub_field('module_id');

$carouseltype = get_sub_field('carousel_type');
$carouselclass = ' carousel--'.$carouseltype;

if ($addid && $id) {
	$carouselid = $id;
} else {
	$carouselid = 'carousel--'.$carouseltype;
}

$carouselwidth = get_sub_field('carousel_width');
?>



<?php
	
echo '<div class="module carousel'.$carouselclass.'">';

?>

		<div class="inner expanded">

			<?php

			if ($optionheader) {
			  echo '<div class="module__header">';
			    echo '<h1>'.$optionheadertext.'</h1>';
			  echo '</div>';
			}
			?><!-- end .module__header -->

			<?php

			// CUSTOM CAROUSEL
			if($carouseltype == 'custom') {	
				if( have_rows('carousel_repeater') ) { 

				// Start Counter for Slides
				$repeaterdata = get_field_object('carousel_repeater');
				$slidecount = (count($repeaterdata['value']));

				?>

					<div class="slider__container">
						<?php
						echo '<div id="'.$carouselid.'">';
							
							while( have_rows('carousel_repeater') ) { the_row(); 
							$slidecount++;
							// Carousel Fields
							$customheader = get_sub_field('custom_header');
							$customsubhead = get_sub_field('custom_subhead');
							$customparagraph = get_sub_field('custom_paragraph');

							$addbutton = get_sub_field('add_button');
						 	$button = get_sub_field('button');
						 	$buttonselect = $button['button_target_select'];
						 	$buttontarget = $button['button_target'];
						 	$buttontext = $button['button_text'];
						 	$modaltarget = $button['modal_target'];

							?>
								<li class="slide slide--custom">
									<?php

									// Slide Holder
									echo'<div class="slide__holder slide--custom--'.$slidecount.'" data-equalizer-watch="carousel--custom">'; 

										// Slide Header
										echo '<header class="slide__header">';
											if ($customsubhead) {
												echo '<h3>'.$customsubhead.'</h3>';
											} 
											
											if ($customheader) {
												echo '<h2>'.$customheader.'</h2>';
											}
										echo '</header>';

										// Paragraph
										if ($customparagraph) {
											echo '<p>'.$customparagraph.'</p>';
										}

										// Button
										if ($addbutton) {
										   if($buttonselect === 'modal') {
										   	echo '<a class="button button--carousel button--modal" data-open="'.$modaltarget.'" onClick="ga(\'send\', \'event\', \'modal button\', \'opens the modal\');">'.$buttontext.'</a>';
										   } else {
										   	echo '<a href="'.$buttontarget.'" class="button button--carousel">'.$buttontext.'</a>';
										   }// if modal
										}// /if addbutton

									echo '</div>';// /Slide Holder ?>

								</li><!-- /.slide -->

							<?php }// /while have_rows ?>
						</div><!-- /#carouselid -->
							
							<?php
							// bx slider controls
							if ($class) {
								echo '<div id="'.$class.'-slider-prev" onClick="ga(\'send\', \'event\', \'carousel\', \'previous slide\');"></div>';
								echo '<div id="'.$class.'-slider-next" onClick="ga(\'send\', \'event\', \'carousel\', \'next slide\');"></div>';
							} else {
							echo '<div id="slider-prev" onClick="ga(\'send\', \'event\', \'carousel\', \'previous slide\');"></div>';
							echo '<div id="slider-next" onClick="ga(\'send\', \'event\', \'carousel\', \'next slide\');"></div>';
							}
							?>
					</div><!-- /.slider__container -->

				<?php } // /if have_rows
			}// /CUSTOM CAROUSEL



			// TESTIMONIAL CAROUSEL
			if($carouseltype == 'testimonial') {
				$posts = get_sub_field('testimonial_select');

				if( $posts ) { 
					echo '<div class="carousel__testimonial large-'.$carouselwidth.' medium-'.$carouselwidth.' small-12 large-centered medium-centered">';
					    echo '<h1 class="carousel__header"><?php echo $carouselheader ?></h1>';

					   	echo '<div class="slider__container">';
						    // call bx-slider on $id
						    echo '<div id="'.$carouselid.'">';

						    foreach( $posts as $post) { // variable must be called $post (IMPORTANT) ?>
							        <?php setup_postdata($post); ?>
							        <li class="slide slide--testimonial">
							            <?php
							            echo '<blockquote>&ldquo;' . get_the_content() . '&rdquo;';
										echo '&nbsp;&nbsp;<span class="author">' . get_field('quoted_person') . '</span>';
										echo '</blockquote>';
										?>
							        </li>
							    <?php } ?>
						    </div><!-- /.carouselclass -->
						</div><!-- /.slider__container -->

					</div>
				<?php
				}// /if $posts ?>

	   				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php
			}// TESTIMONIAL CAROUSEL


			
			// PAGE CAROUSEL
			if($carouseltype == 'pageitems') {
				$posts = get_sub_field('pageitems_select');
				$blurb = get_field('blurb_text', $post->ID);

				// Start Post Counter
				$postdata = get_field_object('pageitems_select');
				$slidecount = (count($postdata['value']));
				
				if( $posts ) { 
					if ($carouselheader) {
				    	echo '<h1 class="carousel__header">'.$carouselheader.'</h1>';
				    	} ?>
				    
				    <?php

				    echo '<div class="slider__container">';
					    // call bx-slider on $carouselclass
					    echo '<div id="'.$carouselid.'">';
						    
						    foreach( $posts as $post) { // variable must be called $post (IMPORTANT) ?>
						        <?php
						        $slidecount++;// add count 
						        setup_postdata($post);
						        ?>
						        <li class="slide slide--pageitems">
							        <?php
							        echo '<div class="slide__holder slide--pageitems--'.$slidecount.'" data-equalizer-watch="columns">';
							            
							            $blurb = get_field('blurb_text', $post->ID);
							            
							            echo '<h1>'. get_the_title() .'</h1>';
							            
							            if ($blurb) {
							            	echo '<div class="slide__holder__text">
								            		<p>'. $blurb .'</p>
								            	</div>';
							            }

										echo '<a href="'. get_the_permalink() .'" class="button button--pageitems">Learn More</a>';
										?>
						        	</div><!-- /.slide__holder -->

						        </li>
						    <?php
						    }// /foreach $posts ?>
					    </div><!-- /$carouselclass /bx-slider -->
					</div><!-- /.slider__container -->

				<div class="bx-controls bx-controls--pageitems">
					<div id="pageitems-prev" onClick="ga('send', 'event', 'carousel', 'previous slide');"></div> 
					<div id="pageitems-next" onClick="ga('send', 'event', 'carousel', 'next slide');"></div>
				</div><!-- /.bx-controls -->

				<?php
				}// /if $posts ?>

					<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php
			}// /PAGE CAROUSEL ?>


		</div><!-- end .inner -->
</div><!-- end .module .carousel -->


