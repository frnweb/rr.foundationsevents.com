<?php
/*
Module: Wrapper Open
*/

// module class/id
$addclass = get_sub_field('add_moduleclass');
$addid = get_sub_field('add_moduleid');
$class = get_sub_field('module_class');
$id = get_sub_field('module_id');

if ($addclass && $class) {
$moduleclass = ' '.$class.'';
}
if ($addid && $id) {
$moduleid = ' id="'.$id.'"';
}

echo '<div'.$moduleid.' class="wrapper'.$moduleclass.'">';