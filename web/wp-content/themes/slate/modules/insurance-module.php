<?php
/*
Module: Insurance
*/


// module class/id
$addclass = get_sub_field('add_moduleclass');
$addid = get_sub_field('add_moduleid');
$class = get_sub_field('module_class');
$id = get_sub_field('module_id');

if ($addclass) {
$moduleclass = ' '.$class.'';
}
if ($addid) {
$moduleid = ' id="'.$id.'"';
}

// header
$optionheader = get_sub_field('optional_header');
$optionheadertext = get_sub_field('optional_header_text');

// paragraph
$optionparagraph = get_sub_field('optional_paragraph');
$optionparagraphtext = get_sub_field('optional_paragraph_text');

$insurancebutton = get_sub_field('insurance_button');
$buttonselect = $insurancebutton['button_target_select'];
$buttontext = $insurancebutton['button_text'];
$buttontarget = $insurancebutton['button_target'];
$modaltarget = $insurancebutton['modal_target'];

$width = get_sub_field('column_width');
$logosobject = get_sub_field_object('insurance_logos');
$logos = get_sub_field('insurance_logos');

?>


<?php
	echo '<div'.$moduleid.' class="module insurance'.$moduleclass.'">';
?>
	<div class="inner expanded">	
		
		<?php
		// MODULE HEADER
		if ($optionheader) { ?>
			<div class="module__header">
				<h1><?php echo $optionheadertext ?></h1>
			</div>	 	
		<?php
		}// /MODULE HEADER 

		// PARAGRAPH
		if ($optionparagraph) { ?>
			<div class="module__paragraph">
				<p><?php echo $optionparagraphtext ?></p>
			</div>
		<?php
		}// /PARAGRAPH


		// INSURANCE GRID
		echo '<div class="row insurance__grid large-up-'.$width.' medium-up-'.$width.' small-up-12">';
		
		foreach ($logosobject['value'] as $logo ){
			// .insurance__logo
		    echo '<div class="column large-centered insurance__logo">';
		    	echo '<img src="'.get_stylesheet_directory_uri().'/assets/insurance-logos/'.$logo['value'].'" alt="'.$logo['label'].'"/>';
		    echo '</div>';
		    // /.insurance__logo
		}// /foreach
		
		echo '</div>'; // /.row
		// /INSURANCE GRID
		
		// Button
		if ($buttontext) {
			if($buttonselect === 'modal') {
				echo '<a class="button button--insurance text-center button--modal" data-open="'.$modaltarget.'" onClick="ga(\'send\', \'event\', \'modal button\', \'opens the modal\');">'.$buttontext.'</a>';
			} else {
				echo '<a href="'.$buttontarget.'" class="button button--insurance text-center">'.$buttontext.'</a>';
			}// /if modal
		}// /if buttontext
		?>

	</div><!-- end .inner -->
</div><!-- end .insurance -->


