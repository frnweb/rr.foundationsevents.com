<?php
/*
Module: Content Editor
*/
?>

<?php
	// module class/id
  $addclass = get_sub_field('add_moduleclass');
  $addid = get_sub_field('add_moduleid');
  $class = get_sub_field('module_class');
  $id = get_sub_field('module_id');
  
  if ($addclass) {
    $moduleclass = ' '.$class.'';
  }
  if ($addid) {
    $moduleid = ' id="'.$id.'"';
  }
	
	$wysiwygcontent = apply_filters('the_content', get_sub_field('wysiwyg_content'));

// If Module Class
if ($addclass) {
echo '<div'.$moduleid.' class="module wysiwyg'.$moduleclass.'">';
} else {
echo '<div class="module wysiwyg">';
}// /If Module Class ?>

	<div class="inner">
		<div class="entry-content">
			
				<?php
					echo $wysiwygcontent;
				?>
		</div><!-- end entry content -->	
	</div><!-- end inner  -->
</div><!-- end module -->