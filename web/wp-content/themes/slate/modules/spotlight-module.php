<?php
/*
Module: Spotlight
*/
?>

<?php
	// module class/id
	$addclass = get_sub_field('add_moduleclass');
	$addid = get_sub_field('add_moduleid');
	$class = get_sub_field('module_class');
	$id = get_sub_field('module_id');
	
	if ($addclass) {
		$moduleclass = ' '.$class.'';
	}
	if ($addid) {
		$moduleid = ' id="'.$id.'"';
	}

	// background image 
	$addbackground = get_sub_field('add_background');
	$backgroundimage = get_sub_field('background_image');

	if ($addbackground) {
		$background = ' style="background-image: url('.$backgroundimage['url'].');"';
		
		if ($addbackground && $addclass) {
			$moduleclass = ' spotlight--background '.$class.'';
		} else {
			$moduleclass = ' spotlight--background';
		}
	}

 	$cardheader = get_sub_field('card_header');
	$cardsecondary = get_sub_field('card_secondary');
	$cardsubtext = apply_filters('the_content', get_sub_field('card_secondary_text'));
	
	// button one
	$addbutton = get_sub_field('add_button');
	$button = get_sub_field('button');
	$buttonselect = $button['button_target_select'];
	$buttontarget = $button['button_target'];
	$buttontext = $button['button_text'];
	$modaltarget = $button['modal_target'];

	// button two
	$addbuttontwo = get_sub_field('second_button');
	$buttontwo = get_sub_field('button_secondary');
	$buttontwoselect = $buttontwo['button_target_select'];
	$buttontwotarget = $buttontwo['button_target'];
	$buttontwotext = $buttontwo['button_text'];
	$modaltwotarget = $buttontwo['modal_target'];

	$addphone = get_sub_field('add_phone_number');
?>

<?php

echo '<div'.$moduleid.' class="module spotlight'.$moduleclass.'"'.$background.'>';

?>

	<div class="inner expanded">
		<div class="card card--spotlight">
			
				<?php 

				// Header
				if ($cardheader){
					echo '<h1>'.$cardheader.'</h1>';
				}
				
			    	
		    	// Phone Number
		    	if($addphone == "true") { 
					echo '<h1 class="phone phone--spotlight">'.do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Spotlight Module"]').'</h1>';
				}
				
				// Subhead/Paragraph
				if ($cardsubtext) {
					echo '<div class="card__secondary card__secondary--spotlight">';
				    	echo '<'.$cardsecondary.'>';
				    		echo $cardsubtext;
				    	echo '</'.$cardsecondary.'>';
			    	echo '</div>';
				}
				
				// Button
				if ($addbutton) {

					if($buttonselect === 'modal') {
						echo '<a class="button button--modal" data-open="'.$modaltarget.'" onClick="ga(\'send\', \'event\', \'modal button\', \'opens the modal\');">'.$buttontext.'</a>';
					} else {
						echo '<a href="'.$buttontarget.'" class="button button--spotlight">'.$buttontext.'</a>';
					}
				}// /if addbutton

				// Second Button
				if($addbuttontwo) { 
					if($buttontwoselect === 'modal') {
						echo '<a class="button button--modal" data-open="'.$modaltwotarget.'" onClick="ga(\'send\', \'event\', \'modal button\', \'opens the modal\');">'.$buttontwotext.'</a>';
					} else {
				    	echo '<a href="'.$buttontwotarget.'" class="button button--secondary">'.$buttontwotext.'</a>';
					}
				}// /if secondbutton 
				?>

		</div><!-- /.card-spotlight -->
	</div><!-- /.inner -->
</div><!--  /.module.spotlight -->

