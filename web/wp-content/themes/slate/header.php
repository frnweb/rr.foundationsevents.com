<!doctype html>
<html class="no-js"  <?php language_attributes(); ?>>
	<head>
		<meta charset="utf-8">
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">
		
		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
		<!-- Icons & Favicons -->
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
		<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/win8-tile-icon.png">
		<meta name="theme-color" content="#121212">
		<?php } ?><!-- this ends the conditional statement for the favicon -->
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
		<?php wp_head(); ?>
		
		<!-- Drop Google Analytics here -->
		<!-- end analytics -->
	</head>
	
	
	<?php

	//Start defining variables for building the header
	$navtype = get_field('nav_type', 'option');
	$mobilenav = get_field('mobile_navtype', 'option');
	$searchtype = get_field('search_type', 'option');
	$dropdowntype = get_field('dropdown_type', 'option');
	$headeractive = get_field('header_on_off', 'option');

	// Sticky
	$sticky = get_field('sticky_option', 'option');

	if ($sticky) {
		$stickycontainer = '<div class="stickynav--container" data-sticky-container>';
		$stickynavigation = ' data-sticky data-options="marginTop:0;" style="width:100%;"';
	}

	?>
	
	<!-- Body -->
	<body <?php body_class(); ?>>
		

		<?php
		if($mobilenav == 'offcanvas') {
		// Off-Canvas Wrappers
		echo '<div class="off-canvas-wrapper">';// closes in footer.php
			echo '<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>';// closes in footer.php
				get_template_part( 'parts/mobile', 'menu' );
					echo '<div class="off-canvas-content" data-off-canvas-content>';// closes in footer.php
		}// /Off-Canvas Wrappers
		?>
				
						<?php
						// Top Anchor for Sticky
						if($sticky) {
							echo '<div id="top_anchor" style="visibility: hidden;"></div>';
						}

						if($headeractive == false) {

						} else {


						// START HEADER
						echo $stickycontainer;
						echo '<header id="globalheader" class="header" role="banner"'.$stickynavigation.'>';

							// Stacked
							if ($navtype == 'stacked') {
								get_template_part( 'parts/nav', 'stacked');
							}

							// Title-Bar
							if ($navtype == 'title-bar') {
								get_template_part( 'parts/nav', 'title-bar');
							}

							// Default
							if ($navtype == 'default') {
								get_template_part( 'parts/nav', 'default');
							}
						?>
																
						</header> <!-- end .header -->
						
						<!-- Grabs the Search Popup -->
						<?php
						if($searchtype == 'search-icon') { 
							get_template_part( 'parts/modal', 'search');
						} ?>
						<!-- End Search Popup -->

						<?php 
							if ($sticky) {
								echo '</div>';
							}

						}// /if $headeractive
						?>











