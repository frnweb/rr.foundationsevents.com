<div id="post-not-found" class="hentry">
	
	<?php if ( is_search() ) : ?>
		
		<header class="post-header">
			<h1><?php _e( 'Sorry, No Results.', 'jointswp' );?></h1>
		</header>
		<div class="callout">
		<section class="search__result">
			<h5><?php _e( 'Try your search again.', 'jointswp' );?></h5>
		</section>
		
		<section class="search">
		    <p><?php get_search_form(); ?></p>
		</section> <!-- end search section -->
	</div>
		
	<?php else: ?>
	
		<header class="post-header">
			<h1><?php _e( 'Oops, Post Not Found!', 'jointswp' ); ?></h1>
		</header>
		<div class="callout">
		<section class="search__result">
			<h5><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'jointswp' ); ?></h5>
		</section>
		
		<section class="search">
		    <p><?php get_search_form(); ?></p>
		</section> <!-- end search section -->
		</div>
			
	<?php endif; ?>
	
</div>
