<!-- this is the mobile off canvas menu -->
<div class="off-canvas position-right" id="off-canvas" data-off-canvas data-position="right">
	<div id="offcanvas_search">
	<?php get_search_form(); ?>
    </div><!-- end offcanvas search -->
	<?php joints_off_canvas_nav(); ?>
    <div id="userbased-mobile">
   <?php wp_nav_menu( array( 'theme_location' => 'user-links' ) ); ?>
    </div>
    <div class="offcanvas-phone">Confidential and Private  <span class="phone"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in off canvas mobile menu"]'); ?></span></div>

</div>