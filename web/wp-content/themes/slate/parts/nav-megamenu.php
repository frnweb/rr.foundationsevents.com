  <ul class="mega-menu vertical medium-horizontal menu  dropdown">
         <?php
		 		 $locations = get_nav_menu_locations();
					 
                    if ( isset( $locations[ 'mega_menu' ] ) ) {
                        $menu = get_term( $locations[ 'mega_menu' ], 'nav_menu' );
                        if ( $items = wp_get_nav_menu_items( $menu->name ) ) {
                            foreach ( $items as $item ) {
                                
								if ( is_active_sidebar( 'mega-menu-widget-area-' . $item->ID ) ) {
                                       echo "<li class='has-dropdown-arrow'>";
									     echo "<a href=\"{$item->url}\" class='mm_link'>{$item->title}</a>";
                                    }
									
								else {
								 echo "<li>";
								 echo "<a href=\"{$item->url}\">{$item->title}</a>";
								}
								
                                   
                                    if ( is_active_sidebar( 'mega-menu-widget-area-' . $item->ID ) ) {
                                        echo "<div id=\"mm_wrapper-{$item->ID}\" class='mega-menu-wrapper'>";
										echo "<div id=\"mega-menu-{$item->ID}\" class=\"mega-menu\">";
											dynamic_sidebar( 'mega-menu-widget-area-' . $item->ID );
                                        echo "</div>";//end mega-menu
										echo "</div>";//end mm_wrapper
                                    }
									
								echo "</li>";
                            }
                     
					    }
                    	
					}
			
                ?>
</ul><!-- end megamenu -->