
<!-- nav-stacked -->

<?php

//Start defining variables for building the header
$navtype = get_field('nav_type', 'option');
$mobilenav = get_field('mobile_navtype', 'option');
$searchtype = get_field('search_type', 'option');
$dropdowntype = get_field('dropdown_type', 'option');

?>

<?php get_template_part( 'parts/mobile', 'header' ); ?>

<?php
if($mobilenav == 'popover') {
	echo '<div class="top-bar has-title-bar" id="top-bar-menu">';
		get_template_part( 'parts/mobile', 'menu');
	echo '</div>';
} ?>

<div id="top-section" class="show-for-medium">
	<div class="inner">
		<div class="row expanded collapse">
			
			<div id="leftcontent" class="large-4 medium-3 small-9 columns">
				<?php get_template_part( 'parts/pieces/header', 'logo' ); ?>
			</div><!-- /#left content -->
					
					
			<div id="rightcontent" class="large-8 medium-9 columns hide-for-small-only">		
				<div class="row expanded large-collapse medium-collapse">
					
					<!-- USERNAV -->
					<?php get_template_part( 'parts/pieces/header', 'usernav' ); ?>
					
					<!-- CALLINFO -->
					<?php get_template_part( 'parts/pieces/header', 'phone' ); ?>

				</div><!-- /.row -->
										
										
			</div><!-- /#right content -->
		</div> <!-- /.row expanded -->
	</div>  <!-- /.inner -->							
</div><!-- end #top-section -->
											

<div class="top-bar show-for-medium" id="top-bar-menu">
	<div class="inner">

		<div class="top-bar-left show-for-medium">
			<?php 
			// MEGAMENU
			if($dropdowntype == 'megamenu') {
				get_template_part( 'parts/nav', 'megamenu' ); 
			}
			
			// DROPDOWN
			if($dropdowntype == 'dropdown') {
				joints_top_nav();
			} ?>
		</div><!-- /.top-bar-left -->

		<div class="top-bar-right show-for-medium">
			<a class="search button" data-toggle="header__searchform" onClick="ga('send', 'event', 'search button', 'opens search bar');"></a>
		</div><!-- /.top-bar-right -->

	</div><!-- end .inner -->
</div><!-- end .top-bar -->
<!-- END Nav-Stacked -->