<?php 
			$staffphoto = get_field('staff_photo');
			$firstname = get_field('first_name');
			$lastname = get_field('last_name');
			$title = get_field('title');
			$certifications = get_field('certifications');
					
				echo '<article id="post-'.get_the_ID().'"';
				echo post_class('');
				echo 'role="article" itemscope itemtype="http://schema.org/BlogPosting">';
				echo '<section class="entry-content staff__member" itemprop="articleBody">';
				echo '<div class="row expanded collapse">';
					if( !empty($staffphoto) ){
					 echo '<div class="columns large-4 medium-6 small-12">';
					 echo '<div class="staff__member__info">';
					 echo '<div class="staff__member__info__photo">';	
					 echo '<img src="'.$staffphoto['url'].'" alt="'.$staffphoto['alt'].'" />';
					 echo '</div>';//end staff__member__info__photo	
					 echo '<h2>';
						   if ($firstname) {
							echo $firstname;
							}
							if ($lastname) {
							echo '&nbsp;'.$lastname.'';
							}
							if ($certifications) {
							echo '<span class="certifications">&nbsp;'.$certifications.'</span>';
							}
					 echo '</h2>';
						 if ($title) { 
							 	echo '<h3>'.$title.'</h3>';
						 }
					 
					 echo '</div>';//end staff__member__info
					 echo '</div>'; //end the first column with the image 
					 echo '<div class="columns large-8 medium-6 small-12">';
					 echo the_content();
					 echo '</div>'; //end the first column with the bio

					}
					
					else {//conditional statement for if they don't have a bio photo 
						echo '<div class="columns large-12">';
						echo '<h2>';
						   if ($firstname) {
							echo $firstname;
							}
							if ($lastname) {
							echo '&nbsp;'.$lastname.'';
							}
							if ($certifications) {
							echo '<span class="certifications">&nbsp;'.$certifications.'</span>';
							}
					 echo '</h2>';
						 if ($title) { 
							 	echo '<h3>'.$title.'</h3>';
						 }
					 echo the_content();
					 echo '</div>'; //end the first column with the bio
					}///end the else conditional statement
					
					echo '</div>'; //end row 
					echo '</section>';//end section
					echo '</article>';//end article
					

?>