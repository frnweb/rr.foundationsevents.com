<!-- Mega-Menu Nav with Offcanvas-topbar -->

<?php 

//Start defining variables for building the header
$navtype = get_field('nav_type', 'option');
$mobilenav = get_field('mobile_navtype', 'option');
$searchtype = get_field('search_type', 'option');
$dropdowntype = get_field('dropdown_type', 'option');
$navsecondary = get_field('nav_secondary', 'option');

?>

<?php get_template_part( 'parts/mobile', 'header' ); ?>

<?php 
	echo '<div class="top-bar has-title-bar top-bar--'.$dropdowntype.'" id="top-bar-menu">';
?>
	
	<!-- LEFT SECTION -->
	<div class="top-bar-left show-for-medium">
		<?php get_template_part( 'parts/pieces/header', 'logo' ); ?>
	</div>
	
	<!-- RIGHT SECTION -->
	<div class="top-bar-right show-for-medium">

		<!-- Desktop-Menu -->
		<div class="title-bar__menu show-for-medium">
			<?php 
			// MEGAMENU
			if($dropdowntype == 'megamenu') {
				get_template_part( 'parts/nav', 'megamenu' ); 
			}
			
			// DROPDOWN
			if($dropdowntype == 'dropdown') {
				joints_top_nav();
			} ?>
		</div><!-- /.title-bar__menu -->
		<!-- /Desktop-Menu -->

		<?php 
		// NAV SECONDARY
			get_template_part( 'parts/nav', 'secondary' );
		?>

	</div><!-- /.top-bar-right -->
	<!-- /RIGHT SECTION -->


	<?php
	if($mobilenav == 'popover') {
		get_template_part( 'parts/mobile', 'menu');
	} ?>

</div><!-- /.top-bar -->
<!-- END Nav-Title-Bar -->
