

<!-- Logo -->

<?php

$footerlogo = get_field('footer_logo', 'option');

echo '<a href="'.home_url('/').'">';
	if($footerlogo) {
		echo '<img src="'.$footerlogo.'" alt="'.$sitetitle.'" class="img-responsive" id="logo" />';			
	} else {
		echo '<img src="'.get_template_directory_uri() . '/assets/images/generic_logo.svg'.'" alt="'.$sitetitle.'" class="img-responsive" id="logo" />';
	}
echo '</a>'; ?>