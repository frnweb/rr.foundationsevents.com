   <div class="fullscreen-bg">
   
     <video loop muted autoplay poster="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/video-holder.jpg" class="fullscreen-bg__video">
    	<source src="<?php echo get_stylesheet_directory_uri(); ?>/assets/video/video-background.webm" type="video/webm">
   		 <source src="<?php echo get_stylesheet_directory_uri(); ?>/assets/video/video-background.mp4" type="video/mp4">
    	</video>
    
 
   </div> 