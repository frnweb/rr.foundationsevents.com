
<section id="location" role="contentinfo">
  <div class="inner section">
     <?php
            
      // Settings
      $sectionheader = get_field('location_section_header', 'option');
      $optionheader = $sectionheader['optional_header'];
      $optionheadertext = $sectionheader['optional_header_text'];

      // Content
      $addressselect = get_field('address_select', 'option');
      $altphonecheck = get_field('altphone_check', 'option');
      $altphonetext = get_field('altphone_text', 'option');
      $altphonenumber = get_field('altphone_number', 'option');
      $addresstext = apply_filters('the_content', get_field('address_text', 'option'));
      
      $button = get_field('contact_page_button','option');
      $buttontarget = $button['button_target'];
      $buttontext = $button['button_text'];

      // Map Fields
      $facilitylocation = get_field('facility_location', 'option'); 
      $mapview = get_field('map_view', 'option');
      $mapwidth = get_field('map_width', 'option');
      $cardwidth = (12 - $mapwidth);
      
      
      

      // MODULE HEADER
      if ($optionheader) { ?>
        <div class="location__header">
          <h1><?php echo $optionheadertext ?></h1>
        </div><!-- end module header -->    
      <?php 
      }// /MODULE HEADER ?>


    <div class="row expanded" data-equalizer data-equalize-on="medium">
        
        <?php 

        // LOCATION MAP
        if($mapview == "left") {
          echo '<div class="location__map large-'.$mapwidth.' medium-'.$mapwidth.' columns">';
        }

        if($mapview == "right") {
          echo '<div class="location__map large-'.$mapwidth.' medium-'.$mapwidth.' large-push-'.$cardwidth.' medium-push-'.$cardwidth.' columns">';
        }

          // Map Repeater
          if( have_rows('facility_location', 'option')) {
            echo '<div class="acf-map" data-equalizer-watch>';
              
              while ( have_rows('facility_location', 'option') ) { the_row();

                $location = get_sub_field('the_map');
                $popupheader = get_sub_field('popup_header');
                $popupselect = get_sub_field('popup_select');
                $popupaddresstext = get_sub_field('popup_address_text');
                $popupaddbutton = get_sub_field('popup_add_button');
                $popupbuttontext = get_sub_field('popup_button_text');
                $popupbuttontarget = get_sub_field('popup_button_target');
                ?>
                
                <!-- START POPUP -->
                <div class="contact__marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
                  <div class="contact__popup">
                    <?php
                    // Header
                    if ($popupheader) {
                      echo '<h2>'.$popupheader.'</h2>';
                    }
                    // Address
                    if ($popupselect == "facility_address") { ?>
                      
                      <!-- Address -->
                      <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <span itemprop="streetAddress"><?php the_field('street_address', 'option'); ?></span> <br />
                        <span itemprop="addressLocality"><?php the_field('city_state', 'option'); ?></span> 
                        <span itemprop="postalCode"><?php the_field('zip_code', 'option'); ?></span>
                      </p>
                      
                      <!-- Telephone -->
                      <p><span itemprop="telephone"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Custom module Popup"]'); ?></span>
                        <?php if ($altphonecheck) {
                          echo 'or <span itemprop="telephone">';
                            echo '<a href="tel:'.$altphonenumber.'">'.$altphonetext.'</a>';
                          echo '</span>';
                        } ?>
                      </p>

                    <?php } // /if facility_address

                    // Custom Address
                    if ($popupselect == "custom_address") {
                      echo '<p>'.$popupaddresstext.'</p>';
                    }
                    
                    // Button
                    if ($popupaddbutton) {
                      echo '<a class="button" href="'.$popupbuttontarget.'" target="_blank">'.$popupbuttontext.'</a>';
                    } ?>


                  </div><!-- /.contact__popup -->
                </div><!-- /.contact__marker -->
              
              <?php } // while has_rows

            echo '</div>';// /.acf-map

          } // /if has_rows

        echo '</div>'; // /.location__map.columns

    



      // LOCATION CONTENT
      if($mapview == "left") {
        echo '<div class="large-'.$cardwidth.' medium-'.$cardwidth.' columns location__content" itemscope itemtype="http://schema.org/Organization" data-equalizer-watch>';
      }

      if($mapview == "right") {
        echo '<div class="large-'.$cardwidth.' medium-'.$cardwidth.' large-pull-'.$mapwidth.' medium-pull-'.$mapwidth.' columns location__content" itemscope itemtype="http://schema.org/Organization" data-equalizer-watch>';
      } ?>
  
          <!-- Content -->
          <div class="card card--location">
            
            <?php
            // Facility Address
            if ($addressselect == "facility") { ?>
              <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <span itemprop="streetAddress"><?php the_field('street_address', 'option'); ?></span> <br />
                <span itemprop="addressLocality"><?php the_field('city_state', 'option'); ?></span> 
                <span itemprop="postalCode"><?php the_field('zip_code', 'option'); ?></span>
              </p>
            
              <?php 
              // Phone Numbers
              echo '<p><span itemprop="telephone">'.do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Contact Module"]').'</span>';

                // Alt Phone Number
                if ($altphonecheck) {
                  echo ' or <span itemprop="telephone">';
                    echo '<a href="tel:'.$altphonenumber.'" onClick="ga("send", "event", "phone", "alternate phone number");">'.$altphonetext.'</a>';
                  echo '</span>';
                }
              echo '</p>';// /Phone Numbers

            }// /Facility Address


            // Custom Address
            elseif ($addressselect == "custom") {
              echo $addresstext;
            }// /elseif "custom"

          echo '</div>';// /card--location
          ?>

        </div><!-- // /.contact__content.columns -->

    </div><!-- /.row expanded -->

  </div><!-- end inner -->
</section><!-- end #location -->