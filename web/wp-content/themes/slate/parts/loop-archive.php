<?php 
	if (is_single()) {
	//no page title because it is doing it in another part 
	}

	else {
		echo get_template_part('parts/loop', 'pagetitle'); //grabs page title
	}

?>
		    	
<?php //this determines the layout and div containers for the archive type pages
                
	if ( is_search() || is_post_type_archive('staff') ) {///removes sidebar for search results and the staff archive page
		echo '<div id="content" class="inner page--resources page--search">';
		echo '<div id="inner-content" class="row expanded large-collapse medium-collapse">';
		echo '<main id="main" class="large-12 medium-12 columns first" role="main">';
	}
	
	elseif (is_singular( 'staff' )) {
		echo '<div id="content" class="inner page--staff page--staff--single">';
		echo '<div id="inner-content" class="row expanded large-collapse medium-collapse">';
		echo '<main id="main" class="large-12 medium-12 columns first" role="main">';
	}

	elseif (is_single()) {
        echo '<div id="content" class="inner page--resources">';
		echo '<div id="inner-content" class="row expanded large-collapse medium-collapse">';

			// start sidebar and main layouts
			$sidebarlayout = get_field('resources_sidebar_layout', 'option');
			$largesidebarwidth = get_field('large_resources_sidebar_width', 'option');
			$mediumsidebarwidth = get_field('medium_resources_sidebar_width', 'option');
			$largewidth = (12 - $largesidebarwidth);
			$mediumwidth = (12 - $mediumsidebarwidth);

				// sidebar
				if($sidebarlayout == 'left') {
					echo '<div id="sidebar1" class="sidebar large-'.$largesidebarwidth.' medium-'.$mediumsidebarwidth.' columns" role="complementary">';
				}

				if($sidebarlayout == 'right') {
					echo '<div id="sidebar1" class="sidebar large-'.$largesidebarwidth.' medium-'.$mediumsidebarwidth.' large-push-'.$largewidth.' medium-push-'.$mediumwidth.' columns" role="complementary">';
				}
					
				echo get_sidebar();
				echo '</div>';//end the sidebar

				// #main
				if($sidebarlayout == 'left') {
					echo '<main id="main" class="large-'.$largewidth.' medium-'.$mediumwidth.' columns has-sidebar" role="main">';
				}
				if($sidebarlayout == 'right') {
					echo '<main id="main" class="large-'.$largewidth.' medium-'.$mediumwidth.' large-pull-'.$largesidebarwidth.' medium-pull-'.$mediumsidebarwidth.' columns has-sidebar" role="main">';
				}

				}//end is single conditional tag 
				
				else {
                	echo '<div id="content" class="inner page--resources">';
					echo '<div id="inner-content" class="row expanded large-collapse medium-collapse">';
					
					// start sidebar and main layouts
					$sidebarlayout = get_field('resources_sidebar_layout', 'option');
					$largesidebarwidth = get_field('large_resources_sidebar_width', 'option');
					$mediumsidebarwidth = get_field('medium_resources_sidebar_width', 'option');

					$largewidth = (12 - $largesidebarwidth);
					$mediumwidth = (12 - $mediumsidebarwidth);

					// sidebar
					if($sidebarlayout == 'left') {
					echo '<div id="sidebar1" class="sidebar large-'.$largesidebarwidth.' medium-'.$mediumsidebarwidth.' columns" role="complementary">';
					}

					if($sidebarlayout == 'right') {
					echo '<div id="sidebar1" class="sidebar large-'.$largesidebarwidth.' medium-'.$mediumsidebarwidth.' large-push-'.$largewidth.' medium-push-'.$mediumwidth.' columns" role="complementary">';
					}
					echo get_sidebar();
					echo '</div>';//end the sidebar

					// #main
					if($sidebarlayout == 'left') {
					echo '<main id="main" class="large-'.$largewidth.' medium-'.$mediumwidth.' columns has-sidebar" role="main">';
					}
					if($sidebarlayout == 'right') {
					echo '<main id="main" class="large-'.$largewidth.' medium-'.$mediumwidth.' large-pull-'.$largesidebarwidth.' medium-pull-'.$mediumsidebarwidth.' columns has-sidebar" role="main">';
					}
				}
         ?><!-- end PHP tag for sidebar and main layouts -->
	
		<?php 
		$postsgridlarge = get_field('blog_post_grid_large', 'option');
		$postsgridmedium = get_field('blog_post_grid_medium', 'option');

		if ( is_single() || is_singular('staff') ) { 
			echo '<div class="row large-up-1">';
	
		}

		else {
			///this establishes the grid for search results, main posts page, archive page, and category page
			echo '<div class="row large-up-'.$postsgridlarge.' medium-up-'.$postsgridmedium.' small-up-1">';
			}
		?>
		
 <?php if (have_posts()) : while (have_posts()) : the_post(); //this is where we start to grab the content inside the loop ?>
	
	<?php 
		if ( is_single() || is_singular('staff') ) { 
			///for single staff page 
			echo '<div class="post_holder column column-block">';
			echo '<div id="post-'.get_the_ID().'" role="article">';
					
	}

		else {
			echo '<div class="post_holder column column-block">';
	 	 	echo '<div id="post-'.get_the_ID().'"';
			echo post_class( 0 === ++$GLOBALS['wpdb']->wpse_post_counter % 2 ? 'even' : 'odd' );//this adds even and odd classes to posts holder 
			echo 'role="article">';	//this counts the posts in the archive loop and it adds the class of even or odd 
				}
	?>
	  				
	<?php // This conditional statement is for grabbing the featured image and lining up the columns 
                
		if ( is_search() ) {
			//search result container 
					echo '<div class="post_excerpt">';
		}

	  	elseif (is_singular('staff') ) {
			///this does something different with the Single posts for Staff custom post type
					echo '<div class="post_excerpt">';///this div is closed back on the loop-archive section on line 177
					echo get_template_part('parts/staff', 'single');///layout for single staff member page layout

		}

		elseif (is_single() ) {
			///this does something different with the Single posts
					echo '<div class="post_excerpt">';//this closes down below on line 177
					echo get_template_part('parts/post', 'single');

		}
				
		elseif ( is_archive() && has_post_thumbnail() || has_post_thumbnail() ) {//this statement covers both the posts with featured images in the archive template and the posts page template
					//If it is not the single post page then we are taking the featured image and alternating them
					echo '<div class="post_excerpt">';
					echo '<div class="featured_image">';
                    echo '<a href="'.get_the_permalink().'">';
					echo the_post_thumbnail('full');
					echo '</a>';//end link wrapped around the featured image
					echo '</div>';///end featured image
        }
				
		else {
                	echo '<div class="post_excerpt">';
		}
                ?>
       
        		
    		
    		   <?php 
				if ( is_single() ) { 
					///does nothing
				}
			
				else {
					//inside this template part is the excerpt, title and share buttons 
					echo get_template_part( 'parts/pieces/loop', 'postcontents' );
				}
				
				?>
        
        		
            
        </div><!-- /.post_excerpt column column-block -->
   
 </div> <!-- end #post-->
 </div><!-- end post_holder -->
		
		<?php endwhile; ?>	
    
	    </div><!-- /.row expanded --><!-- all of these divs are being called on in the conditional statements above and should not be deleted -->
		
		<?php joints_page_navi(); ?>
					
		<?php else : ?>
											
		<?php get_template_part( 'parts/content', 'missing' ); ?>
						
		<?php endif; ?>
					
</main><!-- end main -->
</div><!-- end #inner content -->
</div><!-- end #content -->

    
   
						
				    						
