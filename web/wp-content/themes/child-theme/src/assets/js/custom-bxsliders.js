// START BX Slider For Carousels

// Custom Carousel
$(document).ready(function(){
  "use strict";
  $('#carousel--billboard').bxSlider({
	   onSliderLoad: function() {
          $('.slider__container').css('opacity', '1');

        },
  pager: false,
  autoReload: true,
  breaks: [{screen:0, slides:1, pager:false},{screen:639, slides:1},{screen:768, slides:1},{screen:1024, slides:1}]
});
});

// Custom Carousel
$(document).ready(function(){
	"use strict";
  $('#carousel--custom').bxSlider({
	   onSliderLoad: function() {
          $('.slider__container').css('opacity', '1');

        },
  nextSelector: '#slider-next',
  pager: false,
  prevSelector: '#slider-prev',
  nextText: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
  prevText: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
  autoReload: true,
  breaks: [{screen:0, slides:1, pager:false},{screen:639, slides:1},{screen:768, slides:2},{screen:1024, slides:2}]
  });


});

// Testimonial Carousel
$(document).ready(function(){
  "use strict";
  $('#carousel--testimonial').bxSlider({
	   onSliderLoad: function() {
          $('.slider__container').css('opacity', '1');

        },
  autoReload: true,
  breaks: [{screen:0, slides:1, pager:false},{screen:639, slides:1},{screen:768, slides:1},{screen:1024, slides:1}]
});
});

// Page Items Carousel
$(document).ready(function(){
  "use strict";
    $('#carousel--pageitems').bxSlider({
		 onSliderLoad: function() {
          $('.slider__container').css('opacity', '1');

        },
    pager: false,
    nextSelector: '#pageitems-next',
    prevSelector: '#pageitems-prev',
    nextText: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
    prevText: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
    slideMargin: 20,
    autoReload: true,
    breaks: [{screen:0, slides:1, pager:false},{screen:639, slides:1},{screen:768, slides:2},{screen:1024, slides:3}]
    });

});

// Gallery
$(document).ready(function(){
  "use strict";
  $('#gallery').bxSlider({
	   onSliderLoad: function() {
          $('.slider__container').css('opacity', '1');

        },
  auto: false,
  pager: false,
  nextText: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
  prevText: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
  breaks: [{screen:0, slides:1, pager:false},{screen:639, slides:1},{screen:768, slides:1},{screen:1024, slides:1}]
  });
});

// Duo Gallery
$(document).ready(function(){
  "use strict";
  $('#duo__gallery').bxSlider({
    onSliderLoad: function() {
      $('.slider__container').css('opacity', '1');
    },
  auto: true,
  pager: true,
  nextText: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
  prevText: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
  breaks: [{screen:0, slides:1, pager:true},{screen:639, slides:1},{screen:768, slides:1},{screen:1024, slides:1}]
  });

});



// END BX Slider For Carousels