// Put your custom Javacsrip functions here. Make sure they don't conflict with the Custom.js file in the Parent theme.

"use strict";
//Animates sections when they are reached on scroll- based on viewportchecker.js

$(document).ready(function() {
	"use strict";
	jQuery('.animated-section').addClass("hidden").viewportChecker({
    classToAdd: 'visible animated fadeInUp', // Class to add to the elements when they are visible
    offset: 100
	});
});


// $(document).ready(function() {
  // // Fix sticky nav on small and medium
  // var sticky = $("header.header#globalheader");
  // var breakpoint = Foundation.MediaQuery.current;

  // if (Foundation.MediaQuery.atLeast('medium')) {

  // } else {
  //   sticky.unwrap();
  //   sticky.foundation('destroy');
  // }
// });

// Mobile Phone Call Popover
$(document).ready(function() {
    //this animates the phone number in at the top on mobile
  "use strict";
    var mphone = $("#mobilephone");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 105) {
            mphone.removeClass('relative').addClass("fixed animated slideInDown");
        } else {
            mphone.removeClass("fixed animated slideInDown").addClass('relative');
        }
    });
});


// $(document).ready(function() {

//   // adding data-attr to nav items
//   var $navitems = $('.header #top-bar-menu #menu-global-nav-2 li a');
//   $("#menu-global-nav-2 li a").attr("data-hover", function(i, origValue){
//     return $(this).html();
//   });


//   // Dropdown for Main Navigation
//   $('#menu-global-nav-2').attr("data-closing-time", "0");

//   $('#menu-global-nav-2').on('show.zf.dropdownmenu', function (ev, $el) {
//     $el.css('display', 'block');
//     MotionUI.animateIn($el, 'fade-in fast');
//   });

//   $('#menu-global-nav-2').on('hide.zf.dropdownmenu', function () {
//     $('li ul.menu.vertical.submenu.is-dropdown-submenu').css('display', 'none');
//     MotionUI.animateOut($('li ul'), 'fade-out fast');
//   });
//   // /Dropdown

// });



////equalizes some of the modules that have to be initialized after the load
// Duo Gallery
$(document).ready(function(){
  "use strict";

///DUO equalizers
  $('.duo--image .row').attr("data-equalizer", "duo__equalizer");
  $('.duo--image .row').attr("data-equalize-on", "medium");

  $('.duo--video .row').attr("data-equalizer", "duo__equalizer");
  $('.duo--video .row').attr("data-equalize-on", "medium");
  $('.duo--video .embed-responsive-item').attr("data-equalizer-watch", "duo__equalizer");
  $('.duo--video .flex-video').attr("data-equalizer-watch", "duo__equalizer");

  $('.duo--gallery .row').attr("data-equalizer", "duo__equalizer");
  $('.duo--gallery .row').attr("data-equalize-on", "medium");
  $('.duo--gallery .bx-viewport').attr("data-equalizer-watch", "duo__equalizer");
  $('.duo--gallery li.slide').attr("data-equalizer-watch", "duo__equalizer");
  $('.duo--gallery li.slide').foundation();

  $('.duo .row').foundation();

///Carousel equalizers
  $('.carousel--custom .bx-viewport').attr("data-equalizer", "carousel--custom");
  $('.carousel--custom .bx-viewport').attr("data-equalize-on", "medium");
  $('.carousel--custom .bx-viewport').foundation();

  // Staff Carousels
  $('.staff--carousel .bx-viewport').attr("data-equalizer", "carousel--staff");
  $('.staff--carousel .bx-viewport').attr("data-equalize-on", "medium");
  $('.staff--carousel .bx-viewport').foundation();

	//carousel controls
	$('.carousel--pageitems .bx-viewport').attr("data-equalizer", "column");
    $('.carousel--pageitems .bx-viewport').attr("data-equalize-on", "medium");
    $('.carousel--pageitems .bx-viewport').foundation();
    $('.button--pageitems').css({
        "position": "absolute",
        "bottom": "0px",
        "left": "50%",
        "transform": "translateX(-50%)"
    });
});

//SVG ANIMATION TRIGGER
$(document).ready(function() {
	jQuery('.moc-logo').viewportChecker({
    classToAdd: 'play-animation', // Class to add to the elements when they are visible
    offset: 0
	});
});

$(document).ready(function() {
	jQuery('.moc-award').viewportChecker({
    classToAdd: 'play-animation', // Class to add to the elements when they are visible
    offset: 200
	});
});

//Getting rid of br tag in tabs title
$(document).ready(function(){
    $('ul.tabs .tabs-title a br:first-child').detach();
});


//Smooth Scroll for Page Jumps
// Select all links with hashes
$('#globalheader a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top - 90
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });


//CUSTOM STAFF MODULE JS
function openDivUnderAnchor(name){
	$('div.speaker__box').hide();

	if (name !== '') {
    	name = name.replace('#','');
    	$('a[name='+name+']').next('div').show();
	}
}///end function

//Staff Carousel
$(document).ready(function(){
  "use strict";
  $('#carousel--staff').bxSlider({
    onSliderLoad: function() {
      $('.staff__carousel').css('opacity', '1');///this adds opacity when the slider loads and prevents that weird snapping of bx-slider
      $('.staff__card a.open').on('click', function(event) {
	event.preventDefault();
        openDivUnderAnchor($(this).attr('href'));
        });

    },
  auto: false,
  preventDefaultSwipeX: true,
  pager: false,
  nextText: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
  prevText: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
  breaks: [{screen:0, slides:1, pager:false},{screen:639, slides:2},{screen:768, slides:3},{screen:1024, slides:4}]
  });

});
$(document).ready(function(){
//    // opens the correct div if its link is clicked
//    $('.staff__card a.open').on('click', function(event) {
//		    event.preventDefault();
//        openDivUnderAnchor($(this).attr('href'));
//    });
    // opens the correct div if its anchor is specified in the URL
    openDivUnderAnchor($('.staff__container .staff__card:not(.bx-clone):first a').attr('href'));
});


//REORDER PRICING DIVS
$(document).ready(function() {
  console.log(Foundation.MediaQuery.current);

  if (Foundation.MediaQuery.atLeast('medium')) {
      //Don't do anything
    }
  else {
    $('#pricing .card--basic:nth-of-type(2)').insertBefore('#pricing .card--basic:nth-of-type(1)');
  }
});

//CLOSE MOBILE MENU AFTER CLICK
$(document).ready(function () {
    $('#popover li a').on('click', function () {
        $('#mobilemenu .hamburger').click();
    });
    
//    $('#mobilemenu .hamburger').on('click', function() {
//        $("body").toggleClass("menu-open");
//        
//    });///end adding class to the body when menu is opened 
//    
//    var div = document.createElement("div");
//    div.id = "wrap";
//
//    // Move the body's children into this wrapper
//    while (document.body.firstChild)
//    {
//        div.appendChild(document.body.firstChild);
//    }
//
//    // Append the wrapper to the body
//    document.body.appendChild(div);
});
