module.exports = function(grunt) {
  // Configure task(s)
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    
    // uglify
    uglify: {
      dev: {
        options: {
          mangle: false,
          compress: false,
          beautify: true
        },
        src: [
          '../../slate/vendor/jquery/dist/jquery.min.js',
          '../../slate/vendor/what-input/what-input.min.js',
          '../../slate/vendor/foundation-sites/dist/foundation.min.js',
          '../../slate/assets/js/slate.min.js',
          'js/custom-bxsliders.js',
          'js/child.js',
          '!js/*.min.js'
        ],
        dest: 'js/child.min.js'
      },
    },// /uglify


    // sass
    sass: {
      options: {
        includePaths: [
        'node_modules/foundation-sites/scss',
        'node_modules/motion-ui'
        ],
        sourceMap: true,
        outFile: 'css/child.css'
      },
      dev: {
        files: {
                'css/child.css':'scss/main.scss'
                
        }
      }
    }, // /sass

    // postcss
    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({browsers: ['last 10 version', '> 10%', '> 5% in US', 'ie >= 9', 'and_chr >= 2.3']})
        ]
      },
      dev: {
        src: ['css/child.css', '!css/*.min.css']
      }
    },// /postcss


    // cssmin
    cssmin: {
      options: {
        restructuring: false,
        shorthandCompacting: false,
        roundingPrecision: -1,
        sourceMap: true
      },

      dev: {
        files: {
          'css/child.min.css': [
            '../../slate/assets/css/slate.min.css',
            'css/*.css',
            'css/child.css',
            '!css/ie.css',
            '!css/*.min.css'
            ]
        }
      }
    },// /cssmin

    // tinypng
    tinypng: {
      options: {
        apiKey: "4Ouz0gfzly_SxeykW3oPt1dwhFNKmU7e",
        checkSigs: true,
        sigFile: 'images/file_sigs.json',
        summarize: true,
        stopOnImageError: true
      },
      
      png: {
        expand: true,
        cwd: 'images/originals',
        src: ['**/*.png'],
        dest: 'images/compressed/',
        ext: '.min.png',
        extDot: 'first'
      },
      jpg: {
        expand: true,
        cwd: 'images/originals',
        src: ['**/*.jpg'],
        dest: 'images/compressed/',
        ext: '.min.jpg',
        extDot: 'first'
      }
    },// /tinypng


    // watch
    watch: {
      js: {
        files: ['js/*.js', '!js/*.min.js'],
        tasks: ['uglify:dev']
      },

      sass: {
        files: ['scss/*.scss', 'scss/modules/*.scss', 'scss/parts/*.scss'],
        tasks: ['sass:dev']
      },

      css: {
        files: ['css/*.css', '!css/*.min.css'],
        tasks: ['postcss', 'cssmin:dev'],
        options: {
          spawn: false
        }
      }
    }, // /watch


    // Browsersync
    browserSync: {
      bsFiles: {
        src: ['css/*.min.css', 'js/*.min.js']
      },
      options: {
            watchTask: true,
            proxy: 'http://rrfoundationseventscom.lndo.site/'
            
      }
    }// /Browsersync


  });




  // Load the plugins
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-tinypng');
  grunt.loadNpmTasks('grunt-postcss');

  // Register task(s).
  grunt.registerTask('default', ['uglify:dev', 'sass:dev', 'postcss', 'cssmin:dev', 'browserSync', 'watch']);


};