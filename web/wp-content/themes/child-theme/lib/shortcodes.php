<?php

/* 
Custom Shortcodes
 */

//SVG MOC LOGO 
function moc_logo() {
  ob_start();
  get_template_part('parts/pieces/svg', 'fe-logo');
  return '<div class="fe-logo">'. ob_get_clean() .'</div>';
}
add_shortcode('moc_logo', 'moc_logo');

//SVG MOC BULB
function moc_bulb() {
  ob_start();
  get_template_part('parts/pieces/svg', 'rr-trophy');
  return '<div class="svg svg--shortcode">'. ob_get_clean() .'</div>';
}
add_shortcode('moc_bulb', 'moc_bulb');

//SVG MOC HEART
function moc_heart() {
  ob_start();
  get_template_part('parts/pieces/svg', 'rr-key');
  return '<div class="svg svg--shortcode">'. ob_get_clean() .'</div>';
}
add_shortcode('moc_heart', 'moc_heart');

//SVG MOC NETWORK
function moc_network() {
  ob_start();
  get_template_part('parts/pieces/svg', 'rr-notebook');
  return '<div class="svg svg--shortcode">'. ob_get_clean() .'</div>';
}
add_shortcode('moc_network', 'moc_network');

//SVG MOC AWARD
function moc_award() {
  ob_start();
  get_template_part('parts/pieces/svg', 'moc-award');
  return '<div class="svg svg--shortcode">'. ob_get_clean() .'</div>';
}
add_shortcode('moc_award', 'moc_award');

//// Adds Container Around Content with Class
//function div_container ( $atts, $content = null ) {
//	$specs = shortcode_atts( array(
//		'class'		=> '',
//		), $atts );
//	return '<div class="container ' . esc_attr($specs['class']) . ' ">' .  do_shortcode ( $content ) . '</div>';
//}
//                
//add_shortcode ('container', 'div_container' );