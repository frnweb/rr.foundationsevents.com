# Slate Child Theme for Recovery Results

This is a Slate Child Theme specifically tied to [Recovery Results](http://rr.foundationsevents.com). Changes made to this will not affect any other site.

## Setup

Navigate to the theme directory in your terminal, then type the following:

```
yarn install
```

## Development

```
yarn run start
```

Running this command will launch your site with BrowserSync, which should automatically refresh anytime you change `src/` files.

**NOTE: This theme requires that your Lando instance be named `peachfordcom` in order for BrowserSync to proxy the correct web address.**

### Pre-Commit Git Hook

This theme installs a Git hook which runs a production build prior to **EVERY** commit and stages any relevant changes automatically!

This helps make sure that all of the built files are included with the site.
