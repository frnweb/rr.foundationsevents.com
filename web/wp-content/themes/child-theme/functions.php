<?php
namespace FRN\ChildTheme;

/**
 * Auto load all files in `./lib` directory
 */
foreach (glob(__DIR__ . '/lib/*.php') as $file) {
    require_once(\get_stylesheet_directory() . '/lib/' . basename($file));
}

/**
 * Wrap WordPress' `add_action()` to add our namespace
 */
function add_action($tag, $function, $priority = 10)
{
    \add_action($tag, __NAMESPACE__ . '\\' . $function, $priority);
}

/**
 * Main stylesheet callback
 */
function enqueue_stylesheet()
{
    \wp_enqueue_style(
        $name = 'child-css',
        $src = get_stylesheet_directory_uri() . '/dist/assets/css/main.css',
        $deps = [],
        $ver = false,
        $media = 'all'
    );
}

/**
 * Google Fonts callback
 */
function enqueue_fonts()
{
    \wp_enqueue_style(
        $name = 'google-fonts',
        $src = 'https://fonts.googleapis.com/css?family=Montserrat:300,600|Roboto:400,700',
        $deps = [],
        $ver = false,
        $media = 'all'
    );
}

/**
 * Main JavaScript callback
 */
function enqueue_scripts()
{
    \wp_enqueue_script(
        $name = 'child-js',
        $src = get_stylesheet_directory_uri() . '/dist/assets/js/app.js',
        $deps = [],
        $ver = false,
        $in_footer = true
    );

    // This must be run after the script is enqueued. It passes a variable
    // into the JavaScript `window` variable for use by other code in Slate.
    // NOT running it WILL break things. :)
    \wp_localize_script(
        $name,
        $object_name = 'themepath',
        $i18n = ['stylesheet_directory_uri' => get_stylesheet_directory_uri()]
    );
}

/**
 * Unload all the styles from Slate parent
 * 
 * We will import these later with Gulp and Sass
 */
function dequeue_styles()
{
    foreach ([
        'motion-ui-css',
        'foundation-css',
        'slate-css',
        'frn_plugin_styles'
    ] as $style) {
        \wp_dequeue_style($style);
        \wp_deregister_style($style);
    }
}

/**
 * Unload all the scripst from Slate parent
 * 
 * We will import these later with Gulp and Webpack.
 */
function dequeue_scripts()
{
    foreach ([
        'jQuery',
        'what-input',
        'slate-js',
        'foundation-js'
    ] as $script) {
        \wp_dequeue_script($script);
        \wp_deregister_script($script);
    }
}

/**
 * Register callbacks with appropriate hooks
 */
(function () {
    foreach ([
        'enqueue_stylesheet',
        'enqueue_fonts',
        'enqueue_scripts'
    ] as $function) {
        add_action('wp_enqueue_scripts', $function);
    }

    foreach ([
        'dequeue_styles',
        'dequeue_scripts',
    ] as $function) {
        add_action('wp_print_styles', $function);
    }

})();