<!-- this is the start of the default NAV -->

<?php

//Start defining variables for building the header
$navtype = get_field('nav_type', 'option');
$mobilenav = get_field('mobile_navtype', 'option');
$searchtype = get_field('search_type', 'option');
$dropdowntype = get_field('dropdown_type', 'option');

?>

<?php get_template_part( 'parts/mobile', 'header' ); ?>

<?php
if($mobilenav == 'popover') {
	echo '<div class="top-bar has-title-bar" id="top-bar-menu">';
		get_template_part( 'parts/mobile', 'menu');
	echo '</div>';
} ?>

<div id="top-section" class="show-for-large">
	<div class="inner expanded">
		<div class="row expanded collapse">
			
			<!-- LEFT -->
			<div id="leftcontent" class="large-3 medium-3 small-9 columns">
										
				<?php get_template_part( 'parts/pieces/header', 'logo' ); ?>
										
			</div><!-- /#left content -->
			<!-- /LEFT -->
								

			<!-- right now this is where the mobile header button and menu live -->
			
			<!-- RIGHT -->							
			<div id="rightcontent" class="large-9 medium-9 columns">								
				<div class="row expanded large-collapse medium-collapse">

					<!-- Nav Secondary -->						
					<?php get_template_part( 'parts/nav', 'secondary' ); ?>
				
				</div><!-- /.row -->
																
											
				<div class="top-bar" id="top-bar-menu">
					<?php 
					// MEGAMENU
					if($dropdowntype == 'megamenu') {
						get_template_part( 'parts/nav', 'megamenu' ); 
					}
					
					// DROPDOWN
					if($dropdowntype == 'dropdown') {
						joints_top_nav();
					} ?>
				</div><!-- end .top-bar -->												
			</div><!-- /#right content -->


		</div> <!-- /.row expanded -->
	</div>  <!-- /.inner -->
</div><!-- end #top-section -->
<!-- END Nav-Default -->