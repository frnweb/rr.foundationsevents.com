<?php defined( 'ABSPATH' ) OR die( 'No direct access.' );

/* NOTES:
	"part" php files are in the works in an attempt to make it easier to add to any theme without requiring the massive main plugin PHP file.
	Although this file is seperate, most of the other files depend or reference something from this file. 
	This file only includes frontend changes for users. 
	Admin features are still in the admin php.
	Move to it's own file on 3/7/17
	v 2.1


INCLUDED:
	Company IPs
	Google Analytics
	Chartbeat
	Hotjar
	PPC: 
		Yahoo
		Bing
		Facebook
	Contently
	Outbrain

*/









////////////////////////
// COMPANY_IPs TEST   //
////////////////////////

if(!function_exists('frn_company_ips')) {
function frn_company_ips() {
	if(!is_admin()) {

		global $frn_company_ip;
		$frn_company_ip="";
		if(!isset($frn_company_ip)) {

			/*
			// OLD FRN APPROACH:
			//loop through IP addresses to see if there is a match
			
			$frn_company_ips = array(
				'12.230.217.13',
				'12.230.217.14',
				'73.104.49.76',
				'69.47.164.149',
				'75.22.103.153',
				'173.164.20.37',
				'50.204.247.218',
				'255.255.252.0',
				'108.65.222.48'
			);

			$ch_i=0; $frn_company_ip=="";
			$frn_total_ips=count($frn_company_ips); 
			$frn_user_ip=$_SERVER['REMOTE_ADDR']; 
			while ($ch_i<>$frn_total_ips && $frn_company_ip=="") {
				if($frn_user_ip==$frn_company_ips[$ch_i]) {
					$frn_company_ip=$frn_company_ips[$ch_i];
					//echo "<h1>".$frn_company_ip."</h1>";
				}
				$ch_i++;
			}
			*/

			////////
			//UHS IP Pattern (provided February 2017)
			// Keeps chartbeat/hotjar from tracking any FRN and UHS employee
			// See Google Sheet for readable list, dates, and purposes: 
			//    https://docs.google.com/spreadsheets/d/1ok74iWV4QL0j99KDBr1T0uGcu53eC8fYmJC2Brwdl8E/edit#gid=368443453 
			$uhs_ip_pattern = "/12\.230\.217\.13|12\.230\.217\.14|73\.104\.49\.76|69\.47\.164\.149|75\.22\.103\.153|173\.164\.20\.37|50\.204\.247\.218|12.230.217.[0-2][0-35]?[0-7]?|255\.255\.252\.0|108\.65\.222\.48|50\.76\.171\.85/";

			if($frn_company_ip=="") {
				$ip_regex=preg_match($uhs_ip_pattern,$frn_user_ip,$matches);
				if($ip_regex) $frn_company_ip=$matches[0];
			}
		}
		//echo "<h1>Dax IP: ".$frn_user_ip."; found: ".$matches[0]."</h1>";
		return $frn_company_ip;
		
	} //Ends admin restriction
	else return "";
}
}






//////////////////////////////////
// GOOGLE_ANALYTICS FEATURES    //
//////////////////////////////////
add_action('wp_head', 'frn_addtoheader', 100);
if(!function_exists('frn_addtoheader')) {
function frn_addtoheader() {
    //frn GA version 2.3 10/15/2013
    //frn GA version 2.4 6/15/16
	
	$frn_ga = '
	<!-- #####
	Google Analytics
	##### -->';

	$options_head = get_option('site_head_code');
	
	//Turns on JavaScript processing of external links if the frn_eli_... variables defined in the HEAD code
	$ga_act_eli=""; $ga_exttarget="";
	if(isset($options_head['ext_link_icon_type'])) {
		if($options_head['ext_link_icon_type']=="js") {
			if(!isset($options_head['ext_link_target'])) $options_head['ext_link_target']="";
			//Deactivates processing of external links if this is set to "deactivate"
			if($options_head['ext_link_target']!=="deactivate") $ga_exttarget = '
		frn_eli_target="'.$options_head['ext_link_target'].'";';
			//Deactivates adding icon to external links
			$ga_act_eli = '
		frn_eli_deact="'.(isset($options_head['ext_link_icon']) ? $options_head['ext_link_icon'] : null).'";';
		}
	}
	

	//Prepare the Analytics code
	$launch_GA=true; $site_ga_test=""; $ea_ua_js=""; $site_ga_id_value=""; $error_404_title=""; $demo_js_activate=""; $ga_test=""; $demo_js=""; //$ea_js = ''; 
	
	//Initial Triggers:
	//If test mode isn't initiated, then see if user is logged in. We don't want our updating activity to mess with analytics.
	if(isset($options_head['site_ga_test'])) $site_ga_test=$options_head['site_ga_test'];
	//echo "<h1>".$site_ga_test."</h1>";
	if ( $site_ga_test!=="Test" ) {
		if (is_user_logged_in()) $launch_GA = false; 
	}

	//Prepare the code:
	if ($launch_GA) { 

		/*  //Now a default in all Google Analytics accounts.
		$ga_ua = ""; 
		if(isset($options_head['frn_ga_ua'])) {
			if($options_head['frn_ga_ua']=="Activate") $ga_ua = "_ua"; //Used to refer to other JS files that have the latest GA event and social tracking codes
		}
		*/

		if(isset($options_head['frn_ga_dgrphx'])) {
			if($options_head['frn_ga_dgrphx']=="Activate") $demo_js_activate = "y"; //Used to refer to other JS files that have the latest GA event and social tracking codes
		}

		//decided to continue tracking company persons since many sites employees use and we want to see how
		if(isset($options_head['site_ga_id_value'])) $site_ga_id_value=$options_head['site_ga_id_value'];
		
		if($site_ga_id_value!=="") {  //if no GA site id is not included, then don't put GA code on site	
			//Adds the info to our GA code that turns on our in-page per link click tracking
			if($options_head['frn_ga_ea']=="Activate") {
				//old version discontinued 6/15/16
				//$ea_js = "var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
				//_gaq.push(['_require', 'inpage_linkid', pluginUrl]);";
				$ea_ua_js = "
		ga('require', 'linkid');";  //updated 7/20/16 removed: , 'linkid.js'
			}
			
			if(isset($options_head['frn_ga_404'])) {
				if(trim($options_head['frn_ga_404'])!=="") 
					$error_404_title = "
		error_404_title = '".$options_head['frn_ga_404']."';";
			}
			
			//Used for including Double-Clicks' assumed demographics data
			if($demo_js_activate=="y") 
				$demo_js = "
		ga('require', 'displayfeatures');";

		/*
			//Used to categorize content to evaulate seperately
			if(isset($group_id)) {
				if($group_id=="") $group_id="1";
				$groups_js = "
		ga('set', 'contentGroup".$group_id."', 'Promoted Content'); ";
			}
			else */ $groups_js="";
			
			if ( $site_ga_test=="Test" ) $ga_test="
		//Test mode active";

			$frn_cross_domain = ");";  //default end after GA ID
			if(!isset($options_head['cross_trigger'])) $options_head['cross_trigger']="";
			if(!isset($options_head['cross_sites'])) $options_head['cross_sites']="";
			if($options_head['cross_trigger']!=="" && trim($options_head['cross_sites'])!=="") {
				$frn_cross_domain = ", 'auto', {'allowLinker': true});
		ga('require', 'linker');
		ga('linker:autoLink', ['".$options_head['cross_sites']."'] ); //cross domain tracking active";
			}

			// removed from script and added into extensions JS: 
			//baseDomain = location.hostname;
			//"pluginInstall='".plugins_url()."';".
			$frn_ga .= "
	<script>
		".$ga_test.$error_404_title.$ga_act_eli.$ga_exttarget."
		
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '".$site_ga_id_value."'".$frn_cross_domain.
		$ea_ua_js.
		$demo_js.
		$groups_js."
		ga('send', 'pageview');
	</script>";
			
			/*
			//Once a GA account is converted to a Universal Analytics account, this is the code that will replace the standard GA code.
			//if($ga_ua=="_ua") {
				//this is where the UA code above used to be
			//}
			else { //If UA is not activated, then install the old script by default
			
				//Used for including Double-Clicks' assumed demographics data
				if($demo_js_activate=="y") $demo_js = "'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js'";
				else $demo_js = "'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'";

				$frn_ga .= '
	<script type=\'text/javascript\'>
		pluginInstall="'.plugins_url().'";'.
		$error_404_title.$ga_act_eli.'
		baseDomain = location.hostname;
		var _gaq = _gaq || [];
		'.$ea_js.'
		_gaq.push([\'_setAccount\', \''.$site_ga_id_value.'\']);
		_gaq.push([\'_trackPageview\']);
		(function() {
			var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;
			ga.src = (\'https:\' == document.location.protocol ? '.$demo_js.';
			var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>';
			}
			*/
			
		} //ends if site_ga_id_value!==""
		elseif($ga_exttarget!=="" || $ga_act_eli!=="") {
			//removed due to new reliance on JS for this: pluginInstall="'.plugins_url().'";
			$frn_ga .= '
	<!--FRN Plugin: Google Analytics ID is empty and tracking is not activated-->
	
	<script type=\'text/javascript\'>'.$ga_exttarget.$ga_act_eli.'</script>';
		}
		else $frn_ga .= '

	<!--FRN Plugin: Google Analytics ID is empty and tracking is not activated-->';

	} //if launch_ga (user logged in)
	elseif($ga_exttarget!=="" || $ga_act_eli!=="") {
		//removed due to new reliance on JS for this: pluginInstall="'.plugins_url().'";
		 $frn_ga .= '
	<!--YOU ARE LOGGED IN and therefore not being tracked in Google Analytics or the FRN setting for the GA ID is empty.-->
	
	<script type=\'text/javascript\'>'.$ga_exttarget.$ga_act_eli.'</script>';
	}
	else {
		//removed this line due to new reliance on JS for this: pluginInstall="'.plugins_url().'";
		 $frn_ga .= '

	<!--YOU ARE LOGGED IN and therefore not being tracked in Google Analytics or the FRN setting for the GA ID is empty.-->';
	}

	$frn_ga .= '
	
	<!-- #####
	Google Analytics Ends
	##### -->
	
	';

	//Removed since combinging all JS into one file
	//<script async type="text/javascript" src="'.plugins_url().'/frn_plugins/frn_ga_extensions.js?v=3.1"></script>
	
	echo $frn_ga;
	
} //end function
} //end function exists test for header







//////////////////////////////////
// FRN_GA Reporting
//////////////////////////////////
add_action('wp_head', 'frn_reporting_head', 101);
if(!function_exists('frn_reporting_head')) {
	//this is temporary until we see no errors being reported to GA
function frn_reporting_head() { ?>

	<!-- #####
	FRN Google Analytics Error Checking
	##### -->
	<script type="text/javascript">
		function frn_reporting (type,category,action,label,conv_url) {
			var conv_trigger=false; 
			if(type=="phone") {
				if(category===undefined || category=="") category='Phone Numbers';
				if(action===undefined || action=="") action='Phone Clicks [General]';
				if(label===undefined || label=="") label='Calls';
				var conv_url='/conversion-phone-number-click-touch/';
				if(isTierIphone) conv_trigger=true;
			}
			else if(type===undefined || type=="") { 
				if(category===undefined || category=="") category='Unknown';
				if(action===undefined || action=="") action='Unknown';
				if(label===undefined || label=="") label='';
				if(conv_url===undefined || conv_url=="") conv_url='';
			}
			try{ frn_analytics( category, action, label, conv_trigger, conv_url );  }
			catch(err) { if (typeof ga==='function') ga('send', 'event', category, 'FRN_Analytics Function Not Loaded', err); }
<?php
			//track Facebook conversions
			$options_ppc = get_option('site_head_code');
			if(!isset($options_ppc['fb_conv_type'])) $options_ppc['fb_conv_type']="";
			if(!isset($options_ppc['fb_conv_label'])) $options_ppc['fb_conv_label']="";
			if(isset($options_ppc['fb'])) {
			if($options_ppc['fb']=="A" && $options_ppc['fb_conv_type']!=="pages" ) { ?>
			if(type=="phone") { if(isTierIphone) fbq('track', '<?=($options_ppc['fb_conv_label']!=="") ? $options_ppc['fb_conv_label'] : "Lead" ;?>'); }
<?php }} ?>
		}
	</script>
	<!-- #####
	FRN Google Analytics Error Checking
	##### -->
	
	<?php
}
}





///////////////////////////////////
// CHARTBEAT_HOTJAR, CONTENTLY  //
///////////////////////////////////

///////////
/// HEAD_COMPONENTS
add_action('wp_head', 'frn_addtoheadchrbt', 102	); 
if(!function_exists('frn_addtoheadchrbt')) {
function frn_addtoheadchrbt() {
	if(!is_admin()) {
		//This should only run for the front end of the site
		$options_data_tools = get_option('site_footer_code');
		$frn_chartbeat = '';
		/*
			IPs:
			FRN: [various, see Google Spreadsheet for Analytics referral spam]
			Matt: 75.118.35.203
			//RankLab: 173.55.187.31
		*/

		//First see if chartbeat isn't disabled altogether
		$launch_chartbeat="no"; $ftr_chrbt_checkbox="";
		if(isset($options_data_tools['ftr_chrbt_checkbox'])) $ftr_chrbt_checkbox=$options_data_tools['ftr_chrbt_checkbox'];
		if($ftr_chrbt_checkbox=="Activate" or $ftr_chrbt_checkbox=="Test") {
			
			$frn_company_ip=frn_company_ips();
			if(!isset($frn_company_ip)) $frn_company_ip="";

			//Makes sure the person isn't an admin or company employee
			if(!is_user_logged_in() && $frn_company_ip=="") $launch_chartbeat="yes";
			
			// END COMPANY IP TEST
			////////
			
			//For test mode code to display for admins only
			if(is_user_logged_in() and $ftr_chrbt_checkbox=="Test") { $launch_chartbeat="yes"; $test_mode="
	<!--Chartbeat Test Mode Engaged-->
	";}
			else $test_mode="";
			
			// Display the header notification
			if($launch_chartbeat=="yes") { 
				if($ftr_chrbt_checkbox=="Activate" or $ftr_chrbt_checkbox=="Test") {
					//all limitations are also for the frn_addtofooter code -- had to separate since I can't add an action within another action
					$frn_chartbeat = "
				".$test_mode.
	"<script type='text/javascript'>_sf_startpt=(new Date()).getTime();</script>
		
	";
			
				}
			}
			else $frn_chartbeat = '
	<!--Chartbeat: You are either logged in or accessing the site from a company location. As a result you are not being tracked in Chartbeat: Your IP address: '.$_SERVER['REMOTE_ADDR'].'. See the footer for details.-->
			
	';
		}
		elseif(is_user_logged_in()) {
			//displayed only for logged in users on the frontend of the site
			$frn_chartbeat = '
	<!--Chartbeat: Chartbeat is disabled in the FRN Plugin (this message only displays for admins). See the footer for details. -->
			
	';
		}

		
		$ftr_hotjar_id=""; $frn_hotjar_hdr="";
		if(isset($options_data_tools['ftr_hotjar_radio'])) { 
			//The radio is the main setting for hotjar
			if(isset($options_data_tools['ftr_hotjar_id'])) $ftr_hotjar_id=trim($options_data_tools['ftr_hotjar_id']);
			if($options_data_tools['ftr_hotjar_radio']=="Activate" && $ftr_hotjar_id!=="" && $launch_chartbeat=="yes") {
				$frn_hotjar_hdr = "
	<script type='text/javascript'>frn_hj_act='Y'; //Hotjar js trigger</script>
	
	";
			}
			elseif(is_user_logged_in()) $frn_hotjar_hdr="<!--Hotjar not activated (due to missing ID or for the same reasons as Chartbeat; See footer code for details; this only shows for admins)-->
	
	";
		}
		
		echo $frn_chartbeat.$frn_hotjar_hdr;
	} //Ends requirement that it only runs in the frontend of the site
}
}








//////////
/// FOOTER_COMPONENTS
add_action('wp_footer', 'frn_data_tools_ftr', 2);
if(!function_exists('frn_data_tools_ftr')) {
function frn_data_tools_ftr() {
	//makes sure function doesn't execute for the admin area
	if(!is_admin()) {
		$frn_company_ip=frn_company_ips();
		if(!isset($frn_company_ip)) $frn_company_ip="";
		
		$frn_chartbeat=''; $launch_chartbeat="no";
		$options_data_tools = get_option('site_footer_code');
		

		/////////////////
		/// CHARTBEAT ///
		/////////////////
		if(!is_user_logged_in() && $frn_company_ip=="") $launch_chartbeat="yes";
		
		$ftr_chrbt_checkbox="";
		if(isset($options_data_tools['ftr_chrbt_checkbox'])) $ftr_chrbt_checkbox=$options_data_tools['ftr_chrbt_checkbox'];
		
		if(is_user_logged_in() and $ftr_chrbt_checkbox=="Test") $launch_chartbeat="yes";
		
		if($launch_chartbeat=="yes")
		{
			if($ftr_chrbt_checkbox=="Activate" or $ftr_chrbt_checkbox=="Test") { //if chartbeat is activated
				
				//defaults
				$category="Domain";
				$domain = ucfirst(str_replace($pagehttp,"",str_replace("http://","",str_replace("https://","",str_replace("www.","",home_url())))));
				$path = get_permalink( $post->ID ); //this doesn't work for rehabandtreatment landing pages since there is no permalink for them

				//Settings prep
				if($path=="") $path = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				if(isset($options_data_tools['ftr_chrbt_category'])) {
					if($options_data_tools['ftr_chrbt_category']!=="") {
						if($options_data_tools['ftr_chrbt_category']=="Domain") {
							$category=$domain;
							$sections = "";
						}
						elseif($options_data_tools['ftr_chrbt_category']!=="none") {
							$category=$options_data_tools['ftr_chrbt_category'];
							$sections = '
		_sf_async_config.sections = "'.$domain.'";"';
						}
						else $category="";
					}
				}
				
				$pagehttp = 'http';
				if(isset($_SERVER["HTTPS"])) {if ($_SERVER["HTTPS"] == "on") {$pagehttp .= "s";}}
				$pagehttp .= "://";
				
				//goes in footer

				$frn_company = $options_data_tools['ftr_chrbt_company'];
				if($frn_company=="UHS") $frn_company = "uhssites.com";
				else  $frn_company = "allwebsites.com";
				
				$frn_chartbeat =  "
		
		
	<!-- #####
	Chartbeat Code
	##### -->";
				
				if($ftr_chrbt_checkbox=="Test") { 
					$frn_chartbeat .= '
		
	<!--Chartbeat: Testing Mode Engaged, all messages below confirm typical situations (Your IP address: '.$_SERVER['REMOTE_ADDR'].'; Company IP='.$frn_company_ip.'; launch='.$launch_chartbeat.' )-->
	';
			//if(is_admin()) echo " <!--It's admin -->"; else echo " <!--Not an admin -->";
					if(is_user_logged_in() and $frn_company_ip!=="") $frn_chartbeat .= "
	<!--Chartbeat: As a tester, you are logged in and on the company network and therefore not being tracked in Chartbeat. Your IP address: ".$_SERVER['REMOTE_ADDR']."-->\n
	";
					else if(is_user_logged_in()) $frn_chartbeat .= "
	<!--Chartbeat: As a tester, you are logged in and therefore not being tracked in Chartbeat-->\n
	";
					else if($frn_company_ip!=="") $frn_chartbeat .= "
	<!--Chartbeat: As a tester, you are accessing the site from a company location and therefore not being tracked in Chartbeat: Your IP address: ".$_SERVER['REMOTE_ADDR']."-->\n
	";
					else $frn_chartbeat .= "
	<!--Chartbeat: Not sure why you are seeing this.-->
	";
				}


		///NOTES:
				//path below is used to help with campaign situations. It'll loop campaigns in with normal visits to improve the exposure of the page in Chartbeat. 
				//this applies to Outbrain, facebook, and other campaign
		
		//_sf_async_config.title = "Story Title";
				$frn_chartbeat .= "
	<script type='text/javascript'>
		var _sf_async_config={};
		/** CONFIGURATION START **/
		_sf_async_config.uid = 37107;
		_sf_async_config.domain = '".$frn_company."';".$sections."
		_sf_async_config.path = '".$path."';
		_sf_async_config.authors = '".$category."';
		/** CONFIGURATION END **/
		(function(){
		  function loadChartbeat() {
			window._sf_endpt=(new Date()).getTime();
			var e = document.createElement('script');
			e.setAttribute('language', 'javascript');
			e.setAttribute('type', 'text/javascript');
			e.setAttribute('src',
			   (('https:' == document.location.protocol) ? 'https://a248.e.akamai.net/chartbeat.download.akamai.com/102508/' : 'http://static.chartbeat.com/') +
			   'js/chartbeat.js');
			document.body.appendChild(e);
		  }
		  var oldonload = window.onload;
		  window.onload = (typeof window.onload != 'function') ?
			 loadChartbeat : function() { oldonload(); loadChartbeat(); };
		})();
	</script>
	<!-- #####
	Chartbeat Code Ends
	##### -->
	
	
	";
			
			}
			
		}
		else {
		
			if($ftr_chrbt_checkbox=="Deactivate") $frn_chartbeat .=  "
	<!--Chartbeat: Chartbeat is deactivated-->\n
	";
			else if(is_user_logged_in() and $frn_company_ip!=="") $frn_chartbeat .=   "
	<!--Chartbeat: You are logged in and on the company network and therefore not being tracked in Chartbeat. Your IP address: ".$_SERVER['REMOTE_ADDR']."-->\n
	";
			else if(is_user_logged_in()) $frn_chartbeat .=   "
	<!--Chartbeat: You are logged in and therefore not being tracked in Chartbeat-->\n
	";
			else if($frn_company_ip!=="") $frn_chartbeat .=   "
	<!--Chartbeat: You are accessing the site from a company location and therefore not being tracked in Chartbeat: Your IP address: ".$_SERVER['REMOTE_ADDR']."-->\n
	";
			else $frn_chartbeat .=   "
	<!--Chartbeat: You are looking at the admin area-->
	";
			
		}


		/////////////////
		/// HOTJAR ///
		/////////////////
		
		$frn_hotjar = "";
		if(isset($options_data_tools['ftr_hotjar_radio'])) {
			
			if(isset($options_data_tools['ftr_hotjar_id'])) $ftr_hotjar_id=trim($options_data_tools['ftr_hotjar_id']);
				else $ftr_hotjar_id="";
			
			if($options_data_tools['ftr_hotjar_radio']=="Activate" && $ftr_hotjar_id!=="" && $launch_chartbeat=="yes") { 
				
				$frn_hotjar = "
	<!-- #####
	Hotjar Tracking
	##### -->";
				$frn_hotjar .= '
	<script>
		(function(f,b){
			var c;
			f.hj=f.hj||function(){(f.hj.q=f.hj.q||[]).push(arguments)};
			f._hjSettings={hjid:'.$ftr_hotjar_id.', hjsv:3};
			c=b.createElement("script");c.async=1;
			c.src="//static.hotjar.com/c/hotjar-'.$ftr_hotjar_id.'.js?sv=3";
			b.getElementsByTagName("head")[0].appendChild(c); 
		})(window,document);
	</script>';
				$frn_hotjar .= "
	<!-- #####
	Hotjar Tracking Ends
	##### -->
	
	";
			}
			elseif(is_user_logged_in()) {
			
			$frn_hotjar =  "
	<!--
		Hotjar is deactivated.
		The following cases are true: \n";
			
				if($options_data_tools['ftr_hotjar_radio']=="Deactivate" && is_user_logged_in()) $frn_hotjar .=  "			Hotjar is manually deactivated in settings. \n"; //added logged in state so that this message only applies for admins
				if($options_data_tools['ftr_hotjar_radio']=="" && is_user_logged_in()) $frn_hotjar .=  "			Hotjar settings have never been saved. \n"; //added logged in state so that this message only applies for admins
				if($ftr_hotjar_id==="" && is_user_logged_in()) $frn_hotjar .=  "			The ID field is empty in settings. \n"; //added logged in state so that this message only applies for admins
				if($frn_company_ip!=="") $frn_hotjar .=   "			You are on a company network. \n";
				if(is_user_logged_in()) $frn_hotjar .=   "			You are logged in. \n";
				if(is_admin()) $frn_hotjar .=   "			You are looking at the admin area. \n";
			$frn_hotjar .=  "	--> \n\n";
			}
		}


		/////////////////
		/// CONTENTLY ///
		/////////////////
		$frn_contently = "";
		if(isset($options_data_tools['ftr_contently_radio'])) {
			
			if(isset($options_data_tools['ftr_contently_id'])) $ftr_contently_id=trim($options_data_tools['ftr_contently_id']);
				else $ftr_contently_id="";
			
			if($options_data_tools['ftr_contently_radio']=="Activate" && $ftr_contently_id!=="" && $launch_chartbeat=="yes") { 
				
				$frn_contently = "
	<!-- #####
	Contently Tracking
	##### -->";
				$frn_contently .= "
		<script type=\"text/javascript\">
			(function(){
			  function initInsights() {
			    if (!window._contently) {
			      var _contently = { siteId: \"".$ftr_contently_id."\" }
			      _contently.insights = new ContentlyInsights({site_id: _contently.siteId});
			      window._contently = _contently
			    }
			  }

			  var s = document.createElement('script');
			  s.setAttribute('type', 'text/javascript');
			  s.setAttribute('src', '//assets.contently.com/insights/insights.js')
			  s.onreadystatechange = function() {
			    if (this.readyState == 'complete' || this.readyState == 'loaded') {
			      initInsights();
			    }
			  };
			  s.onload = initInsights;
			  document.body.appendChild(s);
			})();
		</script>";
				$frn_contently .= "
	<!-- #####
	Contently Tracking Ends
	##### -->
	
	";
			}
			elseif(is_user_logged_in()) {
			
			$frn_contently =  "
	<!--
		Contently is deactivated.
		The following cases are true: \n";
			
				if($options_data_tools['ftr_hotjar_radio']=="Deactivate" && is_user_logged_in()) $frn_contently .=  "			Contently is manually deactivated in settings. \n"; //added logged in state so that this message only applies for admins
				if($options_data_tools['ftr_hotjar_radio']=="" && is_user_logged_in()) $frn_contently .=  "			Contently settings have never been saved. \n"; //added logged in state so that this message only applies for admins
				if($ftr_hotjar_id==="" && is_user_logged_in()) $frn_contently .=  "			The ID field is empty in settings. \n"; //added logged in state so that this message only applies for admins
				if($frn_company_ip!=="") $frn_contently .=   "			You are on a company network. \n";
				if(is_user_logged_in()) $frn_contently .=   "			You are logged in. \n";
				if(is_admin()) $frn_contently .=   "			You are looking at the admin area. \n";
			$frn_contently .=  "	--> \n\n";
			}
		}
		
		echo $frn_chartbeat.$frn_hotjar.$frn_contently;
	} //Ends restriction that this only executes on the frontend
}
}





//////////////////////////////////
// Yahoo, Bing, Facebook Reporting
//////////////////////////////////
add_action('wp_head', 'frn_analytics_ppc', 103);
if(!function_exists('frn_analytics_ppc')) {
function frn_analytics_ppc() {
	$options_ppc = get_option('site_head_code');
	if(!isset($options_ppc['site_ga_test'])) $options_ppc['site_ga_test']="";
	$frn_company_ip=frn_company_ips();
	if(!isset($frn_company_ip)) $frn_company_ip="";

	if($options_ppc['site_ga_test']=="Test" || (!is_user_logged_in() && $frn_company_ip=="")) {



		//////
		// YAHOO
		if(isset($options_ppc['yahoo'])) {
			if($options_ppc['yahoo']=="A") {
	?>

	<!-- Yahoo Tracking -->
	<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10027931'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>
	<!-- Yahoo Tracking -->

<?php
			}
		}



		//////
		// BING
		if(isset($options_ppc['bing'])) {
			if($options_ppc['bing']=="A") {
	?>
	<!-- Bing Tracking -->
	<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5625888"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5625888&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>
	<!-- Bing Tracking -->

<?php
			}
		}

	}  //ends check if logged in, company ip, or test mode
	elseif(isset($options_ppc['yahoo']) || isset($options_ppc['bing'])) { 
	?>

	<!-- Bing and Yahoo tracking are disabled because you are either logged in or accessing the site from a UHS network (UHS IP If Applies: <?=$frn_company_ip;?>) -->
		
<?php
	} //ends displaying comment if auto deactivated




	//////
	// FACEBOOK ANALYTICS

	//declare variables
	if(!isset($options_ppc['fb_init'])) $options_ppc['fb_init']="";
	if(!isset($options_ppc['fb_empl'])) $options_ppc['fb_empl']="";
	
	//check if activated
	if(isset($options_ppc['fb'])) {
		if($options_ppc['site_ga_test']=="Test" || ($options_ppc['fb_empl']=="" && !is_user_logged_in() && $frn_company_ip=="")) {
		if($options_ppc['fb']=="A" && $options_ppc['fb_init']!=="") {
	
			//deploy the script
	?>
	<!-- Facebook Pixel -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '<?=$options_ppc['fb_init'];?>');
	fbq('track', "PageView");
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?=$options_ppc['fb_init'];?>&ev=PageView&noscript=1" /></noscript>
<?php
			/////
			//FB CONVERSION TRACKING
			//set up conversion defaults
			if(!isset($options_ppc['fb_pages'])) $options_ppc['fb_pages']="";
			if(!isset($options_ppc['fb_conv_label'])) $options_ppc['fb_conv_label']="";
			if(!isset($options_ppc['fb_conv_type'])) $options_ppc['fb_conv_type']="";
			if($options_ppc['fb_pages']!=="" && ($options_ppc['fb_conv_type']=="pages" || $options_ppc['fb_conv_type']=="both")) {
				$fb_pages=explode(",",$options_ppc['fb_pages']);
				//echo "; Post ID: ".get_the_id();
			foreach($fb_pages as $page_id) {
			if($page_id==get_the_id()) {
				?>	fbq('track', '<?=($options_ppc['fb_conv_label']!=="") ? $options_ppc['fb_conv_label'] : "CompleteRegistration" ;?>');
<?php
			} //end if
			} //end foreach
			} //end conv type options
	?>
	<!-- End Facebook Pixel -->


	<?php

	/* 
	//removed 6/23/17 since at this point, we don't have key pages defined upfront. May simply add this per site as necessary.
	<script>// ViewContent 
	// Track key page views (ex: product page, landing page or article)
	fbq('track', 'ViewContent'); 
	</script> 
	*/
		} //end fb activation
		} //end default exclusions (company employees, etc.)
	} // end fb isset

	
} //ends function
} //ends check for duplicate function







//////////////////////////////////
// FRN_PHONE_OUTBRAIN Reporting
//////////////////////////////////
//This is triggered in both the phone and live chat part php files
//removed 6/16/17 since it's written only when users click on a conversion element.
//script below is in the frn_plugin.js file
/*
add_action('wp_head', 'frn_outbrain_head', 102);
if(!function_exists('frn_outbrain_head')) {
function frn_outbrain_head() { ?>

	<!-- #####
	Outbrain Conversion Reporting
	##### -->
		<script language='JavaScript'>
                var OB_ADV_ID= 30792;
                var scheme = (("https:" == document.location.protocol) ? "https://" : "http://");
                var str = '<script src="'+scheme+'widgets.outbrain.com/obtp.js" type="text/javascript"><\/script>';
                document.write(str);
		</script>
	<!-- #####
	Outbrain Conversion Reporting
	##### -->
	
	<?php
}
}
*/



?>