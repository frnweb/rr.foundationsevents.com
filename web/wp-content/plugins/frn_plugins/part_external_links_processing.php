<?php defined( 'ABSPATH' ) OR die( 'No direct access.' );

/* NOTES:
	"part" php files are in the works in an attempt to make it easier to add to any theme without requiring the massive main plugin PHP file.
	Unlike most "parts", nothing else depends on these features
	This file references other elements like analytics
	This file only includes frontend changes for users. 
	Admin features are still in the admin php.
	Move to it's own file on 3/7/17

*/




//////////////////////////////////
// ICON FOR EXTERNAL_LINKS	    //
//////////////////////////////////

/*
PHP LIMITATIONS:
	External Icon: When <A hrefs are styled with CSS to act like an object and given a background style and controlled via an external CSS, PHP cannot find that out without a lot of processing. 
		The icon will show in those cases unless you specifically add the disable attribute (ext_icon = "no")
*/

//Gets settings to see if external link processing even needs to happen
if(!function_exists('ext_link_processing')) {
function ext_link_processing($type) {
	$status = false; //means don't process the link
	$options_head = get_option('site_head_code');
	
	//check if defined
	if(!isset($options_head['ext_link_icon_type'])) $ext_link_icon_type = "";
		else $ext_link_icon_type = $options_head['ext_link_icon_type'];
	if(!isset($options_head['ext_link_icon'])) $ext_link_icon = "";
		else $ext_link_icon = $options_head['ext_link_icon'];
	if(!isset($options_head['ext_link_target'])) $ext_link_target = "";
		else $ext_link_target = $options_head['ext_link_target'];
	
	if($ext_link_icon_type!=="js") { //run the PHP
		//if target is blank, then it's just checking if JS or PHP processing should be used.
		if($ext_link_target=="deactivate" and $ext_link_icon=="D") $status = false; //turns the whole thing off
		elseif($type=="target" AND $ext_link_target=="deactivate") $status = false; //turns the whole thing off
		elseif($type=="icon" AND $ext_link_icon!="D") $status = true;
		else $status = true;
	}
	
	return (bool) $status;
}
}

if ( ! class_exists( 'WP_External_Links' ) ):

	final class WP_External_Links {
			
		public function __construct() {
			add_action( 'wp', array( $this, 'call_wp' ) );
		}
		public function call_wp() {
			//Turns it all on or off
			if(ext_link_processing('')) {
				$priority = 1000000000;
				//do_action('wpel_ready', array($this, 'call_filter_content'), $this);
				//see for details: https://wordpress.org/plugins/wp-external-links/other_notes/
				
				$options_head = get_option('site_head_code');
				if(!isset($options_head['ext_link_icon_scan'])) $ext_link_icon_scan="";
					else $ext_link_icon_scan = $options_head['ext_link_icon_scan'];
				//Add filter based on settings and as long as it's a page, post, or front page.
				if(($ext_link_icon_scan=="content" || $ext_link_icon_scan=="" || $ext_link_icon_scan=="both") && (!is_category() && !is_home() && !is_tag() && !is_author() && !is_date() && !is_search())) add_filter( 'the_content', array( $this, 'call_filter_content' ), $priority );
				if($ext_link_icon_scan=="widgets" || $ext_link_icon_scan=="both") add_filter( 'widget_text', array( $this, 'call_filter_content' ), $priority );
			}
		}
		public function call_filter_content( $content ) {
			// fix js problem by replacing </a> by <\/a>
			$content = preg_replace_callback( '/<script([^>]*)>(.*?)<\/script[^>]*>/is', array( $this, 'call_fix_js' ), $content );
			return $this->filter( $content );
		}
		public function call_fix_js( $matches ) {
			return str_replace( '</a>', '<\/a>', $matches[ 0 ] );
		}
		private function is_external( $href, $rel ) {
			return ( isset( $href ) AND ( ( strpos( $href, strtolower( get_bloginfo( 'wpurl' ) ), 0 ) === FALSE )
				AND ( substr( $href, 0, 7 ) == 'http://'
						OR substr( $href, 0, 8 ) == 'https://'
						OR substr( $href, 0, 6 ) == 'ftp://'
						OR substr( $href, 0, 2 ) == '//' ) ) );
		}
		/*
		//private function is_ignored( $href ) {
			// check if this links should be ignored based on settings
			for ( $x = 0, $count = count($this->ignored); $x < $count; $x++ ) {
				if ( strrpos( $href, $this->ignored[ $x ] ) !== FALSE )
					return TRUE;
			}
			return FALSE;
		//}
		*/
		
		
		private function filter( $content ) {
			// replace links
			$content = preg_replace_callback( '/<a[^A-Za-z](.*?)>(.*?)<\/a[\s+]*>/is', array( $this, 'call_parse_link' ), $content );
			return $content;
		}
		
		
		/**
		 * Parse an attributes string into an array. If the string starts with a tag,
		 * then the attributes on the first tag are parsed. This parses via a manual
		 * loop and is designed to be safer than using DOMDocument.
		 *
		 * @param    string|*   $attrs
		 * @return   array
		 *
		 * @example  parse_attrs( 'src="example.jpg" alt="example"' )
		 * @example  parse_attrs( '<img src="example.jpg" alt="example">' )
		 * @example  parse_attrs( '<a href="example"></a>' )
		 * @example  parse_attrs( '<a href="example">' )
		 *
		 * @link http://dev.airve.com/demo/speed_tests/php/parse_attrs.php
		 */
		private function parse_attrs ($attrs) {
			if ( ! is_scalar($attrs) )
				return (array) $attrs;

			$attrs = str_split( trim($attrs) );

			if ( '<' === $attrs[0] ) # looks like a tag so strip the tagname
				while ( $attrs && ! ctype_space($attrs[0]) && $attrs[0] !== '>' )
					array_shift($attrs);

			$arr = array(); # output
			$name = '';     # for the current attr being parsed
			$value = '';    # for the current attr being parsed
			$mode = 0;      # whether current char is part of the name (-), the value (+), or neither (0)
			$stop = false;  # delimiter for the current $value being parsed
			$space = ' ';   # a single space

			foreach ( $attrs as $j => $curr ) {
				if ( $mode < 0 ) {# name
					if ( '=' === $curr ) {
						$mode = 1;
						$stop = false;
					} elseif ( '>' === $curr ) {
						'' === $name or $arr[ $name ] = $value;
						break;
					} elseif ( ! ctype_space($curr) ) {
						if ( ctype_space( $attrs[ $j - 1 ] ) ) { # previous char
							'' === $name or $arr[ $name ] = '';   # previous name
							$name = $curr;                        # initiate new
						} else {
							$name .= $curr;
						}
					}
				} elseif ( $mode > 0 ) {# value

					if ( $stop === false ) {
						if ( ! ctype_space($curr) ) {
							if ( '"' === $curr || "'" === $curr ) {
								$value = '';
								$stop = $curr;
							} else {
								$value = $curr;
								$stop = $space;
							}
						}
					} elseif ( $stop === $space ? ctype_space($curr) : $curr === $stop ) {
						$arr[ $name ] = $value;
						$mode = 0;
						$name = $value = '';
					} else {
						$value .= $curr;
					}
				} else {# neither

					if ( '>' === $curr )
						break;
					if ( ! ctype_space( $curr ) ) {
						# initiate
						$name = $curr;
						$mode = -1;
					}
				}
			}

			# incl the final pair if it was quoteless
			'' === $name or $arr[ $name ] = $value;

			return $arr;
		}
		
		
		/**
		 * Add value to attribute
		 * @param array  $attrs
		 * @param string $attr
		 * @param string $value
		 * @param string $default  Optional, default NULL which means the attribute will be removed when (new) value is empty
		 * @return New value
		 */
		public function add_attr_value( &$attrs, $attr_name, $value, $default = NULL ) {
			if ( key_exists( $attr_name, $attrs ) )
				$old_value = $attrs[ $attr_name ];

			if ( empty( $old_value ) )
				$old_value = '';

			$split = explode( ' ', strtolower( $old_value ) );

			if ( in_array( $value, $split ) ) {
				$value = $old_value;
			} else {
				$value = ( empty( $old_value ) )
									? $value
									: $old_value .' '. $value;
			}

			if ( empty( $value ) AND $default === NULL ) {
				unset( $attrs[ $attr_name ] );
			} else {
				$attrs[ $attr_name ] = $value;
			}

			return $value;
		}
		
		
		public function call_parse_link( $matches ) {
			$link = $matches[ 0 ];
			$label = $matches[ 2 ];
			$created_link = $link;

			// parse attributes
			$attrs = $matches[ 1 ];
			$attrs = stripslashes( $attrs );
			$attrs = $this->parse_attrs( $attrs );

			$rel = ( isset( $attrs[ 'rel' ] ) ) ? strtolower( $attrs[ 'rel' ] ) : '';

			// href preperation
			if (isset( $attrs[ 'href' ])) {
				$href = $attrs[ 'href' ];
				$href = strtolower( $href );
				$href = trim( $href );
			} else {
				$href = '';
			}

			// checks
			$is_external = $this->is_external( $href, $rel ); //identifies if a link is an external one
			$is_ignored = false; //not used old: $this->is_ignored( $href );
			$has_rel_external =  (strpos( $rel, 'external', 0 ) !== FALSE); //checks if already has rel=external defined

			// if it's an internal link, then just return the link as it is in the content
			if ( ! $is_external && ! $has_rel_external) {
				return apply_filters('wpel_internal_link', $created_link, $label, $attrs);
			}
			
			
			
			//The remainder of the code assumes that the link is an external link or at least has rel=external added to it

			
			// is an ignored link?
			// rel=external will be threaded as external link
			//if ( $is_ignored && ! $has_rel_external ) {
			//	return apply_filters('wpel_external_link', $created_link, $link, $label, $attrs, TRUE);
			//}

			// set rel="external" (when not already set)
			if(!$has_rel_external) 
				$this->add_attr_value( $attrs, 'rel', 'external' );

			// adds the external icon class as long as:
				// settings doesn't disable it
				// style doesn't have URL(
				// the external icon isn't manually added
				// it's not a link that's linked
			
			$options_extlink = get_option('site_head_code');
			if(!isset($options_extlink['ext_link_icon'])) $ext_link_icon="";
				else $ext_link_icon = $options_extlink['ext_link_icon'];
			if($ext_link_icon!="D") {
				$icon_class = 'frn_ext_icon';
				if(isset($attrs[ 'class' ])) $attr_class = $attrs[ 'class' ]; else $attr_class = "";
				if(isset($attrs[ 'ext_icon' ])) $ext_icon = $attrs[ 'ext_icon' ]; else $ext_icon = "";
				
				if (	strpos( $attr_class, $icon_class, 0 ) === FALSE
						AND strpos( $attr_class, "url(" ,0) === FALSE
						AND ext_link_processing('icon') 
						AND trim($ext_icon) !=""
						AND strpos($label,"<img",0)===FALSE
						AND strpos($label," img ",0)===FALSE
						AND strpos($label,"<iframe",0)===FALSE
						AND strpos( $attr_class, "twitter-share-button" ,0) === FALSE
					) {
							if(trim($attr_class)!=="") $attr_class = $attr_class . " " . $icon_class;
							else $this->add_attr_value( $attrs, 'class', $icon_class." ".strpos($label,"<img",0) );
				}
			}
			//(bool) preg_match( '/<img([^>]*)>/is', $label )
			
			
			//Pulling FRN trigger for target new window
			if(!isset($options_extlink['ext_link_target'])) $ext_link_target="";
				else $ext_link_target = $options_extlink['ext_link_target'];
			
			//This is turned off by default (or not opening in a new window). Previously we had all external links open in a new window.
			//But we are still correcting the teams' old "new" window name so that future links don't pop up into it behind an active window.
			//Desktop users have no problem with new tabs opening. New tabs are very obvious. But mobile users are inconvenienced by a new window and are highly unlikely to return to our site. Therefore, this override will only apply to smartphones and tablets.
			//options: new, same, deactivate
			
			if($ext_link_target!=="deactivate") {
				if(isset( $attrs[ 'target' ])) $target = trim($attrs[ 'target' ]);
					else $target = "";
				if($ext_link_target=="new") {
					//if the setting is to open it in a new window
					//check if the link goes offsite
					if(ext_link_processing('target')) {
						
						//since we have specifically said we want all external links to open in a new window...
						//override that if it's a mobile device
						//so if desktop and if blank, set it. If using the old team's "new" window name, change it to "_blank" since we don't want other popups opening in that link behind the main browser window
						global $frn_mobile;
						if(!$frn_mobile) {
							if($target==="") $target = "_blank";  //$this->get_opt( 'target' );
							elseif(strtolower($target)=="new") $target = "_blank";

							// remove and replace target since value already saved
							unset($attrs[ 'target' ]); //removes all instances in case a link has it programed more than once
							$attrs[ 'target' ] =  $target;
						}
						else unset($attrs[ 'target' ]); //only mobile and tablet
						
					}
					elseif(strtolower($target)=="new") $attrs[ 'target' ] =  "_blank"; //if it's not an external link, still change our links from new to "_blank"
				}
				//elseif(strtolower($target)=="new") $attrs[ 'target' ] = "_blank"; //Applies to external and internal links. Since we don't want all external links to open in a new window, we are at least making the ones we specifically say should still do.
				
				//since the setting is to open in the same window and override defaults for desktop...
				elseif(trim($target)!=="") {
					//check if there is an attribute with a setting to keep the target (if so, then unset those and leave the target)
					if(isset($attrs[ 'keep_target' ]) || isset($attrs[ 'data-target' ])) {
						if(!isset($attrs[ 'keep_target' ])) $attrs[ 'keep_target' ]="";
						if(!isset($attrs[ 'data-target' ])) $attrs[ 'data-target' ]="";
						if($attrs[ 'keep_target' ]=="yes") unset($attrs['keep_target']);  //$attrs[ 'target' ] = "";  
						elseif ( $attrs[ 'data-target' ]=="keep") unset($attrs['data-target']);
						else  {unset($attrs['target']); unset($attrs['data-target']); unset($attrs['keep_target']); }
					}
					//if any links are opening a standard new window, remove the target
					elseif(trim($target)=="_blank") unset($attrs['target']); 
					//elseif(trim($target)!=="") unset($attrs['target']); //removes all targets no matter what's in them. Discontinued 5/2/17 since I realized I need to be more flexible.
				}
			}

			/*  //our javascript should be handling this. Keeping just in case we decide to use it in the future.
			//Add Google Event tracking to external links
			//get onclick attribute
			//if not undefined, then add to it
			//if undefined, create the key and value
			if(isset($attrs[ 'onClick' ])) {
				$onclick = $attrs[ 'onClick' ];
				$attrs[ 'onClick' ] = $onclick . "googleeventcode";
			}
			*/

			
			//Build the new link
			// create element code
			$created_link = '<a';

			foreach ( $attrs AS $key => $value ) {
				if($value!=="") $created_link .= ' '. $key .'="'. $value .'"';
			}

			$created_link .= '>'. $label .'</a>';

			// filter
			$created_link = apply_filters('wpel_external_link', $created_link, $link, $label, $attrs, FALSE);

			return $created_link;
		}
	} // End WP_External_Links Class
endif;
$WP_External_Links = new WP_External_Links();





?>