<?php 

	/////////
	/// Related Posts Shortcode & Function
	/////////
	// Initially created by Daxon Edwards 6/10/2016
	// Transitioned to functions and shortcode 2/8/17
	//Added automatic option July 2017
	// v=2.2



/*
	CONTENTS: 
	* SHARED FUNCTIONS (search phrase prep and search array build)
	* SHORTCODE FUNCTIONS
	* SHORTCODE DISPLAY LIST FUNCTION
	* Auto RP - Core Function (adds the list to content)
	* Auto RP - Trigger (Decides what page should get the list added)


*/




/////////////////
// SHARED FUNCTIONS
////////////////


////////
//Core search function. It simply returns an array
//Used for shortcode and auto related posts
if(!function_exists('frn_related_search')) {
function frn_related_search($total="", $search="", $post_type="any") {
	

	$rp = get_option('site_rp');

	//var prep
	$s = frn_url_search_prep($search);

	//Set default number or recommended posts
	$total = trim($total);
	if($total=="" || !is_numeric($total)) {
		if(is_404()) $total=8;
		else $total=3;
	}

	$unique_id=get_the_id();
	$url_terms_search=false;
	if(!isset($rp['cache'])) $rp['cache']="";
	if(!is_404() && $unique_id) {
		//if array, then it's likely not a 404 page. The cache can be helpful.
		//if array, it's used in a webpage and not 404. As a result, caching can be helpful.
		if($rp['cache']=="") $url_terms_search=get_transient( 'RP_search_'.$unique_id );
	}

	if($url_terms_search===false) {

		if(!is_404()) {
			if(!isset($rp['search_types'])) $rp['search_types']="";
			if($rp['search_types']!=="" && $post_type!=="any") $post_type=$rp['search_types']; //overrides only if "any" is used
		}
		
		//Remove "any" if it's in an array since we don't want it to mess up the wp_query
		//if post_type is blank, search will only include posts -- WP default
		if($post_type!=="any" && !is_array($post_type)) $post_type=explode(",",$post_type);
		//echo "<h1>Before: ";
		//print_r($post_type);
		//echo "</h1>";
		if(is_array($post_type)) {
			if(in_array("any",$post_type)) {
				$any_id=count($post_type)-1;
				unset($post_type[$any_id]); //assumes "any" is always at the end of any array -- because of checkbox order in settings
			}
		}
		//echo "<h1>After: ";
		//print_r($post_type);
		//echo "</h1>";
		
		if(!function_exists('relevanssi_do_query')) {
			//Defaut WP search approach
			$args = array( 
				's' 				=> $s, //"treatment", 
				'post_status' 		=> 'publish',
				'posts_per_page' 	=> $total,
				'fields'			=> 'ids'
			);
			if(!is_404()) {
				if($post_type!=="") {
					$args['post_type'] = $post_type; //can be "any" or an array
				}
				if($unique_id) {
					$args['post__not_in'] = array($unique_id);
				}
			}
			//print_r($args);
			//echo "<h1>Dax Test</h1>";
			$url_terms_search = new WP_Query($args);
			//print_r($url_terms_search);
		}
		else {
			//Relevanssi search approach
			$url_terms_search = new WP_Query();
			$url_terms_search->query_vars['s']				=$s;
			$url_terms_search->query_vars['posts_per_page']	=$total;
			$url_terms_search->query_vars['paged']			=0;
			$url_terms_search->query_vars['post_status']	='publish';
			$url_terms_search->query_vars['fields']			='ids';
			if($post_type!=="") $url_terms_search->query_vars['post_type']	=$post_type;
			if($unique_id) $url_terms_search->query_vars['post__not_in']	=array($unique_id); 
			relevanssi_do_query($url_terms_search);
			//print_r($url_terms_search);
		}

		

	}


	if(!is_404()) {
		// Caching
		//if array, it's used in a webpage and not 404. As a result, caching can be helpful.
		//set caching var to avoid the process heavy Relevanssi search for every load when page content doesn't change often.
		//if caching turned off, we will still save the variable down below for once they reactivate caching.
		$time_limit=60*60*4; //cache timeout in seconds (sec x min x hours) -- refresh is every four hours
		if($url_terms_search) {
			if ( $url_terms_search->have_posts() ) {
				set_transient( "RP_search_".$unique_id, $url_terms_search, $time_limit ); 
			}
		}
	}

	wp_reset_postdata();

	return $url_terms_search;

}
}


////////////
//// Prepping Search Terms
//   Used by shortcode and auto function (looped into the main list building functions)
if(!function_exists('frn_url_search_prep')) {
function frn_url_search_prep($search="") {

	if($search!=="") {
		$s = trim($search); //use a manually entered set of keywords
	}
	else {
		$check_end_slash = substr($_SERVER['REQUEST_URI'],-1);
		if($check_end_slash=="/") $uri_without_ending_slash = substr($_SERVER['REQUEST_URI'],0,strlen($_SERVER['REQUEST_URI'])-1);
		$first_slash_from_end_pos = strrchr($uri_without_ending_slash,"/");
		//$last_part_of_uri = substr($uri_without_ending_slash,$first_slash_from_end_pos);
		$s = str_replace("/","",$first_slash_from_end_pos);
		$s = urldecode($s);
		$s = str_replace("-"," ",$s);
		$s = str_replace("_"," ",$s);
		$s = trim(strtolower(preg_replace('/[0-9]+/', '', $s ))); //remove all numbers
	}

		//echo "Phrase: ".$s;
	
	$stop_words = array(
		"for",
		"the",
		"and",
		"an",
		"a",
		"is",
		"are",
		"than",
		"that",
		"I",
		"to",
		"on",
		"it",
		"with",
		"can",
		"be",
		"of",
		"get",
		"in",
		"you",
		"from",
		"if",
		"by",
		"so",
		"at",
		"do",
		"&",
		"there",
		"too"
	);
	$i=1;
	foreach($stop_words as $word){
		/*
		//for testing:
		if($i==1) {
			echo $s."<br />";
			echo $word."<br />";
			echo "string: ".strlen($s);
			echo "; word: ".strlen(" ".$word)."<br />";
			echo "position: ".strpos($s,$word." ")."<br />";
			echo "string without word: ".(strlen($s)-strlen(" ".$word))."<br />";
		}
		*/
		$word = trim(strtolower($word));
		$s = str_replace(" ".$word." "," ",$s); ///in the middle
		if(strpos($s,$word." ")===0) $s = str_replace($word." ","",$s); // at the beginning
		if(strpos($s," ".$word)==strlen($s)-strlen(" ".$word)) $s = str_replace(" ".$word,"",$s); // at the end
		$i++;
	}
	$s=trim($s);
	if($s=="") $s="treatment";

	return $s;

}
}

if(!function_exists('frn_manual_list')) {
//when this function is called, the system already has determined if ACF is activated and settings are included.
function frn_manual_list() {
	//if there are manual list of posts, store the list in the var
	$frn_list=""; $manual_rps=false;
	if(function_exists('get_field') && frn_check_if_post()=="yes") {
		$styling = get_field('frn_rp_styling');
		$manual_rps=get_field('frn_rp_manual_posts');
	}
	//echo "<h1>Dax: ".count($manual_rps)."</h1>";
	//print_r($manual_rps);

	if($manual_rps) {
		//echo "<h1>".count($manual_rps)."</h1>";
	    foreach( $manual_rps as $post_item): 
	        //setup_postdata($post); 
	        $frn_list .= "
			<li>
				<a href=\"".get_the_permalink($post_item->ID)."\" onClick=\"frn_reporting('','Content Interactions', 'Table of Contents', '".$post_item->post_title."',false,''); \">".$post_item->post_title."</a>
			</li>
			";
	    endforeach; 
	    wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
	}

    return $frn_list;

}
}

if(!function_exists('frn_check_if_post')) {
function frn_check_if_post() {
	if( (is_single() || is_page())   &&   !is_home() && !is_front_page() ) {
			return "yes";
	} 
	else return "no";
}
}







/////////////////
// SHORTCODES
////////////////



///////////
// New Version

//Benefits:
// If function not available, then it displays the code instead of a failure.
// The best approach is to use the function, but wrap it in the function_exists function so that things fail nicely even if the plugin isn't used.
// [frn_related_list html="" total="" search="" no_results_msg=""]
// Easier to remember version of shortcode 6/20/17
if(!function_exists('frn_related_shortcode')) {
function frn_related_shortcode($atts) {

		extract( shortcode_atts( array(
			'html' => "",
			'total' => 8,
			'search' => "",
			'no_results_msg' => "",
			'post_type' => "any",
			'class' => "",
			'id' => ""
			), $atts, 'frn_related' ) );

		return frn_related_sc_funct($html, $total, $search, $no_results_msg, $post_type, $class, $id);

}}
add_shortcode( 'frn_related', 'frn_related_shortcode' );





///////////
// Old Version
//The following is the initial version of the shortcode discontinued 6/2017, but some facilities may still use this in their 404 pages
if(!function_exists('frn_related_list_shortcode')) {
function frn_related_list_shortcode($atts) {

		extract( shortcode_atts( array(
			'html' => "",
			'total' => 8,
			'search' => "",
			'no_results_msg' => "",
			'post_type' => "any",
			'class' => "",
			'id' => ""
			), $atts, 'frn_related_list' ) );

		return frn_related_sc_funct($html, $total, $search, $no_results_msg, $post_type, $class, $id);

}}
add_shortcode( 'frn_related_list', 'frn_related_list_shortcode' );





/////////
// SHORTCODE LAYOUT
// Determines how the search results are to be displayed
// Only used in shortcode situations
if(!function_exists('frn_related_sc_funct')) {
function frn_related_sc_funct($html="", $total="", $search="", $no_results_msg="", $post_type="any", $ul_class="frn_url_results", $ul_id="") {
	
	$rp = get_option('site_rp');

	///////
	// STEP ONE: DO THE SEARCH / Build list of posts
	///////
	
		//Check and load current post custom fields for formatting related posts
     	//Expects ACF to be set up using these field vars
		$styling=false; $frn_list="";
		if(function_exists('get_field') && frn_check_if_post()=="yes") {
			$styling = get_field('frn_rp_styling');
			$frn_list = frn_manual_list();
		}

		//check if current post has a manual list of related posts provided
		//if so, skip search and make sure manual list is in a proper array format
		if(trim($frn_list)=="") {

			$search_words = frn_url_search_prep($search);
			$url_terms_search=false;
			if($search_words!=="") {
				
				if($html=="") {
					//Overrides defaults since this might be used with auto RPs

					if(!isset($rp['search_types'])) $rp['search_types']="";
					if(!isset($rp['count'])) $rp['count']="";

					if($rp['search_types']!=="") {
						$post_type = $rp['search_types'];
					}
				}
				
				$url_terms_search=frn_related_search($rp['count'], $search_words, $post_type);

			}

			if($url_terms_search->have_posts()) {
				$frn_list = frn_rp_list($url_terms_search,$ul_class);
				//returns false if there are no results after making sure current post not in the list
			}
			//echo "<h1>".$search."</h1>";
		}




	 ////////
	 /// STEP TWO: PREPARE DISPLAY
	 ///////

		//Make sure there is a list to return

		$rp_block = "
	     		<p>It looks like something may be wrong with the web address you used.</p>
	     	";


		 //////
		 // ARRAY ONLY
	     // Just return an array so that a custom function can be used to prepare the list
	     //If manual posts are provided on a post page, that option will be ignored.
	     if($html=="array") {
	     	if($url_terms_search->have_posts()) return $url_terms_search;
	     	else return FALSE; 
	     }




	     //////
		 // STANDARD 404 CONTENT
	     elseif($html=="404") {
	     	// return all the typical 404 content

	     	/* 
	     	//5/22/17 - decided to remove since it was making design challenging to consider times when nothing is returned
	     	<h1>Woops! The Web Address is Incorrect</h1>
	     	*/

	     	
	     	
	    	if ( $frn_list!=="" ) {
		    
				$ul_class=""; $ul_id=""; 
				if($ul_id!=="") {
					$ul_id=" id=\"".$ul_id."\"";
		    	}
			    $rp_block .="
			    <h2>Were you looking for any of these?</h2>
				<ul".$ul_id." class=\"".$ul_class."\">
					";
				$rp_block .= $frn_list;
				$rp_block .="
				</ul>
				<br />
				";

		 	}
		 	else {
				// if there are no posts, display this message:
				if($no_results_msg!=="") $rp_block .= $no_results_msg;
				else { 
					$rp_block .= "
				<p>
					And unfortunately, we can't find any posts that might work. 
					Take a look at it in the address bar of your browser above and see if the web address looks pretty normal. 
					Make corrections if not and try again.
				</p>
				";
				}
			}

		 	// the following happens on all 404 cases

			$rp_block .="
				<p><b>...or maybe try searching:</b></p>
		        <div class=\"frn_search_box\">".get_search_form(false)."</div>
				";
		 }



		 //////
		 // LIST AND SEARCHBOX
		 elseif($html=="searchbox") {
	     	// return ONLY the list and search box
	     	// No default 404 messaging

	    	if ( $frn_list!=="" ) {
		    	
		    	if($ul_id!=="") {
		    		$ul_id=" id=\"".$ul_id."\"";
		    	}

		    	//add the list and wrap with <UL>
			    $rp_block .="
				<ul".$ul_id." class=\"".$ul_class."\">
					";
				$rp_block .= $frn_list;
				$rp_block .="
				</ul>
				<br />
				";

			}  
			else { 
				// if there are no posts, display this message:
				if($no_results_msg!=="") $rp_block .= $no_results_msg;
				else { 
					$rp_block .= "
				<p>
					And unfortunately, we can't find any posts that relate to it. 
					Take a look at it in the address bar of your browser above and see if the web address looks pretty normal. 
					Make corrections if not and try again.
				</p>
				";
				} 
			} // ends when there are no posts
			
			// the following happens on all 404 cases
			$rp_block .="
				<p><b>...or maybe try searching:</b></p>
		        <div class=\"frn_search_box\">".get_search_form(false)."</div>
				";

		 } // ends html=searchbox






		 //////
		 // DEFAULT APPROACH

		 // The Default approach should match the auto rp look

		 else { 
		 	// Default: return only an HTML list of posts using the sitewide auto settings
		 	if ( $frn_list!=="" ) {
		 		//This function pulls styling settings from the auto RP settings
			    $rp_block = frn_rp_style($frn_list);

			    if(!isset($rp['id'])) $rp['id']="";
			    if(!isset($rp['class'])) $rp['class']="";
			    
			    //override main defaults if post has custom settings set
			    if($styling) {
					$class=trim($styling['class']);
					$id=trim($styling['id']);
				}

				if($rp['id']!=="" && $id=="") $id=$rp['id'];
		    	else {
		    		$id="frn_related_posts"; //required because it removes the RP anchor from TOC
		    	}
			    
			    if($rp['class']!=="" && $class=="") $class=$rp['class'];
		    	else {
		    		$class="frn_related_posts";
		    	}

				$rp_block = "
				<div id=\"".$id."\" class=\"".$class."\">
					".$rp_block."
				</div><br /> 
				";

			}
			else {
				if($no_results_msg=="blank") $rp_block .= "";
				elseif($no_results_msg!=="") $rp_block .= $no_results_msg;
				else $rp_block="<!-- No related posts available -->"; //hidden from users but included in HTML for confirmation
			}
			
		 }

	
	



	 /////////
	 //// STEP THREE: RETURN THE LIST
	 /////////
	 return $rp_block;
	
}}




///////////
// LIST PREPARATION - USED IN VARIOUS FUNCTIONS
///////////

function frn_rp_list($url_terms_search,$ul_class="") {
	//This function prepares ONLY the list of URLs without any styling
	
	//Check and load current post custom fields for formatting related posts
 	//Expects ACF to be set up using these field vars
	$styling=false; $frn_list="";

	if(function_exists('get_field') && frn_check_if_post()=="yes") {
		$styling = get_field('frn_rp_styling');
		$frn_list = frn_manual_list();
	}
	if(trim($frn_list)=="") { $manual_rp="no"; }
	else { $manual_rp="yes"; }

	//check if current post has a manual list of related posts provided
	//if so, skip search and make sure manual list is in a proper array format
	if($manual_rp=="no") {

		$loop=0;
		if($url_terms_search) {
			if ( $url_terms_search->have_posts() ) {

				//Get ID of current post before looping through results, but only if the standard WP search is enabled
				//sometimes other plugins overwrite the posts__not_in wp_query var and the current post is still in results (Talbott site has this issue as of 7/24/17)
				$unique_id="none";
				if(!function_exists('relevanssi_do_query')) {
					$unique_id=get_the_id();
				}
				while ( $url_terms_search->have_posts() ) {
					$url_terms_search->the_post();
					if(get_the_id()!==$unique_id) {
						$loop++;
						$frn_list .="
					<li>
						<a href=\"".get_the_permalink()."\" onClick=\"frn_reporting('','Content Interactions', 'Table of Contents', '".get_the_title()."',false,''); \">".get_the_title()."</a>
					</li>
					";
					}
				}

				wp_reset_postdata();
			}
		}
	}

	if($loop==0 && $manual_rp=="no") return "";
	
	//if false isn't returned, then there is a list and wrapped it in ULs
	//check for post-specific styling using ACF plugin
	$list_style="";
	if($styling) {
		//Even if class sent to this function, it'll be overwritten by the post-specific setting
		$ul_class=trim($styling['ul_class']);
		$list_style=$styling['list_style'];
	}

	if($ul_class!=="") $ul_class=" class=\"".$ul_class."\"";
	if($list_style=="ol") $type = "ol";
	else $type="ul";

	return "
		<".$type.$ul_class.">
			".$frn_list."
		</".$type.">
		";

}







///////////
// STYLE THE RP LIST
///////////

if(!function_exists('frn_rp_style')) {
function frn_rp_style($frn_list="") {
	//This function includes the CSS in-page styles, adds an h2 header, and page anchor
	//This portion is used for auto RPs and as the default shortcode approach

	$rp = get_option('site_rp');
	$rp_block="";

	if($frn_list!=="") {

		$styles="";
		if(!isset($rp['default_styles'])) $rp['default_styles']="Yes";
		//Check and load current post custom fields for formatting related posts
	 	//Expects ACF to be set up using these field vars
		$styling=false; $title_overrides=false;

		if(function_exists('get_field') && frn_check_if_post()=="yes") {
			$styling = get_field('frn_rp_styling');
			$title_overrides=get_field('frn_rp_title');
		}

		if($rp['default_styles']!=="") {

			$margin=""; $padding=""; $width=""; $maxwidth=""; $list_style=""; $ul="ul";
			$class=""; $div_id=""; $ul_class="";
			if($styling) {
				$float=trim($styling['float']);
				$margin=trim($styling['margin']);
				$padding=trim($styling['padding']);
				$border=trim($styling['border']);
				$list_style=$styling['list_style'];
				$width=trim($styling['width']);
				//$maxwidth=trim($styling['maxwidth']);
				$class=trim($styling['class']);
				$div_id=trim($styling['id']);
				$ul_class=trim($styling['ul_class']);
				$ul_padding=trim($styling['ul_padding']);
			}

			//set defaults
			if($float=="") {
				$float="none";
			}
			elseif($float!=="none") { 
				$width="inherit";
			}
			if($margin=="") {
				$margin="40px 0 80px 0";
			}
			if($padding=="") {
				$padding="20px";
			}
			if($width=="") {
				$width="100%";
			}
			//if($maxwidth=="") {
			//	$maxwidth="none";
			//}
			if($border=="") {
				$border="1px solid #ccc";
			} 
			if($list_style!=="") {
				//four options: none, bullets (disc), numbers (ol), or circles
				if($list_style!=="ol") {
					if($list_style=="none" && $ul_padding=="") {
						$ul_padding=".5em .5em 0 0";
					}
					$list_style = "
					list-style: ".$list_style.";";
				}
				else {
					$ul="ol";
				}
			}
			if($class=="") {
				if(!isset($rp['class'])) $rp['class']="";
				if(trim($rp['class'])!=="") $class=$rp['class'];
				else $class="frn_related_posts";
			}
			if($div_id=="") {
				$div_id="none";
			}
			if($ul_class=="") {
				$ul_class="none";
			}
			if($ul_padding=="") {
				$ul_padding=".5em .5em 0 1.1em";
			}

			$toc = get_option('site_toc');
			if(!isset($toc['h_level'])) $toc['h_level']="h2";
			elseif($toc['h_level']=="") $toc['h_level']="h2";

			$styles="
			<style>
				.".$class." {
				    margin: ".$margin.";
				    width: ".$width.";
				    float: ".$float.";
				    border: ".$border.";
				    padding: ".$padding.";
				}
				.".$class." ".$ul." {
				    margin: 0;
				    padding: ".$ul_padding.";".$list_style."
    			}
    			.".$class." li {
				    margin-bottom: .8em;
				    padding: 5px 5px 5px 0;
    			}
    			.".$class." ".$toc['h_level']." {
					padding: 0;
					margin: 0;
				}
				@media (max-width: 1023px) {
					.".$class." {
					    float: none;
					    max-width:100%;
					    width:inherit;
					}
					.".$class." ".$toc['h_level']." {
						padding: 0 0 5px 0;
						margin: 0;
					}
					.".$class." ".$ul." {
					    padding: 0;
					    list-style: none;
					}
					.".$class." li {
					    margin: 10px 0;
					}
					.".$class." li:active {
						background-color: #efefef;
					}
				}
			</style>
			";
		}


		//Add CSS styles, anchor link and header
			//Styling is added here since if positioning with a DOM, it needs to create the DIV wrapper. 
			//It's much easier to put the styles inside the DIV, although unconventional.
		$title="";
		//echo "<h1>Title: ";
		//print_r($title_overrides);
		//echo "</h1>";
		if($title_overrides) {
			global $frn_mobile;
			if($frn_mobile=="Smartphone") {
				if(trim($title_overrides['smartphones'])!=="") $title=$title_overrides['smartphones'];
			}
			else {
				if(trim($title_overrides['desktops'])!=="") $title=$title_overrides['desktops'];
			}
		}

		if($title=="") {
			if(!isset($rp['h2'])) $rp['h2']="";
			if(trim($rp['h2'])!=="") {
				$title=$rp['h2'];
			}
			else {
				//Default title
				$title="You May Also Like:";
			}
		}

		$rp_block = $styles."
			<a name=\"frn_related_posts\"></a>
			<".$toc['h_level']." id=\"frn_related_hdr\">".$title."</".$toc['h_level'].">
			".$frn_list."
			";

		/*
		//Wrap list in DIV
		//When positioning with DOM, this DIV needs to be created by the DOM as an object
		$frn_list = "
		<div id=\"".$id."\" class=\"".$class."\">
			".$frn_list."
		</div><br /> 
		";
		*/

	}

	return $rp_block;

}}






////////////
/////
//// AUTOMATIC RELATED POSTS FUNCTIONS
/////
/////////////




///////////
// AUTOMATIC RP - CORE Element (Adds and Builds List)
///////////

add_action( 'the_content', 'frn_auto_related_posts', 10 );
if(!function_exists('frn_auto_related_posts')) {
function frn_auto_related_posts($content) {
	//This is the main "automatic" function controlling the searching, preparation, and placement of RPs in the content

	if(frn_auto_rp_trigger($content)) {

		$frn_list="";

		//First see if the post has it's own list using ACF plugin
		if(function_exists('get_field')) {
			$frn_list = frn_manual_list();
		}

		if(trim($frn_list)=="") {
			//If nothing returned, run like normal

			//var prep
			$rp = get_option('site_rp');
			if(!isset($rp['count'])) $rp['count']="";
			if(!isset($rp['term'])) $rp['term']="";
			if(!isset($rp['search_types'])) $rp['search_types']="";

			//Build the list of posts
			if($rp) $search_words = frn_url_search_prep($rp['term']);
			if($search_words!=="") $search_results=frn_related_search($rp['count'], $search_words, $rp['search_types']);
			if($search_results) $frn_list = frn_rp_list($search_results);
		}

		//style and build the final block
		if(trim($frn_list)!=="") $rp_block = frn_rp_style($frn_list);

		//add it to the content
		return frn_auto_rp_placement($content,$rp_block);

	}

	//if RPs shouldn't be on the page, then there will not be anything returned prior to this point
	//So, just return the content
	return $content;

}
}





///////////
// AUTOMATICE RP - TRIGGER Evaluation for Auto RP
///////////

function frn_auto_rp_trigger($content) {
	//This function is called upon within the AUTO RP function above. 
	//Simply returns TRUE if a page should get an RP list or FALSE if it should not.

	//The following two items are necessary for any "the_content" filter
	if( in_the_loop() && is_main_query() ) {
		//the_content hook affects excerpts, meta descriptions, etc. 
		//By adding these two checks, it'll make sure that this feature only changes primary content displays and nothing else.
		$rp = get_option('site_rp');
		if(!$rp) return FALSE;
		elseif(stripos($content,"frn_related")) return FALSE; //if shortcode already in content, deactivate the auto feature -- assumes we wouldn't want two cases on the same page although they could use different searches
		else {

			if(!isset($rp['activation'])) $rp['activation']="";
			if($rp['activation']!=="yes") return FALSE;
			else {

				//Since it's activated, go through normal checks
				//In order of most common
				if(is_single() || is_page()) {

					//check for post-specific settings using ACF plugin options
					if(function_exists('get_field')) {
						$custom_activate = get_field('frn_rp_activate');
						if($custom_activate) {
							if(trim($custom_activate)!=="") {
								if(strtolower($custom_activate)=="yes") return TRUE; 
								elseif(strtolower($custom_activate)=="no") return FALSE;
							}
						}
					}

					$curr_type=get_post_type();
					if(!is_front_page() && !is_home() && $curr_type!=="attachment" && $curr_type!=="revision" && $curr_type!=="nav_menu_item" && !stripos(get_the_title(),"Contact") ) {



						/*
						NOTES:
						if post types selected
							use exclude IDs

						if post types not selected
							use include IDs
						*/

						$post_type_match=false;
						if(!isset($rp['post_types'])) $rp['post_types']="";
						if(!isset($rp['include'])) $is_included="";
							else $is_included=trim($rp['include']); //since we are testing it, need to clean it
						if(is_array($rp['post_types'])) {
							foreach($rp['post_types'] as $post_type) {
								if($post_type) {
									//echo "<h1>Current type: ".get_post_type()."; Selected Type: ".$post_type."</h1>";
									if(get_post_type()==$post_type) {
										$post_type_match=true;
									}
								}
							}
							if($post_type_match) {
								if(!isset($rp['exclude'])) $is_excluded="";
									else $is_excluded=trim($rp['exclude']); //since we are testing it, need to clean it
								if($is_excluded!=="") {
									$is_excluded=explode(",",$is_excluded);
									foreach($is_excluded as $excl_post){
										if(get_the_ID()==trim($excl_post)) return FALSE;
									}
								}
								return TRUE;
							}
							else {
								//if the current post type is not selected, check the include IDs array for the post ID.
								if($is_included!=="") {
									$is_included=explode(",",$is_included);
									foreach($is_included as $incl_post){
										if(get_the_ID()==trim($incl_post)) {
											$frn_related_act=TRUE;
											return TRUE;
										}
									}
								}
							}
						}
						else {
							//post_types is not an array, which means no post types selected
							//still check includes field if current post ID is in the array
							if($is_included!=="") {
								$is_included=explode(",",$is_included);
								foreach($is_included as $incl_post){
									if(get_the_ID()==trim($incl_post)) {
										$frn_related_act=TRUE;
										return TRUE;
									}
								}
							}
						}

						


					} //end if front page
				} //end if page/post
			} //rp activations
		}
	}
	return FALSE; //final stage: if not "return" prior to this, then it means the current post didn't meet any requirements

}






function frn_auto_rp_placement($content,$rp_block="") {
	//This function determines where the list will be placed in the content
	//This wouldn't even be called if a shortcode is in the content
	//It's purpose is to use a DOM approach helping to navigate around competing content
	//As of 7/24/17, the competing content is citations at the bottom of posts.
	//DEFAULT: RPs will be placed above citations

	/*
		Three methods:
			1. If ID provided, look for an object with that ID and place RPs above it (this is rarely used)
			2. If no ID, then try to use a word search for common citation section titles and attempt to move RPs at least above that title. (This won't always look good, but it's a step closer to what we want.)
			3. No citations found, just return content with list at bottom.
	*/



	/////
	// PREPARE DEFAULT STYLE SETTINGS
	$rp = get_option('site_rp');
	$citation_id=""; $citation_text = "";

	$class=""; $id=""; $styling=false;
	//Check and load current post custom fields for formatting related posts
 	//Expects ACF to be set up using these field vars
	if(function_exists('get_field')) {
		$styling = get_field('frn_rp_styling');
	}
	if($styling) {
		$class=trim($styling['class']);
		$id=trim($styling['id']);
	}

	if($id=="") {
		if(!isset($rp['id'])) $rp['id']="";
		if($rp['id']!=="") $id=$rp['id'];
		else $id="frn_related_posts"; //required because it removes the RP anchor from TOC
	}

	if($class=="") {
		if(!isset($rp['class'])) $rp['class']="";
		if($rp['class']!=="") $class=$rp['class'];
		else $class="frn_related_posts";
	}


	//////
	/// CITATION HEADERS TO LOOK FOR
	$citation_hdrs = array(
		"sources",
		"references"
		);
	$citation_hdrs_cnt = count($citation_hdrs);





	//////
	// ACTIVATE DOM

	$citation_id="";
	/*
	//This is a lighter method, but as of 8/2017 no one wanted to direct editors and those posting content to add DIV HTML with an ID. 
	//It was fully functional, but unnecessary to include. Since the effort was already done, keeping in case we change our minds in the future.
	if(!isset($rp['citations_id'])) $rp['citations_id']="";
	$rp['citations_id']= trim($rp['citations_id']);
	if($rp['citations_id']!=="") {
		if(stripos($content,$rp['citations_id'])) $citation_id = "found";
	}
	*/

	$citation_text=""; $i=0;
	while($citation_text!=="found" && $i<$citation_hdrs_cnt) {
		if(stripos($content,$citation_hdrs[$i])) $citation_text = "found";
		$i++;
	}

	if($citation_id=="found" || $citation_text=="found") {
		//citation_id not used
		//IMPORTANT: Keep in mind that in-context personalize boxes and external link processing also use DOM
		//maybe this could be turned into a function where DOMs are only activated on content once
		libxml_use_internal_errors(true);
		$dom = new DOMDocument(null, 'UTF-8');
		$dom->loadHTML( mb_convert_encoding( $content, 'HTML-ENTITIES', 'UTF-8' ) );
		libxml_clear_errors(); //keeps DOM errors from appearing on page
	}



	/*
	//////////
	// OPTION #1: Look for citations block using specified ID

	//This is a lighter method, but as of 8/2017 no one wanted to direct editors and those posting content to add DIV HTML with an ID. 
	//It was fully functional, but unnecessary to include. Since the effort was already done, keeping in case we change our minds in the future.
	//If an ID is added to the main settings, we can activate our DOM approach
	if($citation_id=="found") {
		//Find citations section by ID
		$citations = $dom->getElementById($rp['citations_id']);
		if($citations) {
			//create the DIV wrapper for the RP list with custom ID and CLASS
			//Insert the wrapped list
			$list_domElement = $dom->createElement('div',$rp_block);
				$wrapper_id = $dom->createAttribute('id');
				$wrapper_id->value = $id;
				$list_domElement->appendChild($wrapper_id);
				$wrapper_class = $dom->createAttribute('class');
				$wrapper_class->value = $class;
				$list_domElement->appendChild($wrapper_class);
			$citations->parentNode->insertBefore($list_domElement, $citations);
			//I don't understand DOM enough, but this preg_replace is required for content to display correctly.
			return preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', '', htmlspecialchars_decode($dom->saveHTML()));
		}

	} 
	*/



	//////////
	//OPTION #2: if ID not found, look for text

	if($citation_text=="found") {
		/*
			NOTES:
			This feature only workes if the citations headers are the only thing in a block.
			This strips out all HTML and only looks at the text within each block. 
			So as long as the header is wrapped in tags like P, H2, H3, DIV, SPAN, I, etc, this function will find it.
			Versions of Headers;
			1. Sources
			2. Citations
			3. References
		*/

		$all_content = $dom->getElementsByTagName('*');
		//echo "<h1>Content returned by DB</h1>".$content."<h1>End of Content returned by DB</h1>";

		
		//echo "<h1>".$all_content->length." Blocks On The Page</h1>";


		////////
		/// CREATE THE DIV AND LIST

		$list_domElement = $dom->createElement('div',$rp_block);
			$wrapper_id = $dom->createAttribute('id');
			$wrapper_id->value = $id;
			$list_domElement->appendChild($wrapper_id);
			$wrapper_class = $dom->createAttribute('class');
			$wrapper_class->value = $class;
			$list_domElement->appendChild($wrapper_class);

		$loop=0;
		foreach($all_content as $block){
			
			$inner_text=strip_tags($block->nodeValue);
			$inner_text=trim(strtolower($inner_text));
			
			if($inner_text!=="") {
				if(!is_numeric($inner_text)) {
					if($prev_loop_block!==$inner_text) {
						//FOR TESTING: echo "<b style=\"color:red;\">".$loop."</b>";

						$citation_hdr=""; $i=0;
						while($citation_hdr=="" && $i<$citation_hdrs_cnt) {
							//Remember that the citation header needs to be the only thing within a block
							//For whatever block that the header is in, the RPs will be added directly above it
							//As a result, if you have the header within a DIV, the RPs will be added within that DIV and above the header.
							//If you have an <hr> above the sources' header, the RPs will be added between the <hr> and header.
							//etc.
							
							$prev_loop_block=$inner_text;
							echo "<br /><br />[ <b style=\"color:red;\">Main #: ".$loop."; Sources #: ".($i+1)."</b> Looking for \"".$citation_hdrs[$i]."\"; Inner Text: ".$inner_text." ]";
							if($inner_text==$citation_hdrs[$i]) {
								echo ": <b>Block Matched!</b> Stopping loop and returning content...<br /> <br />";
								$citation_hdr = $citation_hdrs[$i];
								$block->parentNode->insertBefore($list_domElement, $block );
								//FOR TESTING: echo "<b style=\"color:red;\">RPs go here</b>";
							}
							$i++;
						}
						$loop++;
					}
				}
			}

			//Once a header is found, no need to continue looping through block. Just return the saved HTML.
			if($citation_hdr!=="") { 
				//I don't understand DOM enough, but this preg_replace is required for content to display correctly.
				return preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', '', htmlspecialchars_decode($dom->saveHTML()));
			}

		}

		

	} 


	// OPTION #3: There are no citations on the page, so just add it to the end.
	//   By this point, if nothing has been returned, then we can just use our normal, easy approach

	$rp_block = "
		<div id=\"".$id."\" class=\"".$class."\">
			<div class=\"".$class."_inner\">
			".$rp_block."
			</div>
		</div><br /> 
		";

	return $content.$rp_block;


	//////
	//  THIS CULMINATES THE RELATED POSTS FEATURE
	//////


}