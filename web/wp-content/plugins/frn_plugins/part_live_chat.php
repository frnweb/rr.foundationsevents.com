<?php defined( 'ABSPATH' ) OR die( 'No direct access.' );


/* NOTES:
	"part" php files are in the works in an attempt to make it easier to add to any theme without requiring the massive main plugin PHP file.
	Unlike most "parts", nothing else depends on these features
	This file references other elements like analytics, phone numbers, etc.
	This file only includes frontend changes for users. 
	Admin features are still in the admin php.
	Move to it's own file on 3/7/17


TABLE OF CONTENTS:
	LHN_COMMON Vars function (line: 1360)
	LHN_HEAD Code (line: 1545)
	LHN SLIDEOUT_TAB for Footer (line: 1630)
	LHN_SHORTCODE (line: 1675)
	LHN Slideout TAB_DEACTIVATION (line: 1970)
	LHN Invisible Button (LHN_INV_BUTTON) for Text/Tab (line: 2015)
	CALL_IN_CODE for Websites (line: 2035)

*/




//////
// LHN_COMMON Vars Function
//////
if(!function_exists('frn_lhn_common')) {
function frn_lhn_common($chat_type) {
	
	/*
	/////////
	//// All Discontinued Variables & Scripts

	//The following is the typical settings when using the invisible chat button code provided by LHN. 
	//Since the JS handles all of these and is loaded no matter what as of 2/1/17, these are not needed at the button level.
	//The only thing important is that the button ID is -1 for the invisible button.
		
		///CHAT BUTTONS Default vars
		var lhnAccountN = "14160-1";
	    var lhnButtonN = 8557; //invisible button is -1
	    var lhnChatPosition = 'default';
	    var lhnWindowN = 14960;
	    var lhnInviteN = 32167;
	    var lhnDepartmentN = 15027;

		//// SLIDEOUT Default VARS from LHN
		var lhnAccountN = "14160";  //different, no -1
		//no button line//
	    var lhnWindowN = 14960;
	    var lhnInviteN = 32167;
	    var lhnDepartmentN = 15027;
	    var lhnHPCallbackButton = false;
	    var lhnHPKnowledgeBase = false;
	    var lhnHPMoreOptions = false;

	// Discontinued since UA is default now for all sites - 6/16/16
	$ga_ua = "_ua";
	$options_ua = get_option('site_head_code');
	if(isset($options_ua['frn_ga_ua'])) {
		if($options_ua['frn_ga_ua']=="Activate") $ga_ua = "_ua";
	}

	//discontinued in 2016 due to JS handling most of this for ext links and WP giving us the URL using the permalink
	//Defines if page has a secure https or not
	$pagehttp = 'http';
	if(isset($_SERVER["HTTPS"])) {if ($_SERVER["HTTPS"] == "on") $pagehttp .= "s";}
	$pagehttp .= "://";
	$pageURL = $pagehttp.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

	//The following was removed since it was not helping with tracking in the call center in LHN Analytics.
	//lhnCustom2 = encodeURIComponent("'.urlencode(ucwords(str_replace($pagehttp,"",home_url()))).'");

	//lhnCss = "'.plugins_url().'/frn_plugins/frn_plugin.css?v=1.0";';
	//removed since JS handles this, but also decided to enqueue the css file above

	//Removed this due to combinging all JS into one file for fewer server calls
	//<script src="'.plugins_url().'/frn_plugins/lhnhelpouttab-defaults.min.js?v=1.0" type="text/javascript" ></script>
	*/
		




		////////
		/// LHN COMMON VARS
		//Before 2/1/17, if the tab was disabled, we had other JS files loaded with the buttons to take care of defaults.
		//after that, we needed the customized variables to remain in the head even if the tab was not to be loaded.

		$options_lhn = get_option('site_lhn_code'); //LHN array vars with all settings
		$lhn_account = "14160"; //default value
		$lhnWindowN = "14960"; //default value
		$lhnDepartmentN = "15027"; //main help desk, not PPC
		$lhn_autoinvite_id="38372";
		$lhnCss = plugins_url().'/frn_plugins/frn_plugin.css?v=1.0';



		///////
		/// Required Vars
		///These two are the bare minimums to include next to every chat script
		/// The -1 is only required at the end for chat buttons. Email button will not work with it in the URL.
		/// If these variables are used for the tab script, the button should be a -1 to keep a button at the bottom of the page from showing (necessary for tab to work). If a button is already on a page, then a button wouldn't show lower on the page anyway.
		/// If these are used in conjuction with an onpage chat button, the id should be a button ID.

		if($chat_type=="button") $lhn_account=$lhn_account."-1";
		$lhn_account = "
		var lhnAccountN = \"".$lhn_account."\"; ";



		///////
		/// Helpful Vars -- More important if customized

		//department ID for chat rep routing; 
		//default is Help Desk 15027--set by default in JS file
		//default for PPC site is 17982
		//Can have as many departments as the call center wants.
		$frn_domain = str_replace("http://","",str_replace("https://","",str_replace("www.","",home_url())));
		if($frn_domain=="rehabandtreatment.com") $lhnDepartmentN = "17982";
		if(isset($options_lhn['lhn_dept_id'])) { 
			if(trim($options_lhn['lhn_dept_id'])!=="") $lhnDepartmentN = $options_lhn['lhn_dept_id'];
		}		
		$lhnDepartmentN = '
		var lhnDepartmentN = '.$lhnDepartmentN.';';

		//chat window ID
		if(isset($options_lhn['lhn_window'])) { 
			if($options_lhn['lhn_window']!=="") $lhnWindowN = $options_lhn['lhn_window'];
		}
		$lhnWindowN = '
		var lhnWindowN = '.$lhnWindowN.';';



		//////
		// Never Changing for Tab
		if($chat_type=="tab") {
			$never_changing = "
		var lhnHPCallbackButton = false;
	    var lhnHPKnowledgeBase = false;
	    var lhnHPMoreOptions = false;
	    var lhnCss = \"".$lhnCss."\";"; //tried to use the main JS to set this, but it was occasionally overwritten for mobile and the size not changed.
		}
		else $never_changing="";



		/////////
		// Auto Invite Options
		//if not defined, they don't have to show except for lhnInviteEnabled.
		$lhn_autoinvite = false; $lhnInviteEnabled="";$lhnInviteN="";$lhnInviteChime="";$lhnCustomInvitation="";
		if(isset($options_lhn['lhn_autoinvite'])) {
			//info about auto invite variables: http://help.livehelpnow.net/article/1/2974/
			if($options_lhn['lhn_autoinvite']=="Activate") $lhn_autoinvite = true;
		}
		//check if there are keywords
		if(isset($options_lhn['lhn_autoinvite_keywords'])) {
			if(trim($options_lhn['lhn_autoinvite_keywords'])!="") {
				$lhn_kw_array = explode( ",", $options_lhn['lhn_autoinvite_keywords'] );
				$page_title = get_the_title();
				foreach($lhn_kw_array as $keyword) {
					if(strpos($page_title,trim($keyword))!==false) $lhn_autoinvite = true;
				}
			}
		}
		if($lhn_autoinvite) {
			$lhnInviteEnabled = '
		var lhnInviteEnabled = 1;'; //enabled
			if(isset($options_lhn['lhn_autoinvite_id'])) {
				if(trim($options_lhn['lhn_autoinvite_id'])!=="") $lhn_autoinvite_id=$options_lhn['lhn_autoinvite_id'];
			}
			$lhnInviteN = '
		var lhnInviteN = "'.$lhn_autoinvite_id.';"';
			if(isset($options_lhn['lhn_autoinvite_chime'])) {
				if($options_lhn['lhn_autoinvite_chime']=="Activate") $lhnInviteChime = '
		var lhnInviteChime = 1;';
				elseif($options_lhn['lhn_autoinvite_chime']=="Deactivate") $lhnInviteChime = '
		var lhnInviteChime = 0;';
			}
			if(isset($options_lhn['lhn_autoinvite_mssg'])) {
				if($options_lhn['lhn_autoinvite_mssg']!=="") $lhnCustomInvitation = '
		var lhnCustomInvitation = "'.$options_lhn['lhn_autoinvite_mssg'].'";';
			}
		}
		else $lhnInviteEnabled = '
		var lhnInviteEnabled = 0;'; //makes sure the window doesn't pop up



		///////
		/// FRN Custom Vars
		$frn_lhn_dc="";
		if(isset($options_lhn['lhn_phonecode'])) {
			if($options_lhn['lhn_phonecode']!=="") $frn_lhn_dc=trim($options_lhn['lhn_phonecode']);
		}
		if($frn_lhn_dc==="") {
			$frn_lhn_dc=trim(frn_external_db());
			if($frn_lhn_dc=="[none]") $frn_lhn_dc="";
		}
		if($frn_lhn_dc!=="") $frn_lhn_dc = '
		var lhnCustom1 = "[CALL-IN CODE] '.$frn_lhn_dc.'";';
		

		if(is_home() || is_front_page()) $current_page = get_home_url();
			else $current_page = get_permalink();
		if(trim($current_page)!=="") $current_page = '
		var lhnCustom2 = "[CURRENT PAGE] '.$current_page.'";';  //urlencode()

		//Used to make it obvious to the Call Center that a persion is on mobile device ("global" is necessary to refer to variable defined in function above)
		global $frn_mobile; 
		if($frn_mobile) $frn_mobile_setting = '
		var lhnCustom3 = "['.strtoupper($frn_mobile).'] User is chatting on this.";'; //encodeURIComponent("'.wp_title(" | ", false).'");


		return $lhn_account.$lhnWindowN.$lhnDepartmentN.$never_changing.$lhn_autoinvite.$lhnInviteN.$lhnInviteChime.$lhnCustomInvitation.$frn_lhn_dc.$current_page.$frn_mobile_setting;

}
}



//////
// LHN_HEAD: LiveHelpNow Settings for HEAD Area
//////

/// Slideout Phone Number Variable
/// Auto Invite Variables
/// Analytics Error Reporting for Slow Loading JS

add_action('wp_head', 'frn_lhn_head', 100); 
if(!function_exists('frn_lhn_head')) {
function frn_lhn_head() {
//Add Our LiveHelpNow Code to the HEAD
	
	$options_lhn = get_option('site_lhn_code');
	
	//Get glabal trigger variable; run the function that sets the global variable
	//Since this slide out function runs on every page, it's okay to call it here within this function
	frn_lhn_tab_deactivate(); 
	global $frn_lhn_disable_tab;

	//check whether to disable tab alltogether
	if($frn_lhn_disable_tab=="No" || $options_lhn['lhn_inpageact']=="Activate") {

		/* 

		//////////
		/// DISCONTINUED CODE
		//Discontinued 1/31/17 since we were able to add a custom invisible button when no button is on the page
		//Previously we added the style to hide the tab if buttons are present on the page. Did so only if tab was there.
		global $frn_lhn_hide_tab;
		$frn_lhn_hide_tab="";
		if($disable_tab=="Yes") {
			$frn_lhn_hide_tab="
			<style>.lhn_help_btn {display:none;} </style>
			";
		}

		// LHN script file goes here
		// <script async src="'.plugins_url().'/frn_plugins/lhnhelpouttab-custom'.$ga_ua.'.min.js?v=3.3" type="text/javascript" ></script>	

		// Discontinued 2/1/17 now that we are using DialogTech's latest JS version that scans for a particular number to replace instead of relying on replicated SCRIPTs in each location.
		if(stripos(home_url(),"rehabandtreatment")>0) $frn_phone = "";
			else $frn_phone = trim(stripslashes($options_phone['site_phone']));

		*/


		// PHONE NUMBER for Slideout
		// Checks what's saved in the the plugin's settings first. If blank, it checks the array above for it.
		$options_phone = get_option('site_phone_number');
		$frn_phone="";
		if(!isset($options_phone['site_phone'])) $options_phone['site_phone']="";
		if(trim($options_phone['site_phone'])!=="") $frn_phone = trim(stripslashes($options_phone['site_phone']));
		if($frn_phone!=="") $frn_phone = 'frn_phone = "'.$frn_phone.'";';
		
		//chat window ID
		$lhnWindowN="";
		$options_lhn = get_option('site_lhn_code'); 
		if(isset($options_lhn['lhn_window'])) { 
			if(trim($options_lhn['lhn_window'])!=="") $lhnWindowN = '
		lhnWindowN = '.$options_lhn['lhn_window'].';';
		}
//
		/* 

		//Can be used below to test firing of LHN functions
		//if(type=="email") alert("frn_email_window("+category+","+action+")");
		//else alert("frn_chat_window("+category+", "+action+")");

		*/

		//NOTE: this method is temporary until we see no errors being reported to GA

		//track Facebook conversions
		$options_ppc = get_option('site_head_code'); $fbk_conv="";
		if(!isset($options_ppc['fb'])) $options_ppc['fb']="";
		if($options_ppc['fb']=='A') {
			if(!isset($options_ppc['fb_conv_type'])) $options_ppc['fb_conv_type']="";
			if($options_ppc['fb_conv_type']!=="pages" ) {
				if(!isset($options_ppc['fb_conv_label'])) $options_ppc['fb_conv_label']="Lead";
				if($options_ppc['fb_conv_label']=="") $options_ppc['fb_conv_label']="Lead";
				$fbk_conv="
					fbq('track', '".$options_ppc['fb_conv_label']."'); ";
			}
		}
		?>
	<!-- #####
	LiveHelpNow Global Customizations
	##### -->
	<script type="text/javascript">
		<?=$frn_phone;?><?=$lhnWindowN;?>
		
		function frn_open_lhnwindow (type,category,action) {
			if(type===undefined) type=''; if(category===undefined) category='';  if(action===undefined) action=''; if(label===undefined) label='';
			if (typeof ga==='function') {
				if(type=="chat" || type=="") {
					try{ frn_chat_window( category,action,label ) }
					catch(err) { if (typeof ga==='function') ga('send', 'event', 'Live Chat', 'Chat Window Open Error', err); }<?=$fbk_conv;?>

				}
				else if(type=="email") {
					try{ frn_email_window( category,action,label ) }
					catch(err) { if (typeof ga==='function') ga('send', 'event', 'Live Chat', 'Email Window Open Error', err); }<?=$fbk_conv;?>

				}
			}
			else {
					if(type=="chat" || type=="") {frn_chat_window( category,action,label );}
					else if(type=="email") {frn_email_window( category,action,label );}<?=$fbk_conv;?>

			}
			if (typeof frn_outbrain_head==='function') frn_outbrain_head();
		}
	</script>
	<!-- #####
	LiveHelpNow Global Customizations
	##### -->
		
	<?php

	}

}
}


//////
// LHN SLIDEOUT_TAB JS for Footer
//////
add_action('wp_footer', 'frn_lhn_slideout', 10);
if(!function_exists('frn_lhn_slideout')) {
function frn_lhn_slideout() {
	
	$options_lhn = get_option('site_lhn_code');
	if(!isset($options_lhn['lhn_activation'])) $options_lhn['lhn_activation']=""; 

	global $frn_lhn_disable_tab;

	/*
	Cases:
	tab, no button mobile & desktop // easy. done.
	no tab, only a button mobile & desktop // easy. done.
	no tab, no button mobile & desktop // easy. done.
	
	Hardest: tab, button on dekstop, no button on mobile
		- no way to detect if button on page due to async loading (never know when it'll be there)
		- common on mobile pages
		- do I just put an invisible button in all cases?
	*/
	

	if($frn_lhn_disable_tab!=="Yes") {

		//if($frn_mobile || $options_lhn['lhn_inpageact']=="Activate") //Not used since these won't capture all the 
		lhn_invisible_button(); //assumes that there isn't a chat button at the same time as a tab

		//the following vars keeps LHN default button from being created by the slideout when none are in the page
		//Due to JS files being asynchronous, the default values are rarely loaded in time somehow or things overwritten somehow. 
		//These make sure that the necessar values are set just before the tab is created--and likely after the default values have run
		echo '

	<!-- LiveHelpNow Slideout -->
	<script type="text/javascript">'.frn_lhn_common("tab").'</script>
	<script async=\'async\' src="//www.livehelpnow.net/lhn/widgets/helpouttab/lhnhelpouttab-current.min.js?v=1.0" type="text/javascript" id="lhnscriptho"></script>
	<div id="lhnChatButton"></div>
	<!-- LiveHelpNow Slideout -->

	';

	}
}
}


//////
// LHN_SHORTCODE for Pages/Content
//shortcode requires "return" not "echo"
add_shortcode( 'lhn_inpage', 'frn_lhnshortcode_funct' );
if(!function_exists('frn_lhnshortcode_funct')) {
function frn_lhnshortcode_funct($atts){
	
	$lhn_account = "14160";  //Used only in the email URL bekow. Kept here to make it easy to find. Do not use the account number that has a -1. That's only for chat buttons. It will break the email window address.
	$default_chat_id="8557"; //Kept here to make the manual setting more obvious
	
	$options_lhn = get_option('site_lhn_code');
	$frn_lhnbtns = "";
	extract( shortcode_atts( array(

			//all versions
			'button' => '', //determines if chat or email
			'id' => '', //determines chat img or ID attribute is added to <A> link for text versions & email button
			'button_id' => '', //old and discontinued prior to 2016. Likely not on any sites anymore, but including to be safe.
			'title' => '', // Used only on <A> part of links
			'style' => '', //affects <A> tag only

			//defaults
			'text' => '',
			'class' => '',
			'offline' => 'Chat Offline', //original version so we need to maintain it
			'offline_text' => '', //just a backup in case people think this is what the attribute should be since we use mobile_text below.
			'offline_id' => '',
			'offline_class' => '',

			//mobile overrides
			'mobile' => '', //text version only
			'mobile_text' => '', //text version only
			'mobile_class' => '',
			'mobile_offline' => '', //text version only
			'mobile_offline_text' => '', // newer attribute in July 2017, kept since older versions of the shortcode use the other attribute.
			'mobile_offline_class' => '',

			//desktop overrides
			'desktop' => '', //text version only
			'desktop_text' => '', //text version only
			'desktop_class' => '',
			'desktop_offline' => '', //text version only
			'desktop_offline_text' => '', // newer attribute in July 2017, kept since older versions of the shortcode use the other attribute.
			'desktop_offline_class' => '',
			
			//email only
			'url' => '', //triggers email button
			'email_url' => '', //old version
			'alt' => '', //image only

			//Analytics customizations
			'category' => '', 
			'action' => '', //makes more sense for people familiar with Google Events reporting.
			'where_on_page' => '', //old attribute still used on niche sites mostly
			'label' => ''

		), $atts, 'lhn_inpage' ) );


	//Trim these since they are user entered features
	$button = strtolower(trim($button)); //main trigger for button types (chat,email,both) //if var empty, then both buttons will show. "both" is old and likely not used
	  $id = trim($id);
	  $button_id=trim($button_id);
	  if($button_id!=="") $id=$button_id;
	  $title = trim($title);
	  $alt = trim($alt);
	  $url = trim($url);
	  if($email_url!=="") $url=$email_url; //old version just converting to new attribute
	$text = trim($text);
	  if(strtolower($text)=="remove") $text="empty"; //due to potential confusion between phone number shortcode, this makes sure that only device-specific overrides can include "remove"
	  $class = trim($class);
	  $offline = trim($offline);
	  $offline_text = trim($offline_text);
	  if($offline_text!=="") $offline = $offline_text; //overrides defaults if defined
	  $offline_id = trim($offline_id);
	  $offline_class = trim($offline_class);
	$mobile=trim($mobile);
	  $mobile_text=trim($mobile_text);
	  if($mobile_text!=="") $mobile=$mobile_text;
	  $mobile_class = trim($mobile_class);
	  $mobile_offline = trim($mobile_offline);
	  if($mobile_offline_text!=="") $mobile_offline=$mobile_offline_text; // newer attribute in July 2017, kept since older versions of the shortcode use the other attribute.
	  $mobile_offline_class = trim($mobile_offline_class);
	$desktop=trim($desktop);
	  $desktop_text=trim($desktop_text);
	  if($desktop_text!=="") $desktop=$desktop_text;
	  $desktop_class = trim($desktop_class);
	  $desktop_offline = trim($desktop_offline);
	  if($desktop_offline_text!=="") $desktop_offline=$desktop_offline_text; // newer attribute in July 2017, kept since older versions of the shortcode use the other attribute.
	  $desktop_offline_class = trim($desktop_offline_class);
	$category = trim($category);
	  $action = trim($action);
	  $where_on_page = trim($where_on_page); //old version to help people know how to use the attribute, but as we have advanced, it's harder to match this use up with what we see in Analytics.
	  if($where_on_page!=="") $action=$where_on_page;
	  $label = trim($label);

	/// Master Manual Triggers (In page and tab)
	if(!isset($options_lhn['lhn_inpageact'])) $options_lhn['lhn_inpageact']="";

	if($options_lhn['lhn_inpageact']=="Activate") {

		/* //Discontinued 1/31/17 since we were able to add a custom JS function
		global $frn_lhn_hide_tab;
		if($frn_lhn_hide_tab!=="") 
		$frn_lhnbtns .= "
			<style type=\"text/css\" media=\"screen\">
				.lhn_help_btn {display:none;}
			</style>";
		*/




		////////
		// TEXT Preperation
		//$text was the first version, device versions came later, so ok to override text with the other two
		global $frn_mobile;
		if($frn_mobile) { //Mobile overrides
			if($mobile!=="") $text = $mobile;
			if($mobile_class!=="") $class = $mobile_class;
			if($mobile_offline!=="") $offline = $mobile_offline;
			if($mobile_offline_class!=="") $offline_class = $mobile_offline_class;
		}
		else { //desktop overrides
			if($desktop!=="") $text = $desktop;
			if($desktop_class!=="") $class = $desktop_class;
			if($desktop_offline!=="") $offline = $desktop_offline;
			if($desktop_offline_class!=="") $offline_class = $desktop_offline_class;
		}



		//Since the device type has already been identified above and the text by device overrides as well, 
		//it's easy to just see if remove is there. If so, then stop the function and return nothing.
		//unlike with the phone shortcode, there is no presidence for this prior to July 2017, so we can keep it simple and affect all devices and not just desktops
		if(strtolower($text)=="remove") {
			return "";
		}





		//if wanting both buttons side by side, this adds a style and div around the buttons to make it happen
		//If we don't, then we need to use a seperate shortcode for each button
		if($button=="" or $button=="both") $frn_lhnbtns .= "
			<style type=\"text/css\" media=\"screen\">.lhn_buttons div {display:inline;}</style>
			<div class=\"lhn_buttons\">";




		////////
		// Prepare Google Analytics Default Labels
		//customize GA Event category and/or action
		
		if($category!=="" && $action!=="") $ga_options=",'".$category."','".$action."'";
			elseif($category!=="") $ga_options=",'".$category."'";
			elseif($action!=="") $ga_options=",'','".$action."'";
			else $ga_options="";
		if($label!=="") $ga_options=$ga_options.",'".$label."'";
		



		////////
		// EMAIL button/text

		if($button=="email" || $button==="" || $button=="both") {

			$email_obj="";
			$window_open='';
			//when email image button used, JS takes over with reporting clicks to GA
			//but when it's text, we need to use a custom function that does both the window and ga reporting

			//////
			// Button Version
			// Both approaches very similar. One builds an IMG code while the other just inserts the text
			// Both options are wrapped with a link
			$email_text_class=""; //needed for tracking email button image clicks by default
			if($text=="") {
				$emailbtn_url=""; $email_type="Button";
				if(isset($options_lhn['lhn_inpage_email_btnurl'])) $lhn_inpage_email_btnurl=trim($options_lhn['lhn_inpage_email_btnurl']);
					else $lhn_inpage_email_btnurl="";
				if($url!=="") {
					//custom email button used in shortcode on a page
					$emailbtn_url = $url;
				}
				elseif($lhn_inpage_email_btnurl!=="") {
					//default email button defined in settings
					$emailbtn_url = $lhn_inpage_email_btnurl;
				}
				else $emailbtn_url = plugins_url()."/frn_plugins/images/lhn_email_2017.png";
				
				//The following base URLs haven't been used since 2014, but could still be on sites redesigned that year
				if($emailbtn_url!=="" and (stripos($emailbtn_url,"%%frn_imagebase%%")>=0 or stripos($emailbtn_url,"%%idomain%%")>=0 or stripos($emailbtn_url,"%%ldomain%%")>=0 or stripos($emailbtn_url,"%%frn_sitebase%%")>=0)) {
					//create image base url
					$options_sitebase = get_option('site_sitebase_code');
					if(isset($options_sitebase['frn_imagebase'])) {
						if(trim($options_sitebase['frn_imagebase'])!=="") $site_base = trim($options_sitebase['frn_imagebase']);
							else $site_base = site_url();
						$emailbtn_url=str_replace('%%frn_imagebase%%',$site_base,$emailbtn_url);
						$emailbtn_url=str_replace('%%frn_sitebase%%',$site_base,$emailbtn_url);
						$emailbtn_url=str_replace('%%idomain%%',$site_base,$emailbtn_url);
						$emailbtn_url=str_replace('%%ldomain%%',$site_base,$emailbtn_url);
					}
				}
				
				if($alt!=="") $alt='alt="'.$alt.'" ';
					else $alt='alt="Send us an Email" ';
				if($title!=="") $title=' title="'.$title.'" ';
					else $title=' title="Give us a chance to help." ';
				
				//making sure there are no security issues if person didn't include the https
				if(isset($_SERVER["HTTPS"])) {
					if ($_SERVER["HTTPS"] == "on") $emailbtn_url = str_replace('http://','https://',$emailbtn_url);
				}


				//email ID is only used in the link for greater control
				//Image already has an ID. 
				//Unlike classes, we cannot have more than one ID in an ID attribute.
				if(trim($emailbtn_url)!=="") $email_obj = '<img id="lhnEmailButton" src= "'.$emailbtn_url.'" '.$alt.$title.'border="0" />';

			}
			else {
				//Since $text is not blank, that means we won't use an image
				
				///////
				// Text Link Settings Set Up
				if($action=="") $email_text_class=" lhnEmailText"; //used for tracking JS for text only links in non-specific locations. Used only if action is not specifically defined.
				if($id!=="") $email_id='id="'.$id.'" ';
				$email_type="Text Link";

				$email_obj=$text;
				if($email_obj=="empty") {
					$email_obj="";
				}

			}
			
			$window_open="frn_open_lhnwindow('email'".$ga_options.");";
			
			//we have to have a class no matter what for LHN compatibility and automatic JS tracking.
			if($class!=="") $class=" ".$class;
			$class=' class="lhn_btn_email'.$email_text_class.$class.'" ';
			if($style!=="") $style=' style="'.$style.'" ';
			

			/* 
			//removed Analytics onClick 1/31/17 since JS file taking over the listening and reporting
			$ga_tracking="";
			if($button=="email" && $text!=="") {
				if($category!=="Live Chat") {
					if($where_on_page=="") $where_on_page="Email Toggles";
					$ga_tracking = "if(typeof ga==='function') ga('send', 'event', '".$category."', 'Custom Buttons: ".$where_on_page."', 'Email' ); return false;" ;
				}
			}
			*/
			//"return false;" is necessary here since there is no href. We don't want the page popping back to the top or any error performed.
			$frn_lhnbtns .= '<!-- LiveHelpNow On-Page Email '.$email_type.' --><a '.$email_id.$class.$style.$title.' href="#" onClick="'.$window_open.' return false;" >'.$email_obj.'</a>'; //href="https://www.livehelpnow.net/lhn/TicketsVisitor.aspx?lhnid=".$lhn_account."
			if($button=="email" && $text!=="" && $id!=="") $id=""; //clears variable just in case chat button is next
		}



		/////////
		// CHAT button/text

		if($button=="chat" or $button==="" or $button=="both") {

			//OLD METHOD: if(trim($options_lhn['lhn_inpage_chat_btnid'])!=="" or trim($id)!=="" and trim($text)=="") $frn_lhnbtns .= '<script type="text/javascript">lhnButtonN="'.$id.'";</script>';



			//// TEXT VERSION ONLY
			if($button=="chat" && $text!=="") {
				//IMPORTANT: the text versions of buttons cannot be used at the same time. The shortcode must be seperated.

				
				if($text=="empty") $text="";
				if($id!=="") $id=' id="'.$id.'" ';
				
				
				
				if($class!=="") $class=' class="lhn_btn_chat '.$class.'" ';
					else $class=' class="lhn_btn_chat"';
				if($style!=="") $style=' style="'.$style.'" ';
				
				if($title!=="") $title=' title="'.$title.'" ';


				///////
				/// Determine Chatting Hours
				
				/*
				//Disconnected this feature on 2/16/17 since the Call Center decided to go 24 hours at the beginning of Feb 2017.
				//But reactivated 5/2/2017 since Call Center changed hours again to cover all hours but Friday and Saturday nights.
					Call Center Hours (as of 2/29/16) via Mark:
					M-F:   6am to 12:15am
					S-S:     7am to 12:15am
					//will assume midnight since that gives 15 minutes for chat time before rep leaves

					Call Center Hours (as of 5/2/17) via Mark:
					M-F: 24 hours
					S-S: 7am to 11:59pm
				*/
				
				$wk_time_start="00:00:00"; //all zeros will make it work 24 hours a day
				$wkd_time_start="07:00:00";
				$timezone = 'America/Chicago';
				$date = new DateTime('now', new DateTimeZone($timezone));
				$time = $date->format("H:i:s");
				$dayofweek = $date->format("N"); //day of week as a number 1-7 (1=monday)
				$today = $date->format('Y-m-d');
				$time_full = strtotime($today." ".$time);
				$chat_on_wk = strtotime($today." ".$wk_time_start);
				$chat_on_wkd = strtotime($today." ".$wkd_time_start); //using "today" because today could be weekend, but remember today could be a weekday so this could still be positive
				$offline_true=false; 
				if($dayofweek<=5 && $time_full<$chat_on_wk ) $offline_true=true; //weekdays
				elseif($dayofweek>=6 && $time_full<$chat_on_wkd ) $offline_true=true; //weekends
				

				/*
				/////////
				// LHN ONLINE TEST JS VARIABLE
				// LHN offers a JS way to test if system is offline or not. But to avoid relying on JS after a page load, we use PHP and just assume someone is there to answer a chat.
				// But if everyone in the call center is logged out when we expect someone is there, when they click to start a chat, the system will automatically switch to an email form. So no one will be waiting for a rep to answer.
				// Their JS sets a global variable we can use in our own JS: 
				//<script> if (bLHNOnline == 0) {} </script>
				
				// UNUSED FOR NOW: //wasn't working and is unnecessary for now
					$yesterday = $date->sub(new DateInterval('P1D')); //P1D = previous one day //Must be after the "now" version is set
					$yesterday = $date->format('Y-m-d');
					$timestamp = $date->getTimestamp(); //Unix seconds, but doesn't consider timezone
				*/

				//////////
				//FOR TESTING:
				$test_off="";
				/*
					$chat_off = strtotime($today." ".$wk_time_start); //unimportant since it turns off 
					$test_off="<!--Dax_test";
					if( $now < $chat_off ) {
						$test_off .= "
							Chat is offline: $offline_true
							Now is before ".$wk_time_start." on ".$today." (dofw: ".$dayofweek.")
							time_full = $time_full
							chat_on_wk = $chat_on_wk
							chat_on_wkd = $chat_on_wkd
							"; 
					}
					else $test_off .= "<br />now is after ".$wk_time_start." on ".$today." (dofw: ".$dayofweek.")";
					$test_off.="-->";
					$frn_lhnbtns .= $test_off;
				*/

				///////
				//set the text that will be linked
				if($offline_true) {

					//offline text version attributes
					if(strtolower($offline)=="empty") $offline="";

					if($offline_id!=="") $id=$offline_id; //overrides the normal ID if present
					if($id!=="") $id=' id="'.$id.'" ';

					if($offline_class!=="") $class=$offline_class; //overrides the normal class if present
					if($class!=="") $class=' class="lhn_btn_chat '.$class.'" ';

					if(strtolower($offline)=="remove") $text = "<!-- Chat is offline and link removed -->";
					elseif(strtolower($offline)=="email") {
						//This switches to an email link if we want that when chat is offline (leaving a chat link will make it happen anyway but we are being specific here for reporting and to offer customization)
						if($text!=="") $email_text="Chat Offline"; //if text isn't blank, then use default text for email link since that at least indicates we won't rely just on CSS
							else $email_text=""; //Means there wouldn't be any text used for the online version anyway
						$email_window_open="frn_open_lhnwindow('email'".$ga_options.");";  
						$text = '<!-- LiveHelpNow Email Link Instead of Chat --><a'.$id.$class.$style.' href="#" onClick="'.$email_window_open.' return false;" >'.$email_text.'</a>';
					}
			    	else {
			    		//default/typical use
						$text="<span".$id.$class.">".$offline."</span>";
			    	}

		    	}

		    	///////
		    	// Determine if linked or not
				//if chat is offline, don't include the link, just the offline text (defined by the times above)
				elseif(!$offline_true) {
					//The following has to be online to avoid WordPress auto paragraphs that break the scripts. 
					//Jonny's new Slate template utilizes WP's auto filtering that adds the paragraphs for line returns.
					//The style that removes the button below removes all in-page chat buttons on the page. So you can't use a text option in one place and a button version in another...for now.
					// We clear out previously saved code for just the text version since line returns are sensitive in this approach
					//"return false;" is necessary here since there is no href. We don't want the page popping back to the top or any error performed.
					$frn_lhnbtns = "<!-- LiveHelpNow On-Page Chat Text Link -->".$test_off."<a ".$id.$class.$style." href=\"#\" onClick=\"frn_open_lhnwindow('chat'".$ga_options."); return false; \">".$text."</a>";
					//get text from content, scan it for button, if found, no inv button

					add_action('wp_footer', 'lhn_invisible_button', 3); //required when we use text links. It adds the button to the footer.

					//Tne following tracking JS is not needed now since we have an insertion in our JS file (1/31/17): if(typeof ga==='function') ga('send', 'event', '".$category."', 'Custom Buttons: ".$where_on_page."', 'Chats'); return false;
					//<style>#lhnContainerDone {display:none;}</style>
				}
				else {
					$frn_lhnbtns .= $text; //typically unused but kept here as a backup.
				}

				
	    			

	    		//for testing variables
		    		//$time_friendly = $today." ".$time; //$date->format("Y-m-d H:i:s"); //for testing
		    		//$zone=$date->getTimezone(); $zone=$zone->getName();
		    		//$frn_lhnbtns .= "<br>today=".$today." <br>dayofweek=".$dayofweek." <br>turn on week=".$chat_on_wk." (".date("Y-m-d H:i:s", $chat_on_wk).") <br>turn on wkend=".$chat_on_wkd." (".date("Y-m-d H:i:s", $chat_on_wkd).") <br>time=".$time." <br>time_full=".$time_full." (".$time_friendly.") <br>zone=".$zone; //for testing
	   			//
	   			// before combining all js into the same file
	   			//<script src= "'.plugins_url().'/frn_plugins/lhnchatbutton-defaults.min.js?v=1.0" type="text/javascript" ></script>


				//The following account ID variable is required to avoid issues. 
				//Only chat buttons and the slideout use -1 at the end. 

			}
			else {
				
				////////////
				// CHAT IMAGES

				//set global var for other functions to use since LHN's system requires a button to be on the page--whether hidden or displayed
				global $frn_lhn_btn_active;
				$frn_lhn_btn_active=true; 


				// DETERMINE BUTTON ID
				// ID in the shortcode when "text" is empty should include the chat button ID
				// When text is filled out, ID becomes a CSS ID
				// ID in the shortcode is already set. The following happens if it's not set.
				// Makes sure the ID field wasn't mistaken as a CSS ID option or the button
				if($id!=="") {
					if(!is_numeric($id)) $id=""; //clear it out since we use it later for button customizations and don't want the presence of it looking for custom chat button in the LHN system
				}
				else {
					if(!isset($options_lhn['lhn_inpage_chat_btnid'])) $options_lhn['lhn_inpage_chat_btnid']="";
					$options_lhn['lhn_inpage_chat_btnid'] = trim($options_lhn['lhn_inpage_chat_btnid']);
					if($options_lhn['lhn_inpage_chat_btnid']!=="") $id = $options_lhn['lhn_inpage_chat_btnid']; 
					else $id=$default_chat_id; //default chat button ID set higher in code
				}
				$id = "var lhnButtonN = ".$id.";";

				//removed async='async' since button id issues with invisible buttons were causing a problem.
				$frn_lhnbtns .= "
				
				<!-- LiveHelpNow On-Page Chat Button -->
				<script type=\"text/javascript\">
					".$id.frn_lhn_common("button")."
				</script>
				<script async='async' src=\"//www.livehelpnow.net/lhn/widgets/chatbutton/lhnchatbutton-current.min.js?v=1.0\" type=\"text/javascript\" id=\"lhnscript\"></script>
				<!-- End LiveHelpNow On-Page Chat Button -->
				
				";
			}
		}
		if($button=="" or $button=="both") $frn_lhnbtns .= "
			</div><!-- End LiveHelpNow On-Page Buttons -->\n\n";
		
		return $frn_lhnbtns;


	}
	else {
		if($button=="chat") $frn_lhnbtns .= "
				<!-- LiveHelpNow On-Page CHAT Button -->
				<!-- Normally the button would be in this location, but the buttons are deactivated -->\n\n";
		elseif($button=="email") $frn_lhnbtns .= "
				<!-- LiveHelpNow On-Page EMAIL Button -->
				<!-- Normally the button would be in this location, but the buttons are deactivated -->\n\n";
		else $frn_lhnbtns .= "
				<!-- LiveHelpNow On-Page Buttons -->
					<!-- Normally both buttons would be in this location, but they are deactivated -->
				<!-- End LiveHelpNow On-Page Buttons -->\n\n";
	}
	
	return $frn_lhnbtns;
}
}


////////////
// LHN SLIDEOUT TAB_DEACTIVATION TRIGGER
////////////
if(!function_exists('frn_lhn_tab_deactivate')) {
function frn_lhn_tab_deactivate() {
	
	///Making this global so we don't have to run this function several times on a page
	global $frn_lhn_disable_tab;
	$frn_lhn_disable_tab="No";  //The tab is enabled by default

	//////
	// MANUAL TAB ACTIVATION
	$options_lhn = get_option('site_lhn_code');
	if(!isset($options_lhn['lhn_activation'])) $options_lhn['lhn_activation']=""; 


	//////
	// AUTOMATIC TAB DEACTIVATION
	//See if manually selected not to check for "contact" in the page title
	if($options_lhn['lhn_activation']=="Activate") {
		//If activated, now to the more complicated piece
		
		if(isset($options_lhn['contactpg_remove'])) $contactpg_remove=$options_lhn['contactpg_remove']; 
		if($contactpg_remove!=="No" || $contactpg_remove==="") {
			//check if "contact" is in page title
			if(strpos(strtolower(get_the_title()),"contact")!==false) $frn_lhn_disable_tab="Yes";
			//echo "<!--testit, ".strpos(strtolower(get_the_title()),"contact").", ".$frn_lhn_disable_tab.", ".get_the_title()."-->";
		}

		//Check if page id is in list of ids not to show the tab for
		if(isset($options_lhn['tabdisable_pgID'])) $tabdisable_pgID=trim($options_lhn['tabdisable_pgID']); 
		if($tabdisable_pgID!=="") {
			$pgidarray = explode ( "," , $tabdisable_pgID);
			$page_id = get_the_ID();
			//Rotates through list of IDs, if true, then sets boolean that disables rest of function
			if($page_id!=="") {
				foreach ($pgidarray as $id) {
					if($page_id==$id) $frn_lhn_disable_tab = "Yes";
				}
			}
		}
	}
	else $frn_lhn_disable_tab = "Yes";
}
}



//////////////////////////////////
// LHN Invisible Button for Text Links (LHN_INV_BUTTON)
//////////////////////////////////
function lhn_invisible_button() {
	//This adds the LHN invisible button to the footer--in it's own function for positioning flexibility
	//This must be placed between the BODY tags
	//This will only happen when there is a text link or a floating tab and no button.
	//If it happens when buttons are on the page, it might mess up their displaying
	// -1 for account number is only required for chat buttons

	global $frn_lhn_btn_active;
	if(!isset($frn_lhn_btn_active)) 
		echo "
	<script type=\"text/javascript\">
		var lhnButtonN = -1;".frn_lhn_common("button")."
	</script>
	<script async='async' src=\"//www.livehelpnow.net/lhn/widgets/chatbutton/lhnchatbutton-current.min.js?v=1.0\" type=\"text/javascript\" id=\"lhnscript\"></script>
	";
}


//////////////////////////////////
// LHN GET WEBSITE CALL_IN_CODE
//////////////////////////////////
//Get call code (and save) from central DB if not stored in site already
if(!function_exists('frn_external_db')) {
function frn_external_db() {
	
	$frn_domain = str_replace("http://","",str_replace("https://","",str_replace("www.","",site_url())));
	$slash_pos = strpos($frn_domain,"/",0);
	if($slash_pos) $frn_domain = substr($frn_domain,0,$slash_pos);
	
	
	//creates array of domains and our internal display codes to show in the LHN Call Center rep chat windows
	$display_codes = array (				
	"michaelshouse.com" => "Web MHouse",
	"skywoodrecovery.com" => "Web Skywood",
	"talbottcampus.com" => "Web Talbott",
	"calvarycenter.com" => "Web Calvary",
	"lcaccepted.com" => "Web L+C",
	"dualdiagnosis.org" => "Web Dual (DD.org)",
	"rehab-international.org" => "Web RIOS (R-I.org)",
	"rehabandtreatment.com" => "Web Rehab (PPC Site)",
	"interventionsupport.com" => "Web INTV (IS.com)",
	"foundationsrecoverynetwork.com" => "Web FRN (FRN.com)",
	"heroesinrecovery.com" => "Web Heroes (HIR.com)",
	"mentalhealthtreatment.net" => "Web Mental (MHT.com)",
	"theoakstreatment.com" => "Web The Oaks",
	"blackbearrehab.com" => "Web Black Bear",
	"addictionhelpcenter.com" => "Web Niche (Addictn)",
	"lsdabusehelp.com" => "Web Drug (LSD)",
	"ptsdtreatmenthelp.com" => "Web Niche (PTSD)",
	"thecanyonmalibu.com" => "Web Canyon",
	"foundationsevents.com" => "Young, Jordan (Events)",
	"clonazepamaddictionhelp.com" => "Web Drug (Clonazpm)",
	"amphetaminerisks.com" => "Web Drug (Amphtmns)",
	"lorazepamabusehelp.com" => "Web Drug (Lorazpm)",
	"bipolardisorderscenters.com" => "Web Niche (BiPolar)",
	"methadoneabusehelp.com" => "Web Drug (Meth)",
	"rohypnolabusetreatment.com" => "Web Drug (Roofies)",
	"xanaxaddictionhelp.com" => "Web Drug (Xanax)",
	"alcoholrehabadvice.com" => "Web Drug (Alcohol)",
	"heroindetoxrehab.com" => "Web Drug (Heroin)",
	"alprazolamaddictionhelp.com" => "Web Drug (Alprazlm)",
	"klonopinaddictionhelp.com" => "Web Drug (Klonopin)",
	"freeaddictionhotline.com" => "Web Niche (Addictn)",
	"tramadolabusehelp.com" => "Web Drug (Tramadol)",
	"adderallabusetreatment.com" => "Web Drug (Adderall)",
	"opiaterehabtreatment.com" => "Web Drug (Opiate)",
	"ritalinaddictioninfo.com" => "Web Drug (Ritalin)",
	"traumaabusetreatment.com" => "Web Niche (Trauma)",
	"painkiller-addiction-treatment.com" => "Web Drug (Painkillr)",
	"hydrocodonehelp.com" => "Web Drug (Hydrocodn)",
	"morphineaddictionhelp.com" => "Web Drug (Morphine)",
	"drug-addiction-help.org" => "Web Niche (Addictn)",
	"percocetabusehelp.com" => "Web Drug (Percocet)",
	"oxycodoneaddictionhelp.com" => "Web Drug (Oxycodone)",
	"addictioncounselorreferrals.com" => "Web Niche (Counslrs)",
	"ritalinabusehelp.com" => "Web Drug (Ritalin)",
	"gethelpforeatingdisorders.com" => "Web Niche (EatingDis)",
	"desmoinesaddictiontreatment.com" => "Web Geo (DesMoines)",
	"marijuanadrugtreatment.com" => "Web Drug (Marijuana)",
	"alcoholicsoccermom.com" => "Web Drug (Alcohol)",
	"overdosedrugalcohol.com" => "Web Drug (Alcohol)",
	"inhalantaddictiontreatment.com" => "Web Drug (Inhalant)",
	"anchoragedrugtreatment.org" => "Web Geo (Anchorage)",
	"outpatientcenters.org" => "Web Niche (Outpatnt)",
	"alcoholismtreatmentcalifornia.com" => "Web Geo (California)",
	"ativanabusehelp.com" => "Web Drug (Ativan)",
	"cocainerehabtreatment.com" => "Web Drug (Cocaine)",
	"addictiontreatmenttherapy.com" => "Web Niche (Addictn)",
	"residential-drug-rehab-help.com" => "Web Niche (Residntl)",
	"alcoholtreatmenthelp.org" => "Web Drug (Alcohol)",
	"opiumabusetreatment.com" => "Web Drug (Opium)",
	"lgbtdrugrehab.com" => "Web Niche (LGBT)",
	"functionalalcoholichelp.com" => "Web Drug (Alcohol)",
	"interventionhelpcalifornia.com" => "Web Geo (California)",
	"inpatientrehabtreatment.com" => "Web Niche (Inpatnt)",
	"valiumaddictionhelp.com" => "Web Drug (Valium)",
	"ambien-addiction-treatment.com" => "Web Drug (Ambien)",
	"prescriptiondrugabusehelp.com" => "Web Drug (Prscrptns)",
	"drugtreatmentinsurance.com" => "Web Niche (Ins.)",
	"lifewithoutdepression.org" => "Web Niche (Depressn)",
	"sexaddictionrehab.org" => "Web Niche (Sex)",
	"oxycontintreatmenthelp.com" => "Web Drug (Oxycontin)",
	"buprenorphineaddictionhelp.com" => "Web Drug (Buprnrphn)",
	"stopsuboxoneabuse.com" => "Web Drug (Suboxone)",
	"lortababusehelp.com" => "Web Drug (Lortab)",
	"sacramentodrugrehab.org" => "Web Geo (Sacramento)",
	"steroidabusehelp.com" => "Web Drug (Steroids)",
	"aboutrecovery.com" => "Web Niche (Recovery)",
	"drug-rehab-experts.org" => "Web Niche (Experts)",
	"dialecticalbehaviortherapy-dbt.com" => "Web Niche (DBT)",
	"albuquerquerehabs.com" => "Web Geo (Albuquerqu)",
	"frnalumni.com" => "Alumni",
	"vicodin-rehab-guide.com" => "Web Drug (Vicodin)",
	"alcohol-rehab-guide.org" => "Web Drug (Alcohol)",
	"interventionhelptennessee.com" => "Web Geo (Tenn INTV)",
	"oklahomacitydrugrehab.org" => "Web Geo (Ok. City)",
	"lorcetabusehelp.com" => "Web Drug (Lorcet)",
	"traumarehabcenters.com" => "Web Niche (Trauma)",
	"alcoholismtreatmenttennessee.com" => "Web Geo (Alcohol)",
	"phoenixdrugtreatment.org" => "Web Geo (Phoenix)",
	"finddrugtreatment.com" => "Web Niche (FindTrmt)",
	"sandiegodrugrehabcenters.com" => "Web Geo (San Diego)",
	"njdrugtreatmentcenters.com" => "Web Geo (New Jersey)",
	"alcoholismhelplines.com" => "Web Drug (Alcohol)",
	"california-drug-treatment.org" => "Web Geo (California)",
	"gamblingtreatmenthelpline.com" => "Web Niche (Gambling)",
	"louisvilledrugrehabs.com" => "Web Geo (Louisville)",
	"pain-management-treatment.com" => "Web Drug (Painkillr)",
	"austinalcoholtreatment.org" => "Web Geo (Austin, TX)",
	"depressiontherapyprograms.com" => "Web Niche (Depressn)",
	"kansascityrehabdetox.com" => "Web Geo (KS City)",
	"avoidarelapse.com" => "Web Niche (Relapse)",
	"percodanabusehelp.com" => "Web Drug (Percodan)",
	"propoxypheneabusetreatment.com" => "Web Drug (Propxyphn)",
	"vancouveralcoholrehab.com" => "Web Geo (Vancouver)",
	"minneapolisalcoholtreatment.com" => "Web Geo (Minneapols)",
	"bostondrugtreatment.org" => "Web Geo (Boston)",
	"madisonrehabcenters.com" => "Web Geo (Madison)",
	"saltlakecityaddictionclinics.com" => "Web Geo (Salt Lake)",
	"atlantaaddictiontreatment.com" => "Web Geo (Atlanta)",
	"demeroladdictionhelp.com" => "Web Drug (Demerol)",
	"drugaddictioncenters.org" => "Web Niche (Centers)",
	"choosesoberliving.com" => "Web Niche (Sobr Lvg)",
	"tennesseedrugtreatment.org" => "Web Geo (Tennessee)",
	"ohiodrugrehabcenters.com" => "Web Geo (Ohio)",
	"percodantreatmentcenters.com" => "Web Drug (Percodan)",
	"portlanddrugrehab.org" => "Web Geo (Portland)",
	"drugaddictiontherapists.com" => "Web Niche (Therapst)",
	"chicagodrugrehabs.org" => "Web Geo (Chicago)",
	"boiserehabtreatment.com" => "Web Geo (Boise, ID)",
	"dallasdrugrehabs.com" => "Web Geo (Dallas, TX)",
	"canadaaddictionhelp.com" => "Web Geo (Canada)",
	"rehabtreatmentinsurance.com" => "Web Niche (Ins.)",
	"bipolar-disorder-help.com" => "Web Niche (BiPolar)",
	"sanjoseaddictiontreatment.com" => "Web Geo (San Jose)",
	"denvercoloradorehabs.com" => "Web Geo (Denver, CO)",
	"drugdetoxprograms.org" => "Web Niche (Detox)",
	"houstonrehabdetox.com" => "Web Geo (Houston)",
	"darvonabusetreatment.com" => "Web Drug (Darvon)",
	"addictiontreatmentandhelp.com" => "Web Niche (Addictn)",
	"soberhero.com" => "Web Niche (Sober)",
	"tyloxaddictionhelp.com" => "Web Drug (Tylox)",
	"seattledrugrehab.org" => "Web Geo (Seattle)",
	"orlandodrugtreatment.org" => "Web Geo (Orlando)",
	"detroitdrugrehabs.com" => "Web Geo (Detroit)",
	"sanfranciscorehabs.com" => "Web Geo (San Fran)",
	"raleighdurhamdrugrehab.com" => "Web Geo (Raleigh D)",
	"darvocetaddictionhelp.com" => "Web Drug (Darvocet)",
	"philadelphiaalcoholtreatment.org" => "Web Geo (Philadelph)",
	"maliburehabtreatment.com" => "Web Geo (Malibu)",
	"testsite.foundationsrecoverynetwork.com" => "FRN Test Site",
	"drugrehabtv.com" => "Web Niche (TV)"
	);
	
	if(isset($display_codes[$frn_domain])) $frn_lhn_dc = $display_codes[$frn_domain];
		else $frn_lhn_dc = "[none]"; //$new_frn_domain; //
	
	return $frn_lhn_dc;
	
}
}




?>