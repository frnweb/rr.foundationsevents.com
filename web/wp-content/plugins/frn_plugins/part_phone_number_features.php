<?php defined( 'ABSPATH' ) OR die( 'No direct access.' );

/* NOTES:
	"part" php files are in the works in an attempt to make it easier to add to any theme without requiring the massive main plugin PHP file.
	Although this file is seperate, most of the other files depend or reference something from this file. 
	This file only includes frontend changes for users. 
	Admin features are still in the admin php.
	Move to it's own file on 3/7/17


TABLE OF CONTENTS:
	CLEAN_UP code
	PHONE_PREP code 
	FRN_SHORTCODE
	AUTOSCAN
	JAVASCRIPT_SCAN



*/




//////////////////////////////////
// FRN_PHONE_SETUP FEATURES
//////////////////////////////////

add_shortcode( 'frn_phone', 'frn_phone_funct' );
add_action('wp_footer', 'frn_phone_js_scan', 3); //3 to make sure the script is added below chartbeat, etc.


//////////
// CLEAN_UP PHONE NUMBER
//Used in phone number and copyright shortcode functions below. Used to format the number friendly for all mobile phones.
if(!function_exists('frn_clean_number')) {
function frn_clean_number($number) {
	$frn_number=trim($number);
	//Removed the script clean up since Rehab and Treatment no longer use the DialogTech SCRIPT approach to switching out numbers
	//if(strpos($number,"script")!==false) { 
		//Cleans it up to use as mobile phone number--works with images only
		if(trim($number)!=="") $frn_number = trim(strip_tags(stripslashes($number))); //cleans out all HTML and quotes  
		else {
			$options = get_option('site_phone_number');
			$frn_number = trim(strip_tags(stripslashes($options['site_phone']))); //cleans out all HTML and quotes
		}
		
		if($frn_number!=="") {
			$frn_number = str_replace(array(" ","(",")","-"),"",$frn_number);
			if(!is_numeric($frn_number)) $frn_number=preg_replace('/\D/',"",$frn_number); //strips all but numbers in case some text is left
			
			//Format the phone to still look pretty although the JS portion will reformat again
			if(strlen($frn_number)==11) {$frn_phoneFormat = '/^1?(\d)([0-9]..)([0-9]..)([0-9]...)$/'; //starts with 1
			  $frn_number = preg_replace($frn_phoneFormat,'$1-$2-$3-$4',$frn_number);}
			else if(strlen($frn_number)==10) {$frn_phoneFormat = '/([0-9]..)([0-9]..)([0-9]...)$/'; //doesn't start with 1
			  $frn_number = preg_replace($frn_phoneFormat,'1-$1-$2-$3',$frn_number);}
			else if(strlen($frn_number)==13){$frn_phoneFormat = '/([0-9]..)([0-9]..)([0-9]..)([0-9]...)$/'; //international
			  $frn_number = preg_replace($frn_phoneFormat,'$1-$2-$3-$4',$frn_number);}
			else if(strlen($frn_number)==14 && substr($frn_number,1)=="("){$frn_phoneFormat = '/([0-9]...)([0-9]..)([0-9]..)([0-9]...)$/'; //international
			  $frn_number = preg_replace($frn_phoneFormat,'$1-$2-$3-$4',$frn_number);}
		}
	//}
	return $frn_number;
}
}


////////////
// PHONE_Preperation for Functions
//
if(!function_exists('frn_phone_prep')) {
function frn_phone_prep($settings) {
	//used only to build what actually prints to the page.
	global $frn_mobile;
	$wrapped = trim($settings[0]);
	$start="";$end="";
	$options = get_option('site_phone_number');
	//phone_linking = processing method
	if(!isset($options['phone_linking'])) $options['phone_linking']="";
	
	$turn_on = true;
	if($frn_mobile) {
		if($options['phone_linking']=="N" && ($options['auto_scan']=="D" || $options['auto_scan']=="")) $turn_on=false;
	}
	else $turn_on = false;
	
	
	if($turn_on) {
		
		if(isset($options['site_phone'])) $number_in_settings=frn_clean_number($options['site_phone']); else $number_in_settings="";
		
		if(isset($settings[1])) $frn_number = trim($settings[1]); else $frn_number="";
		
		//removed && strpos($wrapped,"script")===false from the next IF statement since SCRIPTs are no longer used in Rehab and Treatment phone numbers
		if(($frn_number=="0" || $frn_number=="") ) $frn_number = frn_clean_number(trim(strip_tags(stripslashes($wrapped))));  //This portion could cause us issues since the wrapped portion won't be stripped of things.
			//elseif(strpos($frn_number,"script")!==false) $frn_number="";
		if(isset($settings[2])) $ga_phone_category = trim($settings[2]); else $ga_phone_category = "Phone Numbers";
		if(isset($settings[3])) $ga_phone_location = trim($settings[3]); else $ga_phone_location = "Phone Clicks [General]";
		if(isset($settings[4])) $ga_phone_label = trim($settings[4]); else $ga_phone_label = "Calls";
		if(isset($settings[5])) $style = trim($settings[5]); else $style = "";
		if(isset($settings[6])) $class = trim($settings[6]); else $class = "";
		if(isset($settings[7])) $id = trim($settings[7]); else $id = "";
		if(isset($settings[8])) $title = trim($settings[8]); else $title = "";
		if(isset($settings[9])) $lhn_tab_disabled = trim($settings[9]); else $lhn_tab_disabled = "";  //javascript only feature -- used to keep the main phoneBasic variable from being set on the page so that no number shows in the slideout
		if(isset($settings[10])) $intl_prefix = trim($settings[10]); else $intl_prefix = "";  //javascript only feature -- since Zander manually added in the international code in front of IfByPhone script, we had to add it to the number manually since the international pattern then couldn't be recognized

		if($frn_number!=="") {
			$frn_number_check=frn_clean_number($frn_number);
			if($frn_number_check!==frn_clean_number($number_in_settings)) {  // && preg_replace('/\D/',"",$frn_number)!==preg_replace('/\D/',"",$number_in_settings)
				$ga_phone_label=$frn_number;
		}}
		
		if($id!=="") $id=' id="'.$id.'"';
		if($class!=="") $class=' class="'.$class.'"';

		//Determine if javascript will be used to link the numbers or not
		//removed strpos($wrapped,"script")!==false &&  from the next IF statement since we no longer use the DialogTech feature
		//phone_linking = processing method
		if($options['phone_linking']!=="JS") {
			//PHP Version (and script not in phone field)
			if($style==="") $style = ' style="white-space:nowrap;"';
				else $style = ' style="'.$style.'"';
			if($title!=="") $title = ' title="'.$title.'"';
			if($ga_phone_label!=="") $ga_phone_label=",'".$ga_phone_label."'";
				else $ga_phone_label=",'Calls'";
			//do not include "return false in an onClick unless you don't want the href followed"
			$start = ' <a '.$id.$class.$title.' href="tel:'.$frn_number.'"'.$style.' onClick="frn_reporting(\'phone\',\''.$ga_phone_category.'\', \''.$ga_phone_location.'\''.$ga_phone_label.');" >'; //if(typeof ga===\'function\') ga(\'send\', \'event\', \''.$ga_phone_category.'\', \''.$ga_phone_location.'\''.$ga_phone_label.'\'
			$end = "</a>";
		}
		/* 
		//Discontinued by Dax on 12/30/16 since the new DialogTech approach doesn't require using scripts in every phone number location.
		//It instead uses a phone number scan on the page to replace our number with theirs.
		elseif(strpos($wrapped,"script")!==false || $options['phone_linking']=="JS") {
			//JavaScript approach (applies if "script" is in phone number field)
			if($ga_phone_category=="") $ga_phone_category='ga_phone_category="Phone Numbers" ';
				else  $ga_phone_category='ga_phone_category="'.$ga_phone_category.'" ';
			if($ga_phone_location=="") $ga_phone_location='ga_phone_location="Phone Clicks [General]" ';
				else  $ga_phone_location='ga_phone_location="'.$ga_phone_location.'" ';
			if($ga_phone_label!=="") $ga_phone_label='ga_phone_label="'.$ga_phone_label.'" ';
			if($lhn_tab_disabled!=="") $lhn_tab_disabled='lhn_tab_disabled="'.$lhn_tab_disabled.'" ';
			if($intl_prefix!=="") $intl_prefix = 'intl_prefix="'.$intl_prefix.'" ';
			
			if($style!=="") $frn_number_style = 'frn_number_style="'.$style.'" ';
			else $frn_number_style = "";
			
			$start = '<span id="frn_phones '.$id.'" '.$class.$frn_number_style.$ga_phone_category.$ga_phone_location.$ga_phone_label.$lhn_tab_disabled.$intl_prefix.' >';
			$end = "</span>";
		}
		*/
	}
	else {
		if($wrapped!=="") {
			$start = '<span '.$id.$class.' style="white-space:nowrap;" >';
			$end = "</span>";
		}
		else {
			$start = '<span '.$id.$class.' >';
			$end = "</span>";
		}
	}
	
	return $start.$wrapped.$end;
}
}



////////////
// Phone FRN_Shortcode: If shortcode found, this is what splits out the variables into our SPAN tags and phone number.
//shortcode requires "return" not "echo"
if(!function_exists('frn_phone_funct')) {
function frn_phone_funct($atts){
	extract( shortcode_atts( array(
		
		'number' => '', //overrides default number
		'only' => '', //if yes, only shows the number
		'text' => '', //substitutes for number when linking
		
		//Defaults
		'id' => '', //applied to <A> tag for text/phone number versions, or <IMG> tags for images
		'class' => '', //applied to SPAN, not links
		'style' => '', //newer version since easier to remember -- used for inline styles
		'css_style' => '', //older version prior to 2016 -- used for inline styles
		'title' => '', //added to links only -- i.e. smartphones only

		//Mobile overrides
		'mobile' => '',
		'mobile_text' => '', //here in case someone mistakes this to be like the rest
		'mobile_id' => '',
		'mobile_class' => '',
		'mobile_category' => '', //overrides Analytics defaults
		'mobile_action' => '', //overrides Analytics defaults

		//Desktop overrides
		'desktop' => '',
		'desktop_text' => '', //here in case someone mistakes this to be like the rest
		'desktop_url' => '', //if desktop text defined, it can link the text with a URL to go to a contact us page, for example
		'desktop_id' => '',
		'desktop_class' => '',
		'desktop_category' => '', //overrides Analytics defaults
		'desktop_action' => '', //overrides Analytics defaults

		//Analytics defaults
		'category' => '', //updated 2016 since other versions hard to remember, "ga_phone_category" version assigned to this later in code
		'action' => '', //updated 2016 since other versions hard to remember, "ga_phone_location" version assigned to this later in code
		'ga_phone_category' => 'Phone Numbers',
		'ga_phone_location' => 'Phone Clicks [General]',
		'ga_phone_label' => 'Calls', //changed to phone number if different than site default

		//Image options for when phone numbers are used in them (very popular on niche sites)
		//There is no mobile or desktop only image options
		'url' => '', //initial attribute, but since all other image attributes required "image", added image_url to keep things consistent. Kept since versions from 2013 or 2014 may still use it.
		'image_url' => '',
		'image_id' => '', //ID will be used if this is blank
		'image_class' => '',
		'image_style' => '',
		'image_title' => '',
		'alt' =>'', //was initial use but since all other image attributes required "image", added image_alt to keep things consistent. Kept since versions from 2013 or 2014 may still use it.
		'image_alt' =>'',

		//Likely not used anymore (prior to 2015)
		'intl_prefix'=> '', //was used on PPC only when testing international numbers -- made regex too hard when detecting number in text blocks
		'lhn_tab_disabled'=> '', //used to disable the chat tab if preferred on a specific page where this shortcode used. A forgotton feature by 2017.
		'frn_number_style' => '' //old option some rare situations may still use -- now just assigned to inline $style if it isn't defined

	), $atts, 'frn_phone' ) );
	

	//Clean up user values since spaces can negatively affect some situations
	$number=trim($number);
	  $only=strtolower(trim($only));
	  $text=trim($text);
	  $id=trim($id);
	  $class=trim($class);
	  $style=trim($style);
	  $css_style=trim($css_style); //old version likely used on niche sites prior 2016
	  if($css_style!=="") $style=$css_style; 
	  $title=trim($title);
	$url=trim($url);
	  if($url!=="") $image_url=$url;
	  $image_url=trim($image_url);
	  $image_id=trim($image_id);
	  $image_class=trim($image_class);
	  $image_style=trim($image_style);
	  $image_title=trim($image_title);
	  $alt=trim($alt);
	  $image_alt=trim($image_alt);
	$mobile=trim($mobile);
	  $mobile_text=trim($mobile_text);
	  if($mobile_text!=="") $mobile=$mobile_text;
	  $mobile_id=trim($mobile_id);
	  $mobile_class=trim($mobile_class);
	  $mobile_category=trim($mobile_category);
	  $mobile_action=trim($mobile_action);
	$desktop=trim($desktop);
	  $desktop_text=trim($desktop_text);
	  if($desktop_text!=="") $desktop=$desktop_text;
	  $desktop_url=trim($desktop_url); //used to link the text to a page on the site
	  $desktop_id=trim($desktop_id);
	  $desktop_class=trim($desktop_class);
	  $desktop_category=trim($desktop_category);
	  $desktop_action=trim($desktop_action);
	$category=trim($category);
	  $action=trim($action);
	  $ga_phone_label=trim($ga_phone_label);
	  $ga_phone_location=trim($ga_phone_location);
	  $ga_phone_category=trim($ga_phone_category);

	//likely no longer used -- prior to 2015
	$intl_prefix=trim($intl_prefix);
	  $lhn_tab_disabled=trim($lhn_tab_disabled);
	  $frn_number_style=trim($frn_number_style);




	/////
	/// Load Global Values
	global $frn_mobile;
	$options = get_option('site_phone_number');



	/////////
	/// Analytics Event Label Unification
	//As more developers were involved, realized simplifying labels to match with GA was easier. March 2017
	//kept old variables in code below -- fewer code changes
	if($category!=="") $ga_phone_category=$category;
	if($action!=="") $ga_phone_location=$action;



	//////
	/// Device Overrides
	if($frn_mobile) {
		if($mobile!=="") $text=$mobile;
		if($mobile_category!=="") $ga_phone_category=$mobile_category;
		if($mobile_action!=="") $ga_phone_location=$mobile_action;
	}
	else {
		if($desktop!=="") $text=$desktop;
		if($desktop_category!=="") $ga_phone_category=$desktop_category;
		if($desktop_action!=="") $ga_phone_location=$desktop_action;
		if($desktop_class!=="") $class=$desktop_class;
	}


/*
	text var:
		the text included is linked with the phone number and used for all platforms
		empty = removes all text, but keeps the link (best for CSS or image backgrounds)
		remove = initially removed everything for desktop only--but transitioned to a desktop "remove" version to help clarify
	desktop var:
		the text included is only used for desktop versions. Mobile platforms will use the text values--whether default or custom.
		if using remove, then it means it'll be removed for desktops only. Mobile will still get the full linked text --whatever text is in the var
		if using empty, then it means the link or span will still be there but the text version will be used for mobile
*/




	/////////
	//// PHONE NUMBER PROCESSING NOT NEEDED
	/////////

	//If desktop and we have it set to remove everything, just return nothing. Added January 2017.
	//Added mobile attributes 6/30/17--after text=remove for desktops. 
	//But due to some sites already expecting Text=remove to affect desktops only, we have to maintain that situation. text=remove should not affect smartphones.
	if((!$frn_mobile && strtolower($text)=="remove") || ($frn_mobile && strtolower($mobile)=="remove")) : 
		//TESTING: return "<h1>Stage 1: Desktop only. Text or Desktop set to 'remove'. (mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text."; desktop_url: ".$desktop_url.")</h1>";
		return "";

	//if it's a desktop and a contact url is provided, then we know no number processing is necessary. Added March 2017.
	//but if it's an image, the phone number may be in the alt tag, so it's skipped here by looking for the $url attribute
	elseif(!$frn_mobile && $desktop_url!=="" && $url=="") : 
		//TESTING: return "<h1>Stage 2: Desktop only. Text version. URL switcheroo. (mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text."; desktop_url: ".$desktop_url.")</h1>";

		//Prepare TEXT attribute
		if($text=="") $text="Contact"; //if text not even defined, we need something in case it's forgotten
		elseif(strtolower($text)=="empty") $text="";

		if($desktop_id!=="") $id=$desktop_id;
		if($id!=="") $id='id="'.$id.'" ';
		if($class!=="") $class='class="'.$desktop_class.'" ';

		//When this shortcode is used on a page like a normal link, it's no longer a conversion element
		//so it should be reported as a normal click to another page and NOT as a Google click event.
		//However, this can be overridden if we use the word "desktop" or "global" somewhere in the category label. (Global assumes it's used in our global contact options)
		if($desktop_action!=="") $ga_phone_location = $desktop_action;
			else $ga_phone_location = "Desktop Switch with: ".$desktop_url;
		$ga_phone_label = "Contact";

		$onclick=""; //sets it to avoid error log reporting
		if(strpos(strtolower($ga_phone_category),"global")!==false || strpos(strtolower($ga_phone_category),"desktop")!==false) $onclick=' onClick="frn_reporting(\'\',\''.$ga_phone_category.'\',\''.$ga_phone_location.'\',\''.$ga_phone_label.'\',false,\'\');"';
		
		//Since this is a link, it'll work like a normal pageview, but I wanted to record the event so we can evaluate contact options performance in one place
		return '<a href="'.$desktop_url.'" '.$id.$class.$onclick.' >'.$text.'</a>'; 
		//onClick="if(typeof ga===\'function\') ga(\'send\', \'event\', \''.$ga_phone_category.'\', \'Desktop Phone Number Switch with: '.$desktop_url.'\');" >'.$desktop.'</a>';

	elseif($only=="yes") :

		//stage 2a
		if($number!=="") $printed_number = $number;
		else $printed_number = trim($options['site_phone']);

		return stripslashes($printed_number);

	else : 



		///////
		//// PHONE NUMBER PROCESSING IS REQUIRED
		///////
		
			//The rest of this NOT for:
				//if removing a link for desktop devices
				//if text is linked to a page instead of a number



		////////
		//Phone Number Prepping
		//So, if number processing is needed in some version, do our traditional processing	

		if(!isset($options['phone_linking'])) $options['phone_linking']=""; //phone_linking = processing method
		$number_from_db = frn_clean_number($options['site_phone']);

		//TESTING: return "<h1>Stage 3: All devices // phone number processing is required // no 'remove' vars // desktop_url is empty (number: ".$number_from_db."; mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text."; desktop_url: ".$desktop_url.")</h1>";

		//Prepare the two types of phone numbers
		if($number!=="") {
			$frn_number = frn_clean_number($number);
			$printed_number = stripslashes($number);
		}
		else {
			//if not defined in the shortcode, then pull from FRN Settings
			$frn_number = $number_from_db;
			$printed_number = trim(stripslashes($options['site_phone']));
		}




		////////
		// Prepare for display

		if($only=="cleaned") {
			//stage 2b; 
			//used the processed version of the site's number or the specified number
			//often used in links for phone numbers
			return $frn_number; 
		}
		else {
			
			//TESTING: return "<h1>Stage 3c: Mobile or Desktop // No onlys // no 'remove' vars // desktop_url is empty (mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text.")</h1>";

			//if number set is different than what's in the settings, use that as the label if it's not set in the shortcode
			if(($frn_number!==$number_from_db) && trim($ga_phone_label)=="") $ga_phone_label = $printed_number;
			
			/*
			//No longer used as of 2016, but kept to keep international concepts in mind
			//$lhn_tab_disabled=""; $intl_prefix="";
			//if($lhn_tab_disabled!=="") $lhn_tab_disabled = ' lhn_tab_disabled="'.$lhn_tab_disabled.'"';
			//if($intl_prefix!=="") $intl_prefix=' intl_prefix="'.$intl_prefix.'"';
		
			//Old approach using SPANs and javascript
			//if($ga_phone_category!=="") $ga_phone_category=' ga_phone_category="'.$ga_phone_category.'"';
			//if($ga_phone_location!=="") $ga_phone_location=' ga_phone_location="'.$ga_phone_location.'"';
			//if($ga_phone_label!=="") $ga_phone_label=' ga_phone_label="'.$ga_phone_label.'"';
			//if($frn_number!=="") $frn_number = ' frn_number="'.$frn_number.'"';
			*/
			



			/////////////
			/// IMAGES
			if($url!=="") {
				
				//TESTING: return "<h1>Stage 3da: Mobile or Desktop // Image ONLY // No onlys // no 'remove' vars // desktop_url is empty (mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text.")</h1>";

				//consolidate image vars into one var
				if($alt!=="") $image_alt=$alt;
				if($title!=="") $image_title=$title;

				//Setting CLASS and ID
				//we specifically set image_id/class to keep them out of phone number processing and leave the others blank
				//initially, we used image_id, but it became too complex for designers to remember, so sticking with just ID and class now (as of 2015)
				if($image_id=="" && $id!=="") {$image_id=$id; $id="";} //we can't use ID since it's passed to the link. For images, we don't want IDs in links
				if($image_id!=="") $image_id=' id="'.$image_id.'"';
				
				if($class!=="" && $image_class=="") {$image_class=$class; $class="";}
				if($image_class!=="") $image_class=' class="'.$image_class.'"';
				
				//inline style for number only
				if(strtolower($style)=="none") $style="";
					elseif($style!=="") $style=' style="'.$style.'"';
				
								/// LIKELY NOT USED ANYWHERE BY 2016
								//Check if domain bases are used (likely deprecated as of 2016, but didn't search 2015 redesigned sites for usage, so keeping activated)
								if(stripos($image_url,"%%frn_imagebase%%")>=0 or stripos($image_url,"%%idomain%%")>=0) {
									$options_sitebase = get_option('site_sitebase_code');
									$site_base = site_url();
									if(isset($options_sitebase['frn_imagebase'])) {
										if(trim($options_sitebase['frn_imagebase'])!=="") $site_base =  trim($options_sitebase['frn_imagebase']);
									}
									$image_url=str_replace('%%frn_imagebase%%',$site_base,$image_url);
									$image_url=str_replace('%%idomain%%',$site_base,$image_url);
								}
								else if(stripos($image_url,"%%frn_sitebase%%")>=0 or stripos($image_url,"%%ldomain%%")>=0) {
									$options_sitebase = get_option('site_sitebase_code');
									$site_base = site_url();
									if(isset($options_sitebase['frn_sitebase'])) {
										if(trim($options_sitebase['frn_sitebase'])!=="") $site_base =  trim($options_sitebase['frn_sitebase']);
									}
									$image_url=str_replace('%%frn_sitebase%%',$site_base,$image_url);
									$image_url=str_replace('%%ldomain%%',$site_base,$image_url);
								}

				if(isset($_SERVER["HTTPS"])) {if ($_SERVER["HTTPS"] == "on") $image_url = str_replace('http://','https://',$image_url);}
				

				//Check if phone number FRN %% shortcode used in alt tag and title
				if($image_alt!=="") {
					$image_alt=str_replace('%%frn_phone%%',$printed_number,$image_alt);
					$image_alt=' alt="'.$image_alt.'"';
				}
				if($image_title!=="") {
					$image_title=' title="'.$image_title.'"';
					$image_title=str_replace('%%frn_phone%%',$printed_number,$image_title);
					$title=str_replace('%%frn_phone%%',$printed_number,$title);
				}
				
				//build image HTML
				$image='<img'.$image_id.$image_class.' src="'.$image_url.'"'.$image_title.$image_alt.$image_style.' />';
				
				//Prep it all by sending to the phone prep function to build it
				//phone_linking = processing method
				if($frn_mobile && $options['phone_linking']!=="N") 
					return frn_phone_prep(array($image,$frn_number,$ga_phone_category,$ga_phone_location,$ga_phone_label,$style,$class,$id,$title,$lhn_tab_disabled,$intl_prefix));
				else {
					if($desktop_url!=="") return '<a href="'.$desktop_url.'">'.$image.'</a>';
					else return $image; 
				}

			}
			else {






				///////////
				// TEXT version (printing a phone number or text)
				///////////

				/*
					first figure out what should be linked
					then determine if it should be linked (desktop default is not to link a phone number)
					Then add ids and classes
				*/


				///////
				/// Prepare Text 

				/// Device-specific overwrites
				if($frn_mobile) {
					if($mobile!=="") $text=$mobile;
					if($mobile_id!=="") $id=$mobile_id;
					if($mobile_class!=="") $class=$mobile_class;
				}
				else {
					if($desktop!=="") $text=$desktop;
					if($desktop_id!=="") $id=$desktop_id;
					if($desktop_class!=="") $class=$desktop_class;
				}

				

				//Prepare TEXT attribute
				if(strtolower($text)=="empty") $text="";
				//elseif($text=="") $text="Contact"; //not sure why here. Was keeping the phone number from being printed on desktops.
				// $id -- don't define id here. It's defined in the frn_phone_prep or if no number, in the span section below.

				//inline styles
				if($style=="" && $css_style!=="") $style=$css_style;
				if($frn_number_style!=="" && $style=="") $style=$frn_number_style; //old approach no one could remember
				if(strtolower($style)=="none") $style="";
					elseif($style!=="") $style=' style="'.$style.'"';

				if($intl_prefix!=="") $intl_prefix=' intl_prefix="'.$intl_prefix.'"'; //used for JS processing of numbers ONLY
				
				//unexpected case: mobile phone and desktop=remove
				if($text!=="") {
					if(strtolower($text)=="remove") $printed_number="";
					else $printed_number=$text;
				}

				//return "<h1>Stage 3db: All devices (or smartphone && desktop=remove) // TEXT ONLY // no images (number: ".$printed_number."; mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text."; image_url: ".$image_url.")</h1>";
				//phone_linking = processing via JS or PHP
				//If it's a smartphone, we'll link the phone number
				if($frn_mobile && $options['phone_linking']!=="N") return frn_phone_prep(array($printed_number,$frn_number,$ga_phone_category,$ga_phone_location,$ga_phone_label,$style,$class,$id,$title,$lhn_tab_disabled,$intl_prefix));
				else {
					//Since phone number processing won't happen, pass id and class to the span instead of the link
					if($class!=="") $class=' class="'.$class.'" ';
					if($id!=="") $id=' id="'.$id.'" ';
					//prior to 3/26/17, the span with the number didn't have id or class. Not sure if including them will cause issues on older sites or not
					if($printed_number!=="") return '<span '.$id.$class.' style="white-space:nowrap;" >'.$printed_number.'</span>';
					else return '<span '.$id.$class.' ></span>';
				}
			} //end detection if image or not
		} //ends the more complicated processing section
	endif; //ends requirement that text NOT be "remove" with desktop
} // ends function
}


/////////
// Phone AutoScan: If turned on, this will scan only content or the entire page for phone number patterns for only mobile devices and turn touches on them into trackable events and to always look like links.
/////////
if(!function_exists('frn_phone_autoscan_content')) {
function frn_phone_autoscan_content( $content ) {
	//PHP option: simply acts like a shortcode in that it looks for a number pattern and then replaces it with the spans and number again
	/*
	Will not work with letters (helps with pattern identification and mobile conversions)
	You can change out dashes for spaces or periods
	International:
	0000 0000
	00 00 00 00
	00 000 000
	00000000
	00 00 00 00 00
	+00 0 00 00 00 00
	00000 000000
	+00 0000 000000
	(00000) 000000
	+00 0000 000000
	+00 (0000) 000000
	00000-000000
	00000/000000
	000 0000
	000-000-000
	0 0000 00-00-00
	(0 0000) 00-00-00
	0 000 000-00-00
	0 (000) 000-00-00
	000 000 000
	000 00 00 00
	000 000 000
	000 000 00 00
	+00 00 000 00 00
	0000 000 000
	(000) 0000 0000
	(00000) 00000
	(0000) 000 0000
	0000 000 0000
	0000-000 0000
	0000 000 0000
	00000 000000
	0000 000000
	0000 000 00 00
	+00 000 000 00 00
	(000) 0000000
	+00 00 00000000
	000 000 000
	+00-00000-00000
	(0000) 0000 0000
	+00 000 0000 0000
	(0000) 0000 0000
	+00 (00) 000 0000
	+00 (0) 000 0000
	+00 (000) 000 0000
	(00000) 00-0000
	(000) 000-000-0000
	(000) [00]0-000-0000
	(00000) 0000-0000
	+ 000 0000 000000
	
	U.S. Versions:
	0 (000) 000-0000
	+0-000-000-0000
	0-000-000-0000
	000-000-0000
	(000) 000-0000
	000-0000
	0 (000) 000-0000 ext 1
	0 (000) 000-0000 x 1001
	0 (000) 000-0000 extension 2
	0 000 000-0000 code 3
	
	NOTE: To help with the autoscan, it requires a period, space, bracket, greater or less than, or parenthesis to be at the front and/or end of the number before it's found. 
		  Some links have numbers with periods in them that cause the autoscan feature to add the SPAN code within the links. This decreases the chances of that happening, but it also includes those characters in the linked text, but not the telephone link.
		  Primary resources: http://us3.php.net/preg_replace and http://us3.php.net/manual/en/function.preg-replace-callback.php
		  A phone pattern resources: http://stackoverflow.com/questions/17331386/php-preg-replace-phone-numbers ,  http://www.myspotinternetmarketing.com/javascript-php-and-regular-expressions-for-international-and-us-phone-number-formats/
	
	These are found, but are incorrect formats: Basically, make sure you don't have a dash, space, plus sign, or period before number or this will pull it in.
	   0-000-0000
	   -0-000-0000
	   -0-000-000-0000
	   -0 (000) 000-0000
	   +000 00-0-000-0000
	   3456789012345-6789
	   	8.8.8.8
		192.168.1.1
	*/
	global $frn_mobile;
	if($frn_mobile) {
		$options_phone = get_option('site_phone_number');
		$auto_scan="C";
		if(isset($options_phone['auto_scan'])) $auto_scan = $options_phone['auto_scan'];
		if($auto_scan!=="W" && $auto_scan!=="DA") { // && $auto_scan!=="A" Removed since the JavaScript entire page scanning isn't working.
			$pattern_in_content = "/(\s|\(|\[|\>)\d?(\s?|-?|\+?|\.?)((\(\d{1,4}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)\d{3}(-|\.|\s)\d{4}(\s|\)|\]|\.|\<)/s"; //makes sure there is a space or some other character on either side of the number
			//$content = preg_replace_callback($pattern, "preg_replace_clean_up", $content, 1);  //This helps make sure that characters outside of phone numbers aren't linked.
			//old (links the characters on either side of number as well: $content = preg_replace($pattern, $before."$0".$after, $content, 1);
			$content = preg_replace_callback($pattern_in_content,"phone_find_stage2", $content);
		}
	}
	return $content;
}
}
add_filter( 'the_content', 'frn_phone_autoscan_content');

if(!function_exists('phone_find_stage2')) {
function phone_find_stage2($found_pattern) {
	//Since this is looking within the pattern already found, we can be more flexible.
	$pattern_in_found = "!((\d )|(\d-))?(\(|\[)?(\b\+?[0-9()\[\]./ -]{7,17}\b|\b\+?[0-9()\[\]./ -]{7,17}\s+(extension|x|#|-|code|ext)\s+[0-9]{1,6})!i";
	return preg_replace_callback($pattern_in_found, function($matches){return frn_phone_prep(array($matches[0]));}, $found_pattern[0]);
}
}
if(!function_exists('frn_phone_autoscan_widget')) {
function frn_phone_autoscan_widget( $widget ) {
	//PHP option: simply acts like a shortcode in that it looks for a number pattern and then replaces it with the spans and number again
	global $frn_mobile;
	if($frn_mobile) {
		$options_phone = get_option('site_phone_number');
		$auto_scan="C";
		if(isset($options_phone['auto_scan'])) $auto_scan = $options_phone['auto_scan'];
		if($auto_scan=="WC" || $auto_scan=="W") {
			//Pull the matches
			$pattern = "/(\s|\(|\[|\>)\d?(\s?|-?|\+?|\.?)((\(\d{1,4}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)\d{3}(-|\.|\s)\d{4}(\s|\)|\]|\.|\<)/";
			$widget = preg_replace_callback($pattern, function($matches){return frn_phone_prep(array($matches[0]));}, $widget, 1);
		}
	}
	return $widget;
}
}
add_filter( 'widget_text', 'frn_phone_autoscan_widget');



/////////
// Phone JAVASCRIPT_SCAN Trigger
/////////
// Javascript phone detection feature only, not ever used so far
// Was initial version until realizing it slowed page load on the browser side of things
// Tested in June 2017 and it wasn't working. Helps when phone numbers are in PHP locations that can't be changed or in fields where shortcodes aren't possible.
if(!function_exists('frn_phone_js_scan')) {
function frn_phone_js_scan(){
// This script runs for every page load.
// Used to add phones2links.js to footer and see if entire page autoscan is turned on for JS file to scan the page for phone number formats and turn them into links using JS.
	global $frn_mobile;
	if($frn_mobile) {
		$frn_phone_ftr=""; $phone_linking="";

		//Turn on auto scanning
		$options_phone = get_option('site_phone_number');
		if(isset($options_phone['site_phone'])) $site_phone=$options_phone['site_phone'];
			else $site_phone="";

		//phone_linking = processing method
		if(isset($options_phone['phone_linking'])) $phone_linking=$options_phone['phone_linking'];
		$options_ua = get_option('site_head_code');
		$ga_ua = "_ua";
		if(isset($options_ua['frn_ga_ua'])) {
			if($options_ua['frn_ga_ua']!=="Activate") $ga_ua = ""; //Used to refer to other JS files that have the latest GA event and social tracking codes
		}
		
		//This feature was disabled due to the JavaScript replacing previously changed numbers in the sequence. So, A will never be an option.
		$auto_scan="C";
		if(isset($options_phone['auto_scan'])) $auto_scan = $options_phone['auto_scan'];
		if($auto_scan=="A") {
			//if to scan all of the page, set a global JS variable that will turn on the autoscan feature in our normal phones2links.js
			//See if content ID block is set
			if(isset($options_phone['js_content_id'])) $js_content_id=trim($options_phone['js_content_id']);
			if($js_content_id!=="") $js_content_id = ' js_content_id="'.trim($options_phone['js_content_id']).'";';
			
			$frn_phone_ftr .=  '
	<script type="text/javascript">frn_phone_autoscan="yes";'.$js_content_id.'</script>'; 
	
		}
		
		//Checks if JS turned on manually or phone number is added via JavaScript
		if($auto_scan=="A" || $phone_linking=="JS" ) { //removed || strpos($site_phone,"script")!==false from this IF statement since it's no longer use by DialogTech.
		$frn_phone_ftr .=  '
	<script async=\'async\' type="text/javascript" src="'.plugins_url().'/frn_plugins/part_phones2links'.$ga_ua.'.js"></script>
		';
		}
		
		echo $frn_phone_ftr;
	}
}
}







//////////////////////////////////
// DIALOG TECH SCRIPT/OPTIONS   //
//////////////////////////////////

//This script is added into the section where the wp_footer function is added in the PHP. It's unlikely it's right before the /BODY tag. Dax isn't sure how that'll affect DT's find and replace of the phone numbers and Analytics reporting.
add_action('wp_footer', 'frn_dialogtech', 101);
if(!function_exists('frn_dialogtech')) {
function frn_dialogtech(){

	$options_phone = get_option('site_phone_number');
	if(!isset($options_phone['dialogtech'])) $options_phone['dialogtech']="";
	if(!isset($options_phone['dialogtech_id'])) $options_phone['dialogtech_id']="29b7c6a9512f3b1450d85acb9aa55fb8818ba6aa";

	if($options_phone['dialogtech']!=="") {
		$dialogtech ='
	<!-- #####
	DialogTech Service
	##### -->
	<script type="text/javascript">
        var _stk = "'.$options_phone['dialogtech_id'].'";
        (function(){
            var a=document, b=a.createElement("script"); b.type="text/javascript";
            b.async=!0; b.src=(\'https:\'==document.location.protocol ? \'https://\' :
            \'http://\') + \'d31y97ze264gaa.cloudfront.net/assets/st/js/st.js\';
            a=a.getElementsByTagName("script")[0]; a.parentNode.insertBefore(b,a);
        })();
    </script>
    <!-- #####
	END DialogTech Service
	##### -->



	';

		echo $dialogtech;

	}
}
}
//The action that adds this to the footer is at the very bottom of this page with the rest of them




?>