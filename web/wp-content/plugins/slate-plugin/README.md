[logo]: https://github.com/christianstclair/slate-plugin/blob/master/assets/images/slate-logo.png
![alt text][logo]

# Slate Plugin

Wordpress Plugin for Slate Framework developed by Foundations Recovery Network.

-----------------------

* Readme : https://github.com/christianstclair/slate-plugin/blob/master/readme.txt
* Author : http://www.christianstclair.com/