module.exports = function(grunt) {

  // Configure task(s)
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    
    // Watch
    watch: {
      sass: {
        files: ['scss/*.scss'],
        tasks: ['sass:dev']
      }
    },// /watch


    // sass
    sass: {
      dev: {
        options: {
            sourceMap: true
        },
        files: {
                'css/slate.css':'scss/slate.scss'
        }
      }
    } // /sass

  });




  // Load the plugins
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  

  // Register task(s).
  grunt.registerTask('default', ['sass:dev', 'watch']);

};