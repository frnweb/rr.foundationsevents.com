<?php

/*
Plugin Name: Slate Plugin
Plugin URI: https://github.com/christianstclair/slate-plugin
Description: A WordPress Admin theme
Author: christianstclair
Version: 1.1.01
Author URI: http://christianstclair.com/
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://github.com/christianstclair/slate-plugin',
    __FILE__,
    'slate-plugin'
);



//////// PARTS //////////

  // Customize Wordpress Admin Bar
  require_once('functions/admin-bar.php'); 

  // Supporting Relevanssi Excerpts from ACF Fields
  require_once('functions/search.php'); 





// Hide editor on specific pages.
function hide_editor() {
  // Get the Post ID.
  $post_id = $_GET['post'];
  if( !isset( $post_id ) ) return;
  // Hide the editor on a page with a specific page template
  // Get the name of the Page Template file.
  $template_file = get_post_meta($post_id, '_wp_page_template', true);
  if($template_file == 'template-home.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
}
add_action( 'admin_head', 'hide_editor' );


// Remove the hyphen before the post state
add_filter( 'display_post_states', 'slate_post_state' );
function slate_post_state( $post_states ) {
	if ( !empty($post_states) ) {
		$state_count = count($post_states);
		$i = 0;
		foreach ( $post_states as $state ) {
			++$i;
			( $i == $state_count ) ? $sep = '' : $sep = '';
			echo "<span class='post-state'>$state$sep</span>";
		}
	}
}
  // Enqueue and Dequeue Scripts
  require_once('functions/enqueue-scripts.php'); 

?>